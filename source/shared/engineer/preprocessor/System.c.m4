divert(-1)
changecom(/*,*/)dnl
define(`HEADER', `include(SOURCE_PATH/System.h)')
define(`SOURCE', `include(SOURCE_PATH/System.c)')

define(`SYMBOL',
   `define(`ASSET_PAYLOAD', `$1')'
   `define(`LABEL_PAYLOAD', `$2')'
)
define(`PREREQS', `define(`PREREQS_PAYLOAD', `$*')')
define(`GLOBAL',  `define(`GLOBAL_PAYLOAD',  `$*')')
define(`ENTITY',  `define(`ENTITY_PAYLOAD',  `$*')')
HEADER

divert(0)dnl
#define System_State ASSET_SYMBOL`_'DOMAIN_SYMBOL`_System_State'
#define Entity_State ASSET_SYMBOL`_'DOMAIN_SYMBOL`_Entity_State'

SOURCE

/*** Local Type Definitions and Module-Global Vars ***/

#define BATCH_SIZE 64

typedef struct
{
   Indx         owner;
   Indx         offset;
   Entity_State state[];
}
Batch_Header;

typedef enum
{
   PREREQ_NULL
   divert(-1)
   define(`ASSET',  `define(`asset',  `$1') $2 undefine(`asset')')
   define(`SYSTEM', ``,''``PREREQ_`'asset`'_System_$1'')

   divert(0)dnl
   ifdef(`PREREQS_PAYLOAD', `PREREQS_PAYLOAD', `')`'dnl
}
System_Prereq;

typedef enum
{
   SYSTEM_EVENT_NULL

   divert(-1)
   define(`STATE',  `dnl')
   define(`EVENTS', `$*')

   define(`EVENT', ``,''SYSTEM_EVENT_$1)
   divert(0)dnl
   ifdef(`GLOBAL_PAYLOAD', `GLOBAL_PAYLOAD', `')`'dnl
}
System_Event_Key;

typedef enum
{
   ENTITY_EVENT_NULL

   divert(-1)
   define(`INPUTS',       `dnl')
   define(`OUTPUTS',      `dnl')
   define(`EVENTS',        `$*')

   define(`FIELD',        `dnl')
   define(`EVENT', ``,'' ENTITY_EVENT_$1)

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
}
Entity_Event_Key;

typedef enum
{
   INPUT_NULL
   divert(-1)
   define(`INPUTS',   `$*')
   define(`OUTPUTS', `dnl')
   define(`EVENTS',  `dnl')

   define(`ASSET',     `define(`asset', `$1') $2 undefine(`asset')')
   define(`COMPONENT', `define(`label', `$1') $2 undefine(`label')')
   define(`FIELD',     ```,''' ``INPUT_``''asset``''_Component_`'label`'_$2'')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD',  `')`'dnl
}
Component_Input;

typedef enum
{
   OUTPUT_NULL
   divert(-1)
   define(`INPUTS',  `dnl')
   define(`OUTPUTS',  `$*')
   define(`EVENTS',  `dnl')

   define(`ASSET',     `define(`asset', `$1') $2 undefine(`asset')')
   define(`COMPONENT', `define(`label', `$1') $2 undefine(`label')')
   define(`FIELD',     ```,''' ``OUTPUT_``''asset``''_Component_`'label`'_$2'')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
}
Component_Output;


/*** Class Identifier Methods ***/

EAPI String *
eng_system_symbol_echo()
{
   return `"'ASSET_SYMBOL.DOMAIN_SYMBOL.System`"';
}

EAPI String *
eng_system_asset_echo()
{
   return `"'ASSET_SYMBOL`"';
}

EAPI String *
eng_system_label_echo()
{
   return `"'DOMAIN_SYMBOL`"';
}


/*** Class and Module Constant Table Factories ***/

EAPI Eina_Inarray *
eng_system_prereq_enumerator_new()
{
   Eina_Inarray            *enumerator;
   Eng_System_Class_Prereq  prereq EINA_UNUSED;

   enumerator = eina_inarray_new(sizeof(Eng_System_Class_Prereq), 0);

   divert(-1)
   define(`ASSET',  `define(`asset_label', $1) $2 undefine(`asset_label')')dnl
   define(`SYSTEM', ``dnl
      prereq.symbol = "`'asset_label`'.$1.System";
      prereq.key    = eng_hash_murmur3(prereq.symbol);
      prereq.class  = NULL;
      eina_inarray_push(enumerator, &prereq);
   '')

   divert(0)dnl
   ifdef(`PREREQS_PAYLOAD', `PREREQS_PAYLOAD', `')`'dnl

   return enumerator;
}

static void
_eng_system_prereq_reference(System_State *system EINA_UNUSED)
{
   divert(-1)
   define(`ASSET', `define(`asset_label', $1) $2 undefine(`asset_label')')dnl
   define(`SYSTEM', ``dnl
      system->prereq.asset_label.$1 = (void*)eng_scene_system_state_get(system->scene, "asset_label.$1.System");
   '')

   divert(0)dnl
   ifdef(`PREREQS_PAYLOAD', `PREREQS_PAYLOAD', `')`'dnl
}

EAPI Eina_Inarray *
eng_system_component_prereq_class_enumerator_new(Eo *node EINA_UNUSED)
{
   Eina_Inarray               *enumerator;
   Eng_Component_Class_Prereq  prereq EINA_UNUSED;

   enumerator = eina_inarray_new(sizeof(Eng_Component_Class_Prereq), 0);

   divert(-1)
   define(`INPUTS',   `$*')
   define(`OUTPUTS', `dnl')
   define(`EVENTS',  `dnl')

   define(`ASSET',     `define(`asset_label',  $1) $2 undefine(`asset_label')')
   define(`COMPONENT', ``dnl
      prereq.symbol = "`'asset_label`'.$1.Component";
      prereq.key    = eng_hash_murmur3(prereq.symbol);
      prereq.class  = NULL;
      eina_inarray_push(enumerator, &prereq);
   '')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   return enumerator;
}

static Eina_Inarray *
_eng_system_component_prereq_module_enumerator_new(Eo *scene EINA_UNUSED)
{
   Eina_Inarray *enumerator;
   Eo           *prereq EINA_UNUSED;
   uint64_t      key EINA_UNUSED;

   enumerator = eina_inarray_new(sizeof(uint64_t), 0);

   divert(-1)
   define(`INPUTS',   `$*')
   define(`OUTPUTS', `dnl')
   define(`EVENTS',  `dnl')

   define(`ASSET',     `define(`asset',  $1) $2 undefine(`asset')')
   define(`COMPONENT', ``dnl
      key = eng_hash_murmur3("`'asset`'.$1.Component");
      if((prereq = eng_scene_component_module_query_loaded_by_id(scene, key)))
         eina_inarray_push(enumerator, &prereq);
      else { eina_inarray_free(enumerator); return NULL; }
   '')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   return enumerator;
}

EAPI Eina_Inarray *
eng_system_component_input_class_enumerator_new(Eo *node EINA_UNUSED)
{
   Eina_Inarray               *enumerator;
   Eng_Component_Class_Target  target EINA_UNUSED;

   enumerator = eina_inarray_new(sizeof(Eng_Component_Class_Target), 0);

   divert(-1)
   define(`INPUTS',   `$*')
   define(`OUTPUTS', `dnl')
   define(`EVENTS',  `dnl')

   define(`ASSET',     `define(`asset', `$1') $2 undefine(`asset')')
   define(`COMPONENT', `define(`label', `$1') $2 undefine(`label')')
   define(`FIELD', ``dnl
      target.class_key = eng_hash_murmur3("``''asset``''.`'label`'.Component");
      target.class     = NULL;
      target.field_key = eng_hash_murmur3("``''asset``''.`'label`'.Component.$2");
      target.field     = (Indx)-1;
      eina_inarray_push(enumerator, &target);
   '')dnl

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   return enumerator;
}

static Eina_Inarray *
_eng_system_component_input_module_enumerator_new(Eo *scene EINA_UNUSED)
{
   Eina_Inarray                *enumerator;
   Eng_Component_Module_Target  target    EINA_UNUSED;
   uint64_t                     symbol_id EINA_UNUSED;

   enumerator = eina_inarray_new(sizeof(Eng_Component_Module_Target), 0);

   divert(-1)
   define(`INPUTS',   `$*')
   define(`OUTPUTS', `dnl')
   define(`EVENTS',  `dnl')

   define(`ASSET',     `define(`asset', `$1') $2 undefine(`asset')')
   define(`COMPONENT', `define(`label', `$1') $2 undefine(`label')')
   define(`FIELD', ``
      symbol_id = eng_hash_murmur3("``''asset``''.`'label`'.Component");
      if((target = (Pntr)eng_scene_component_module_query_loaded_by_id(scene, symbol_id)))
         eina_inarray_push(enumerator, &target);
      else { eina_inarray_free(enumerator); return NULL; }
   '')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   return enumerator;
}

EAPI Eina_Inarray *
eng_system_component_output_class_enumerator_new(Eo *node EINA_UNUSED)
{
   Eina_Inarray               *enumerator;
   Eng_Component_Class_Target  target EINA_UNUSED;

   enumerator = eina_inarray_new(sizeof(Eng_Component_Class_Target), 0);

   divert(-1)
   define(`INPUTS',  `dnl')
   define(`OUTPUTS',  `$*')
   define(`EVENTS',  `dnl')

   define(`ASSET',     `define(`asset', `$1') $2 undefine(`asset')')
   define(`COMPONENT', `define(`label', `$1') $2 undefine(`label')')
   define(`FIELD', ``dnl
      target.class_key = eng_hash_murmur3("``''asset``''.`'label`'.Component");
      target.class     = NULL;
      target.field_key = eng_hash_murmur3("``''asset``''.`'label`'.Component.$2");
      target.field     = (Indx)-1;
      eina_inarray_push(enumerator, &target);
   '')dnl

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   return enumerator;
}

static Eina_Inarray *
_eng_system_component_output_module_enumerator_new(Eng_Scene *scene EINA_UNUSED)
{
   Eina_Inarray                *enumerator;
   Eng_Component_Module_Target  target EINA_UNUSED;
   uint64_t                     symbol_id EINA_UNUSED;

   enumerator = eina_inarray_new(sizeof(Eng_Component_Module_Target), 0);

   divert(-1)
   define(`INPUTS',  `dnl')
   define(`OUTPUTS',  `$*')
   define(`EVENTS',  `dnl')

   define(`ASSET',     `define(`asset', `$1') $2 undefine(`asset')')
   define(`COMPONENT', `define(`label', `$1') $2 undefine(`label')')
   define(`FIELD', ``
      symbol_id = eng_hash_murmur3("``''asset``''.`'label`'.Component");
      if((target = (Pntr)eng_scene_component_module_query_loaded_by_id(scene, symbol_id)))
         eina_inarray_push(enumerator, &target);
      else { eina_inarray_free(enumerator); return NULL; }
   '')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   return enumerator;
}

EAPI Eina_Hash *
eng_system_event_enumerator_new()
{
   // Create an System Event Enumeration Hash Lookup Table.
   Eina_Hash *output = eina_hash_int64_new(NULL);
   uint64_t   key EINA_UNUSED;

   divert(-1)
   define(`STATE', `dnl')
   define(`EVENTS', `$*')

   define(`FIELD', `dnl')
   define(`EVENT', ``dnl
      key = eng_hash_murmur3("$1");
      eina_hash_add(output, &key, (uint64_t*)SYSTEM_EVENT_$1);
   '')

   divert(0)dnl
   ifdef(`GLOBAL_PAYLOAD', `GLOBAL_PAYLOAD', `')`'dnl

   return output;
}

EAPI Eina_Bool
eng_system_event_register()
{
   uint64_t key EINA_UNUSED;

   divert(-1)
   define(`STATE', `dnl')
   define(`EVENTS', `$*')

   define(`FIELD', `dnl')
   define(`EVENT', ``dnl
      key = eng_hash_murmur3("$1");
      eng_node_notice_register(eng_node(), key, $2);
   '')

   divert(0)dnl
   ifdef(`GLOBAL_PAYLOAD', `GLOBAL_PAYLOAD', `')`'dnl

   return EINA_TRUE;
}

EAPI Eina_Hash *
eng_system_entity_event_enumerator_new()
{
   Eina_Hash *output = eina_hash_int64_new(NULL);
   uint64_t   hash EINA_UNUSED;

   divert(-1)
   define(`INPUTS',  `dnl')
   define(`OUTPUTS', `dnl')
   define(`EVENTS',   `$*')

   define(`FIELD', `dnl')
   define(`EVENT', ``dnl
      hash = eng_hash_murmur3("$1");
      eina_hash_add(output, &hash, (uint64_t*)ENTITY_EVENT_$1);
   '')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   return output;
}

EAPI Eina_Bool
eng_system_entity_event_register()
{
   uint64_t key EINA_UNUSED;

   divert(-1)
   define(`INPUTS',  `dnl')
   define(`OUTPUTS', `dnl')
   define(`EVENTS',   `$*')

   define(`FIELD',  `dnl')
   define(`EVENT', ``dnl
      key = eng_hash_murmur3("$1");
      eng_node_notice_register(eng_node(), key, $2);
   '')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   return EINA_TRUE;
}


/*** Eo Constructor/Destructor Methods ***/

EAPI Efl_Object *
eng_system_new(Eo *parent)
{
   Eo *obj;

   efl_domain_current_push(EFL_ID_DOMAIN_SHARED);
   obj = efl_add(ENG_SYSTEM_CLASS, parent);
   efl_domain_current_pop();

   return obj;
}

EOLIAN Data *
_eng_system_state(Eo *obj EINA_UNUSED, Eng_System_Data *pd)
{
   return pd->global_state;
}

EOLIAN static Efl_Object *
_eng_system_efl_object_constructor(Eo *obj, Eng_System_Data *pd EINA_UNUSED)
{
   return efl_constructor(efl_super(obj, ENG_SYSTEM_CLASS));
}

EOLIAN static Efl_Object *
_eng_system_efl_object_finalize(Eo *obj, Eng_System_Data *pd)
{
   obj = efl_finalize(efl_super(obj, ENG_SYSTEM_CLASS));

   Eng_Scene    *scene  = efl_parent_get(obj);
   String       *symbol = eng_system_symbol_echo();
   System_State *system = calloc(1, sizeof(System_State));

   pd->global_state = system;

   // These are System_Module-specific constants.
   system->class  = eng_node_system_class_query_loaded(eng_node(), symbol);
   system->scene  = scene;
   system->entity = eng_node_entity_id_use(eng_node());

   _eng_system_prereq_reference(system);

   pd->dependent_systems = eina_hash_int64_new(NULL);

   pd->entity_targets      = eina_inarray_new(sizeof(EntityID), 0);
   pd->entity_state_buffer = eng_queue_new(4096);

   pd->component_prereqs   = _eng_system_component_prereq_module_enumerator_new(scene);
   pd->component_inputs    = _eng_system_component_input_module_enumerator_new(scene);
   pd->component_outputs   = _eng_system_component_output_module_enumerator_new(scene);

   eng_node_entity_scene_set(eng_node(), system->entity, scene);
   eng_scene_entity_create_event(scene, (Data*)&system->entity);

   _eng_system_start(system->entity, system);

   return obj;
}

EOLIAN static void
_eng_system_efl_object_destructor(Eo *obj, Eng_System_Data *pd)
{
   System_State *system EINA_UNUSED = pd->global_state;

   //eng_node_entity_destroy(eng_node(), system->entity);

   eina_inarray_free(pd->component_prereqs);
   eina_inarray_free(pd->component_inputs);
   eina_inarray_free(pd->component_outputs);

   eina_inarray_free(pd->entity_targets);

   eina_hash_free(pd->dependent_systems);

   efl_destructor(efl_super(obj, ENG_SYSTEM_CLASS));
}

/*** Depependent System_Module API ***/

EOLIAN static void
_eng_system_dependent_register(Eo *obj EINA_UNUSED, Eng_System_Data *pd,
        uint64_t class_id)
{
   eina_hash_set(pd->dependent_systems, &class_id, &class_id);
}

EOLIAN static uint64_t *
_eng_system_dependent_query(Eo *obj EINA_UNUSED, Eng_System_Data *pd,
        uint64_t class_id)
{
   return eina_hash_find(pd->dependent_systems, &class_id);
}

EOLIAN static void
_eng_system_dependent_deregister(Eo *obj EINA_UNUSED, Eng_System_Data *pd,
        uint64_t class_id)
{
   eina_hash_del_by_key(pd->dependent_systems, &class_id);
}

/*** System Iteration and Dispatch Methods ***/

static void
_eng_system_entity_transport_state(
        Eng_Component_Class_Target  *target EINA_UNUSED,
        Eng_Component_Module_Target *module EINA_UNUSED,
        Entity_State                *entity EINA_UNUSED,

        const Eina_Bool              import,
        const Eina_Bool              input
){
   void (*transfer)(void *pd, Indx field, EntityID entity, Data *state);

   void                        *state;
   Eng_Component_Class_Target  *first_target = target;
   Eng_Component_Module_Target *first_module = module;

   if(input)
   {
      divert(-1)
      define(`INPUTS',   `$*')
      define(`OUTPUTS', `dnl')
      define(`EVENTS',  `dnl')

      define(`ASSET',     `define(`asset', $1) $2 undefine(`asset')')
      define(`COMPONENT', `define(`label', $1) $2 undefine(`label')')
      define(`FIELD', ``dnl
         target = first_target + INPUT_``''asset``''_Component_`'label`'_$2 - 1;
         module = first_module + INPUT_``''asset``''_Component_`'label`'_$2 - 1;

         if(import) transfer = target->class->state_export;
         else       transfer = target->class->state_import;

         state = target->class->state(*(Eo**)module);
         transfer(state, target->field, entity->id, (Data*)&entity->asset.label.$2);
      '')

      divert(0)dnl
      ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
   }
   else
   {
      divert(-1)
      define(`INPUTS',  `dnl')
      define(`OUTPUTS',  `$*')
      define(`EVENTS',  `dnl')

      define(`ASSET',     `define(`asset', $1) $2 undefine(`asset')')
      define(`COMPONENT', `define(`label', $1) $2 undefine(`label')')
      define(`FIELD', ``dnl
         target = first_target + OUTPUT_``''asset``''_Component_`'label`'_$2 - 1;
         module = first_module + OUTPUT_``''asset``''_Component_`'label`'_$2 - 1;

         if(import) transfer = target->class->state_export;
         else       transfer = target->class->state_import;

         state = target->class->state(*(Eo**)module);
         transfer(state, target->field, entity->id, (Data*)&entity->asset.label.$2);
      '')

      divert(0)dnl
      ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
   }
}

static Eina_Bool
_eng_system_dispatch_events(System_State *system)
{
   Indx               progress, offset;
   Eng_Queue         *outbox;
   Eng_Queue_Payload *payload;
   Eng_Event         *event;
   System_Event_Key   keyhole;

   outbox = eng_scene_entity_outbox_get(system->scene, system->entity);
   if(outbox)
   {
      // Alter the Scenegraph according to the given Entities' outbox.
      progress = 0;
      while(progress < outbox->occupancy)
      {
         offset  = (outbox->tail_in + progress) % outbox->capacity;
         payload = (Eng_Queue_Payload*)(outbox->payload + offset);
         event   = (Eng_Event*)payload->data;
         keyhole = (System_Event_Key)eina_hash_find(system->class->system_events, &event->key);

         switch(keyhole)
         {
            case SYSTEM_EVENT_NULL : /* Nothing Happens! */ break;

            divert(-1)
            define(`STATE',  `dnl')
            define(`EVENTS',  `$*')
            define(`FIELD',  `dnl')
            define(`EVENT',
               case SYSTEM_EVENT_$1 :
               _eng_system_event_$1(system->entity, system, (Data*)event->data);
               break;
            )

            divert(0)dnl
            ifdef(`GLOBAL_PAYLOAD', `GLOBAL_PAYLOAD', `')`'dnl
         }
         progress += PAGE_ALIGN(payload->size + sizeof(Eng_Queue_Payload));
      }
   }

   return EINA_TRUE;
}

static void
_eng_system_entity_import_state_task(void *data, Ecore_Thread *thread EINA_UNUSED)
{
   // We process Entities in batches of BATCH_SIZE to reduce thread contention and queue drain/fill
   //    use, and to allow the compiler to do loop unrolling/SSE optimizations.
   Eng_System_Worker *worker;
   System_State      *system;

   Batch_Header *batch;
   Entity_State *entity;

   Indx size, offset, index;

   worker = data;
   system = worker->global;
   size   = sizeof(Batch_Header) + (sizeof(Entity_State) * BATCH_SIZE);

   for(offset = worker->id * worker->total; offset < (worker->id + 1) * worker->total; offset++)
   {
      batch         = eng_queue_fill(worker->state_buffer, size);
      batch->owner  = worker->id;
      batch->offset = offset;

      entity = batch->state;
      for(index = batch->offset; index < (batch->offset + BATCH_SIZE); index++)
      {
         if(index < eina_inarray_count(worker->entity_targets))
         {
            entity->id     = *(EntityID*)eina_inarray_nth(worker->entity_targets, index);
            entity->outbox = eng_scene_entity_outbox_get(system->scene, entity->id);
            _eng_system_entity_transport_state(
               eina_inarray_nth(system->class->component_inputs,  0),
               eina_inarray_nth(worker->component_module_targets, 0),
               entity,
               EINA_TRUE,
               EINA_TRUE
            );
            entity++;
         }
         else break;
      }

      eng_queue_fill_done(worker->state_buffer, batch);
   }

   eng_scene_fence_decrement(system->scene);
}

static Eina_Bool
_eng_system_entity_dispatch_events(Eng_System_Worker *worker, Entity_State *entity)
{
   System_State      *system;
   Eng_Queue         *outbox;
   Eng_Queue_Payload *payload;
   Eng_Event         *event;
   Entity_Event_Key   keyhole;
   Indx               progress, offset;

   if(!entity->outbox) return EINA_TRUE;

   // Alter the Scenegraph according to the given Entities' outbox.
   system   = worker->global;
   outbox   = entity->outbox;
   progress = 0;

   while(progress < outbox->occupancy)
   {
      offset  = (outbox->tail_in + progress) % outbox->capacity;
      payload = (Eng_Queue_Payload*)(outbox->payload + offset);
      event   = (Eng_Event*)payload->data;
      keyhole = (Entity_Event_Key)eina_hash_find(system->class->entity_events, &event->key);

      switch(keyhole)
      {
         case ENTITY_EVENT_NULL : /* Nothing Happens! */ break;

         divert(-1)
         define(`INPUTS',  `dnl')
         define(`OUTPUTS', `dnl')
         define(`EVENTS',   `$*')

         define(`FIELD', `dnl')
         define(`EVENT',
            case ENTITY_EVENT_$1 :
            _eng_system_entity_event_$1(entity->id, system, entity, (Data*)event->data);
            break;
         )

         divert(0)dnl
         ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
      }

      progress += PAGE_ALIGN(payload->size + sizeof(Eng_Queue_Payload));
   }
   return EINA_TRUE;
}

static void
_eng_system_entity_update_state_task(void *data, Ecore_Thread *thread EINA_UNUSED)
{
   Eng_System_Worker *worker;
   Eng_Queue_Payload *payload;
   Batch_Header      *batch;
   System_State      *system;
   Entity_State      *entity;
   Indx               size, start, index, progress, this, occupancy;

   worker   = data;
   system   = worker->global;
   size     = PAGE_ALIGN(sizeof(Eng_Queue_Payload) + sizeof(Batch_Header) + (sizeof(Entity_State) * BATCH_SIZE));
   start    = LOAD(&worker->state_buffer->tail_in);
   occupancy = worker->state_buffer->occupancy;

   printf("Entity Update State Task Checkpoint. System: %s\n", system->class->label_echo());

   progress = AFA(worker->progress, size);
   while(progress < occupancy)
   {
      this      = (start + progress) % worker->state_buffer->capacity;
      payload   = (Eng_Queue_Payload*)(worker->state_buffer->payload + this);
      batch     = (Batch_Header*)payload->data;
      entity    = batch->state;
      for(index = batch->offset; index < (batch->offset + BATCH_SIZE); index++)
      {
         if(index < eina_inarray_count(worker->entity_targets))
         {
            _eng_system_entity_dispatch_events(worker, entity);
            _eng_system_entity_update(entity->id, system, entity);
            entity++;
         }
         else break;
      }
      progress = AFA(worker->progress, size);
   }

   eng_scene_fence_decrement(system->scene);
}

static void
_eng_system_entity_export_state_task(void *data, Ecore_Thread *thread EINA_UNUSED)
{
   Eng_System_Worker *peers, *peer, *worker;
   Batch_Header      *batch, *handoff;
   System_State      *system;
   Entity_State      *entity;
   Eng_Queue         *target;
   Indx               size, index, progress;

   size   = sizeof(Batch_Header) + (sizeof(Entity_State) * BATCH_SIZE);
   worker = data;
   system = worker->global;

   peers    = worker - worker->id;
   progress = 0;
   while(progress < worker->total)
   {
      if( (batch = eng_queue_drain(worker->handoff_buffer)) ) target = worker->handoff_buffer; else
      if( (batch = eng_queue_drain(worker->state_buffer)) )   target = worker->state_buffer;   else
      continue;

      if(batch->owner != worker->id)
      {
         peer    = peers + batch->owner;
         handoff = eng_queue_fill(peer->handoff_buffer, size);

         memcpy(handoff, batch, size);

         eng_queue_fill_done(peer->handoff_buffer, handoff);
      }
      else
      {
         entity = batch->state;
         for(index = batch->offset; index < (batch->offset + BATCH_SIZE); index++)
         {
            if(index < eina_inarray_count(worker->entity_targets))
            {
               _eng_system_entity_transport_state(
                  eina_inarray_nth(system->class->component_outputs, 0),
                  eina_inarray_nth(worker->component_module_targets, 0),
                  entity,
                  EINA_FALSE,
                  EINA_FALSE);
               entity++;
            }
            else break;
         }
         progress++;
      }
      eng_queue_drain_done(target, batch);
   }
   eng_scene_fence_decrement(system->scene);
}

EAPI void
eng_system_iterate(Eo *obj)
{
   Eng_System_Data   *pd;
   System_State      *system;
   Eng_System_Worker *worker;
   Eina_Inarray      *workers;
   _Atomic Indx       progress;
   Indx               population, targets, id, batches, total;
   int32_t            extra;

   pd         = efl_data_scope_get(obj, ENG_SYSTEM_CLASS);
   system     = pd->global_state;
   population = ecore_thread_max_get();
   workers    = eina_inarray_new(sizeof(Eng_System_Worker), population);
   worker     = eina_inarray_grow(workers, population);
   targets    = eina_inarray_count(pd->entity_targets);
   progress   = 0;

   // No need to process target Entities if there are none.
   if(targets)
   {
      // We usually won't have an evenly divisible number of batches for a given number of workers,
      //    so we short the last worker and give him the modulo's worth of batches.
      batches = targets / BATCH_SIZE;
      if( (targets % BATCH_SIZE) ) batches++;

      if(population > batches) population = batches;

      total = batches / population;
      extra = batches % population;

      for(id = 0; id < population; id++)
      {
         worker->global            = system;
         worker->id                = id;     // The worker id number.
         worker->total             = total + (extra-- > 0);  // The total number of Batches assigned to this worker.
         worker->progress          = &progress;
         worker->state_buffer      = pd->entity_state_buffer;
         worker->handoff_buffer    = eng_queue_new(4096);
         worker->entity_targets    = pd->entity_targets;
         worker++;
      }
      worker -= population;

      eng_scene_phase_wait(system->scene);
      eng_scene_phase_set(system->scene, SYSTEM_ENTITY_IMPORT);

      for(id = 0; id < population; id++)
      {
         eng_scene_fence_increment(system->scene);
         worker->component_module_targets = pd->component_inputs;
         //ecore_thread_run(_eng_system_entity_import_state_task, NULL, NULL, worker);
         _eng_system_entity_import_state_task(worker, NULL);
         worker++;
      }
      worker -= population;

      eng_scene_phase_wait(system->scene);
      eng_scene_phase_set(system->scene, SYSTEM_ENTITY_UPDATE);

      for(id = 0; id < population; id++)
      {
         eng_scene_fence_increment(system->scene);
         worker->component_module_targets = NULL;
         //ecore_thread_run(_eng_system_entity_update_state_task, NULL, NULL, worker);
         _eng_system_entity_update_state_task(worker, NULL);
        worker++;
      }
      worker -= population;

      if(eina_inarray_count(pd->component_outputs))
      {
         eng_scene_phase_wait(system->scene);
         eng_scene_phase_set(system->scene, SYSTEM_ENTITY_EXPORT);

         for(id = 0; id < population; id++)
         {
            eng_scene_fence_increment(system->scene);
            worker->component_module_targets = pd->component_outputs;
            //ecore_thread_run(_eng_system_entity_export_state_task, NULL, NULL, worker);
            _eng_system_entity_export_state_task(worker, NULL);
            worker++;
         }
         worker -= population;
      }

      for(id = 0; id < population; id++) { eng_queue_free(worker->handoff_buffer); worker++; }
      worker -= population;
   }

   eng_scene_phase_wait(system->scene);
   eng_scene_phase_set(system->scene, SYSTEM_GLOBAL_UPDATE);

   _eng_system_dispatch_events(system);
   _eng_system_update(system->entity, system);

   eng_scene_phase_wait(system->scene);
   eng_scene_phase_set(system->scene, SYSTEM_CLEANUP);

   eng_queue_flush(pd->entity_state_buffer);
   eina_inarray_free(workers);
}

/*** Target Entity Methods ***/

static int
_eng_system_entity_sort(const void *data1, const void *data2)
{
   const EntityID *a = data1, *b = data2;

   if(*a < *b) return -1;
   if(*a > *b) return  1;
   return 0;
}

EOLIAN static Eina_Bool
_eng_system_entity_query(Eo *obj EINA_UNUSED, Eng_System_Data *pd,
        EntityID entity)
{
   if(eina_inarray_search_sorted(pd->entity_targets, &entity, _eng_system_entity_sort) != -1)
        return EINA_TRUE;
   else return EINA_FALSE;
}

// Add a target Entity to be processed by this system. This should only be called by an
//    Engineer_Component.so module.  It happends during system event dispatch.
EOLIAN static void
_eng_system_entity_add(Eo *obj, Eng_System_Data *pd,
        EntityID entity)
{
   uint32_t                    total;
   Eng_Component_Class_Prereq *prereq;
   Eo                         *module;
   Entity_State                initial;
   System_State               *system = pd->global_state;
   void                       *state;

   if(_eng_system_entity_query(obj, pd, entity)) return;

   total = eina_inarray_count(pd->component_prereqs);
   for(uint32_t current = 0; current < total; current++)
   {
      prereq = eina_inarray_nth(system->class->component_prereqs, current);
      module = *(Eo**)eina_inarray_nth(pd->component_prereqs,     current);
      state  = prereq->class->state(module);
      if(prereq->class->index_get(state, entity) == (Indx)-1) return;
   }

   eina_inarray_insert_sorted(pd->entity_targets, &entity, _eng_system_entity_sort);

   printf("System Target Added | EntityID: %ld, Class: %s\n", entity, system->class->symbol_echo());

   initial.id = entity;
   _eng_system_entity_transport_state(
      eina_inarray_nth(system->class->component_inputs, 0),
      eina_inarray_nth(pd->component_inputs,            0),
      &initial,
      EINA_TRUE,
      EINA_TRUE);
   _eng_system_entity_start(entity, system, &initial);
   _eng_system_entity_transport_state(
      eina_inarray_nth(system->class->component_inputs, 0),
      eina_inarray_nth(pd->component_inputs,            0),
      &initial,
      EINA_FALSE,
      EINA_TRUE);
}

// If an Entity loses a Component this System requires, remove it from the target list.
EOLIAN static void
_eng_system_entity_remove(Eo *obj EINA_UNUSED, Eng_System_Data *pd,
        EntityID entity)
{
   Indx index;

   if((index = eina_inarray_search_sorted(pd->entity_targets, &entity, _eng_system_entity_sort)))
      eina_inarray_remove_at(pd->entity_targets, index);
}

#include <eng_system.eo.c>

undefine(`PREREQS')dnl
undefine(`GLOBAL')dnl
undefine(`ENTITY')dnl

