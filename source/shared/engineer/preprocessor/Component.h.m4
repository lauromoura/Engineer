divert(-1)
changecom(/*,*/)dnl
define(`HEADER', `include(SOURCE_PATH/Component.h)')
define(`COMPONENT_STRUCTS', `dnl
typedef struct
{
   define(`FIELD', $`'1_SOA $`'2;)dnl
   define(`ARRAY', Eina_Inarray *$`'2;)dnl
   ifdef(`STATE_PAYLOAD', STATE_PAYLOAD, `')dnl
}
ASSET_SYMBOL`_'DOMAIN_SYMBOL`_Vault';

typedef struct
{
   define(`FIELD', $`'1 $`'2;)dnl
   define(`ARRAY', State_Array *$`'2;)dnl
   ifdef(`STATE_PAYLOAD', STATE_PAYLOAD, `')dnl
}
ASSET_SYMBOL`_'DOMAIN_SYMBOL`_State';
')
define(`STATE', `define(`STATE_PAYLOAD', `$1') COMPONENT_STRUCTS dnl')

divert(0)dnl
HEADER

undefine(`STATE')dnl
undefine(`COMPONENT_STRUCTS')dnl

