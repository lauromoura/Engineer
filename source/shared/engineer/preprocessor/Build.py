#!/usr/bin/env python3

import os, sys, subprocess

asset  = sys.argv[1]
domain = sys.argv[2]
target = sys.argv[3]
share  = sys.argv[4]
source = sys.argv[5]
out_h  = sys.argv[6]
out_c  = sys.argv[7]

if os.path.isfile(os.path.join(source, target + ".h")):

   src_h = open(out_h, "w+")
   subprocess.call([ "m4",
                     "-DSOURCE_PATH="   + source,
                     "-DASSET_SYMBOL="  + asset,
                     "-DDOMAIN_SYMBOL=" + domain,
                     os.path.join(share, target + ".h.m4")],
                     stdout = src_h)
   src_h.close()

   src_c = open(out_c,"w+")
   subprocess.call([ "m4",
                     "-DSOURCE_PATH="   + source,
                     "-DASSET_SYMBOL="  + asset,
                     "-DDOMAIN_SYMBOL=" + domain,
                     os.path.join(share, target + ".c.m4")],
                     stdout = src_c)
   src_c.close()

