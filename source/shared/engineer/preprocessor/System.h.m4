divert(-1)
changecom(/*,*/)
define(`HEADER', `include(SOURCE_PATH/System.h)')
define(`SYSTEM_STRUCTS', `dnl
typedef struct
{
   EntityID          entity; // Each System is represented in the Scene with an Entity for notification

   Eng_System_Class *class;
   Eng_Scene        *scene;

   struct
   {
   divert(-1)
   define(`PREREQS', $`'1)
   define(`ASSET',
   struct
   {
      $`'1_$`'2
   }
   $`'1;)
   define(`SYSTEM', $`'1_System_State *$`'1;)
   divert(0)dnl
   ifdef(`PREREQS_PAYLOAD', PREREQS_PAYLOAD, `')dnl
   }
   prereq;

   divert(-1)
   define(`STATE',  $`'*)
   define(`FIELD',  $`'1 $`'2;)
   define(`QUEUE',  $`'1 $`'2;)
   define(`EVENTS', `dnl')

   divert(0)dnl
   ifdef(`GLOBAL_PAYLOAD', GLOBAL_PAYLOAD, `')dnl
}
ASSET_SYMBOL`_'DOMAIN_SYMBOL`_System_State';

typedef struct
{
   EntityID      id;
   Eng_Queue    *outbox;

   divert(-1)
   define(`INPUTS',   $`'*)
   define(`OUTPUTS', `dnl')
   define(`EVENTS',  `dnl')

   define(`ASSET', dnl
   struct
   {
      $`'2
   }
   $`'1;)
   define(`COMPONENT', dnl
   struct
   {
      $`'2
   }
   $`'1;)
   define(`FIELD', $`'1 $`'2;)

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', ENTITY_PAYLOAD, `')dnl

   divert(-1)
   define(`INPUTS',  `dnl')
   define(`OUTPUTS', `dnl')
   define(`EVENTS',   $`'*)
   define(`EVENT',   `dnl')

   divert(0)dnl
   struct
   {
      ifdef(`ENTITY_PAYLOAD', ENTITY_PAYLOAD, `')dnl
   }
   event;
}
ASSET_SYMBOL`_'DOMAIN_SYMBOL`_Entity_State';
')
define(`SYMBOL',
   `define(`ASSET_PAYLOAD', `$1')'
   `define(`LABEL_PAYLOAD', `$2')'
)
define(`PREREQS', `define(`PREREQS_PAYLOAD', `$*')')
define(`GLOBAL',  `define(`GLOBAL_PAYLOAD',  `$*')')
define(`ENTITY',  `define(`ENTITY_PAYLOAD',  `$*') SYSTEM_STRUCTS dnl')
divert(0)dnl
HEADER

