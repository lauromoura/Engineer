divert(-1)
changecom(/*,*/)dnl
define(`HEADER', `include(SOURCE_PATH/Component.h)')

define(`STATE', `define(`STATE_PAYLOAD', $1)dnl')dnl
HEADER

divert(0)dnl
#define Component_Vault ASSET_SYMBOL`_'DOMAIN_SYMBOL`_Vault'
#define Component_State ASSET_SYMBOL`_'DOMAIN_SYMBOL`_State'

#include "Component.h"

typedef enum
{
   STATE_null
   define(`FIELD', ``,''STATE_$2)dnl
   define(`ARRAY', ``,''STATE_$2)dnl
   ifdef(`STATE_PAYLOAD', STATE_PAYLOAD, `')dnl
}
Eng_Component_State_Key;

void *
_eng_component_vault_init()
{
   Component_Vault *vault = calloc(1, sizeof(Component_Vault));

   divert(-1)
   define(`FIELD', $1_NEW(&vault->$2);)
   define(`ARRAY', vault->$2 = eina_inarray_new(sizeof(State_Array), 0);)

   divert(0)dnl
   STATE_PAYLOAD

   return vault;
}

/*** Class Methods ***/

EAPI String *
eng_component_symbol_echo()
{
   return `"'ASSET_SYMBOL`.'DOMAIN_SYMBOL`.'Component`"';
}

EAPI String *
eng_component_asset_echo()
{
   return `"'ASSET_SYMBOL`"';
}

EAPI String *
eng_component_label_echo()
{
   return `"'DOMAIN_SYMBOL`"';
}

EAPI uint32_t
eng_component_state_sizeof()
{
   return sizeof(Component_State);
}

EAPI Eina_Hash *
eng_component_field_enumerator_new()
{
   Eina_Hash *state = eina_hash_int64_new(NULL);
   Indx       index;
   uint64_t   hash;
   divert(-1)
   define(`FIELD',
   index = STATE_$2;
   hash  = eng_hash_murmur3(`"'ASSET_SYMBOL`.'DOMAIN_SYMBOL`.'Component.$2`"');
   eina_hash_set(state, &hash, (void*)(uint64_t)(index + 1));
   )
   define(`ARRAY', FIELD($1, $2))
   divert
   STATE_PAYLOAD()dnl
   return state;
}

EAPI String *
eng_component_field_label_echo(Indx enumerator)
{
   switch(enumerator)
   {
      divert(-1)
      define(`FIELD',
      case STATE_$2 :
         return "$2";
      break;
      )
      define(`ARRAY', FIELD($1, $2))
      divert`'dnl
      STATE_PAYLOAD()dnl
      default : return "Index Undefined";
   }
}

EAPI String *
eng_component_field_symbol_echo(Indx enumerator)
{
   switch(enumerator)
   {
      divert(-1)
      define(`FIELD',
      case STATE_$2 :
         return `"'ASSET_SYMBOL`.'DOMAIN_SYMBOL`.'Component.$2`"';
      break;
      )
      define(`ARRAY', FIELD($1, $2))
      divert`'dnl
      STATE_PAYLOAD()dnl
      default : return "Index Undefined";
   }
}


/*** Component Services ***/

EAPI void // Should be static
_eng_component_index_set(Eng_Component_Data *pd,
        EntityID entityid, Indx index)
{
    index += 1;
    eina_hash_add(pd->lookup, &entityid, (void*)(uintptr_t)index);
}

EAPI Indx
eng_component_index_get(Eng_Component_Data *pd,
        EntityID target)
{
   Indx index  = (Indx)(uintptr_t)eina_hash_find(pd->lookup, &target);
   index--;

   return index;
}

EAPI void // Should be static
_eng_component_push(Eng_Component_Data *pd, EntityID parent, Data *initial)
{
   Component_Vault *vault    = (Component_Vault*)pd->vault;
   Component_State *template = (Component_State*)initial;
   Indx             index;

   index = eina_inarray_push(pd->parent, &parent);
   _eng_component_index_set(pd, parent, index);

   // Push our awake() processed template data to the active frames.
   define(`FIELD', $1_PUSH(&vault->$2, &template->$2);)
   /* define(`ARRAY', eng_state_array_setup(eina_inarray_grow(vault->$2, 1), sizeof($1));) */
   STATE_PAYLOAD
}

EAPI Eina_Bool
eng_component_attach_event(Eo *module,
        EntityID parent, Data *template)
{
   Eng_System_Module_Entry *target;
   Eng_Component_Data      *pd;

   pd = efl_data_scope_get(module, ENG_COMPONENT_CLASS);

   if(eng_component_index_get(pd, parent) == (Indx)-1)
   {
      _eng_component_push(pd, parent, template);

      printf("Component Attached. | EntityID: %ld, Class: %s\n", parent, pd->class->symbol_echo());

      // Add the Parent Entity to the target list of each relevant dependent System.
      EINA_INARRAY_FOREACH(pd->dependents, target)
         target->class->entity_add(target->module, parent);
   }

   return EINA_TRUE;
}

EAPI void
eng_component_state_export(Eng_Component_Data *pd,
        Indx field, EntityID entity, Data *state)
{
   Component_Vault *vault = pd->vault;
   Indx             index = eng_component_index_get(pd, entity);

   switch(field)
   {
      divert(-1)
      define(`FIELD', case STATE_$2 : $1_READ(&vault->$2, ($1*)state, index); break;)
      define(`ARRAY', `')
      divert(0)dnl
      STATE_PAYLOAD
   }
}

EAPI void
eng_component_state_import(Eng_Component_Data *pd,
        Indx field, EntityID entity, Data *state)
{
   Component_Vault *vault = pd->vault;
   Indx             index = eng_component_index_get(pd, entity);

   switch(field)
   {
      divert(-1)
      define(`FIELD', case STATE_$2 : $1_WRITE(&vault->$2, ($1*)state, index); break;)
      define(`ARRAY', `')

      divert(0)dnl
      STATE_PAYLOAD
   }
}

EAPI Data *
eng_component_state_query(Eng_Component_Data *pd,
        EntityID target, String *field)
{
   Component_Vault     *vault = pd->vault;
   String              *key;
   uint64_t             field_id, index;
   void                *result;
   Eng_Component_State_Key field_key;

   key   = eina_stringshare_printf("%s.%s", eng_component_symbol_echo(), field);

   field_id = eng_hash_murmur3(key);

   field_key = (uint64_t)eina_hash_find(pd->class->field_enumerator, &field_id) - 1;
   index     = eng_component_index_get(pd, target);
   switch(field_key)
   {
      case STATE_null:
         result = NULL;
         printf("Component State Null Return.\n");
      break;
      divert(-1)
      define(`FIELD',
         case STATE_$2 :
            result = malloc(sizeof($1));
            $1_READ(&vault->$2, result, index);
         break;
      )
      define(`ARRAY', `')
      divert(0)dnl
      STATE_PAYLOAD

      default:
         result = NULL;
      break;
   }

   return result;
}


/*** Eo Methods ***/

EAPI Efl_Object *
eng_component_new(Eo *parent, Eng_Component_Class *class)
{
   Eo *obj;

   efl_domain_current_push(EFL_ID_DOMAIN_SHARED);
   obj = efl_add(ENG_COMPONENT_CLASS, parent);
   efl_domain_current_pop();

   eng_component_eng_class_set(obj, class);

   return obj;
}

EOLIAN static Efl_Object *
_eng_component_efl_object_constructor(Eo *obj, Eng_Component_Data *pd EINA_UNUSED)
{
   return efl_constructor(efl_super(obj, ENG_COMPONENT_CLASS));
}

EOLIAN static Efl_Object *
_eng_component_efl_object_finalize(Eo *obj, Eng_Component_Data *pd)
{
   obj = efl_finalize(efl_super(obj, ENG_COMPONENT_CLASS));

   printf("Component Module Created.  Pointer: %p\n", obj);

   pd->dependents = eina_inarray_new(sizeof(Eng_System_Module_Entry), 0);

   pd->lookup = eina_hash_int64_new(NULL);
   pd->parent = eina_inarray_new(sizeof(EntityID), 0);
   pd->vault  = _eng_component_vault_init();

   return obj;
}

EOLIAN static void
_eng_component_efl_object_destructor(Eo *obj, Eng_Component_Data *pd EINA_UNUSED)
{
   efl_destructor(efl_super(obj, ENG_COMPONENT_CLASS));
}

static int64_t
_target_compare(Eng_System_Module_Entry *a, Eng_System_Module_Entry *b)
{
   if ((Sclr)a->class->id > (Sclr)b->class->id) return  1;
   if ((Sclr)a->class->id < (Sclr)b->class->id) return -1;
   return 0;
}

EOLIAN static void
_eng_component_eng_class_set(Eo *obj EINA_UNUSED, Eng_Component_Data *pd,
        Eng_Component_Class *class)
{
   if(class) pd->class = class;
}

EOLIAN static Eng_Component_Class *
_eng_component_eng_class_get(Eo *obj EINA_UNUSED, Eng_Component_Data *pd)
{
   return pd->class;
}

// This is run when a System_Module Eo that depends on this Component_Module Eo is loaded into a
//    Scene. It adds a refcount to the Component that will insure that it will not be unloaded until
//    all the System_Modules that depend on it are unloaded first.
EOLIAN static Indx
_eng_component_dependent_register(Eo *obj EINA_UNUSED, Eng_Component_Data *pd,
        uint64_t system_key)
{
   Eng_Scene               *scene;
   Eng_System_Module_Entry  target;
   Eina_Compare_Cb          comparator;
   Indx                     result;

   comparator = (Eina_Compare_Cb)_target_compare;

   scene         = efl_parent_get(obj);
   target.class  = eng_node_system_class_query_loaded_by_id(eng_node(), system_key);
   target.module = eng_scene_system_module_query_loaded_by_id(scene,    system_key);
   if(!target.class || !target.module)
      { printf("Failed to register Scene as Component dependent target.class = %p, target.module = %p!\n", target.class, target.module); return -1; }

   result = eina_inarray_search_sorted(pd->dependents, &target, comparator);
   if(result == (Indx)-1) eina_inarray_insert_sorted(pd->dependents, &target, comparator);

   return eina_inarray_count(pd->dependents);
}

EOLIAN static Indx
_eng_component_dependent_deregister(Eo *obj EINA_UNUSED, Eng_Component_Data *pd,
        uint64_t system_key)
{
   Eng_Scene               *scene;
   Eng_System_Module_Entry  target;
   Eina_Compare_Cb          comparator;
   Indx                     result;

   comparator = (Eina_Compare_Cb)_target_compare;

   scene = efl_parent_get(obj);
   target.class  = eng_node_system_class_query_loaded_by_id(eng_node(), system_key);
   target.module = eng_scene_system_module_query_loaded_by_id(scene,    system_key);
   if(!target.class || !target.module) return -1;

   result = eina_inarray_search_sorted(pd->dependents, &target, comparator);
   if(result != (Indx)-1) eina_inarray_remove_at(pd->dependents, result);

   return eina_inarray_count(pd->dependents);
}

EOLIAN static Data *
_eng_component_state(Eo *obj EINA_UNUSED, Eng_Component_Data *pd)
{
   return (Data*)pd;
}

#include <eng_component.eo.c>

undefine(`STATE')dnl
undefine(`LABEL')dnl

