#ifndef _CORE_INPUT_SYSTEM_H_
#define _CORE_INPUT_SYSTEM_H_

#include <eng_system.h>

SYMBOL(Core, Input)

PREREQS()

GLOBAL(
   STATE(
      FIELD(Byte, w)
      FIELD(Byte, a)
      FIELD(Byte, s)
      FIELD(Byte, d)
      FIELD(Byte, q)
      FIELD(Byte, e)

      FIELD(Vec2, delta)
   )
   EVENTS(
      EVENT(input_flush,     0)
      EVENT(keyboard_listen, 8)
      EVENT(keyboard_ignore, 8)
      EVENT(mouse_listen,    8)
      EVENT(mouse_ignore,    8)
   )
)

ENTITY(
   INPUTS(
      ASSET(Core,
         COMPONENT(Input_Listener,
            FIELD(Sclr, context)
         )
      )
   )
   OUTPUTS(
   )
   EVENTS(
   )
)

enum Key_State
{
   KEY_IDLE     = 0,
   KEY_DOWN     = 1,
   KEY_HELD     = 2,
   KEY_RELEASED = 4,
};

#endif

