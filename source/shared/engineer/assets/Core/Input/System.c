#include "System.h"

void
system_start(System_State *system)
{
   printf("System Start Checkpoint. System: Input.\n");

   //system_notify(this, "input_flush", NULL);
}

void
system_update(System_State *system)
{
   printf("System Update Checkpoint. System: Input.\n");

   system_notify(this, "input_flush", NULL);
}

void
system_stop(System_State *system)
{
}

static void
_key_down_eo_cb(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Key_Down *event  = event_info;
   System_State        *system = data;

   if(!strncmp(event->keyname, "a", 16)) system->a |= KEY_DOWN;
   if(!strncmp(event->keyname, "d", 16)) system->d |= KEY_DOWN;
   if(!strncmp(event->keyname, "e", 16)) system->e |= KEY_DOWN;
   if(!strncmp(event->keyname, "q", 16)) system->q |= KEY_DOWN;
   if(!strncmp(event->keyname, "s", 16)) system->s |= KEY_DOWN;
   if(!strncmp(event->keyname, "w", 16)) system->w |= KEY_DOWN;
}

static void
_delta_set_eo_cb(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Mouse_Move *event  = event_info;
   System_State          *system = data;

   system->delta.x += BASIS * (event->cur.output.x - event->prev.output.x);
   system->delta.y += BASIS * (event->cur.output.y - event->prev.output.y);
}

void
system_event(input_flush, System_State *system, Data *data)
{
   system->a = KEY_IDLE;
   system->d = KEY_IDLE;
   system->e = KEY_IDLE;
   system->q = KEY_IDLE;
   system->s = KEY_IDLE;
   system->w = KEY_IDLE;

   system->delta.x = 0;
   system->delta.y = 0;
}

void
system_event(keyboard_listen, System_State *system, Data *data)
{
   Eo *target = *(Eo**)data;

   evas_object_event_callback_add(target, EVAS_CALLBACK_KEY_DOWN, _key_down_eo_cb, system);
}

void
system_event(keyboard_ignore, System_State *system, Data *data)
{
   Eo *target = *(Eo**)data;

   evas_object_event_callback_del_full(target, EVAS_CALLBACK_KEY_DOWN, _key_down_eo_cb, system);
}

void
system_event(mouse_listen, System_State *system, Data *data)
{
   Eo *target = *(Eo**)data;

   evas_object_event_callback_add(target, EVAS_CALLBACK_MOUSE_MOVE, _delta_set_eo_cb, system);
}

void
system_event(mouse_ignore, System_State *system, Data *data)
{
   Eo *target = *(Eo**)data;

   evas_object_event_callback_del_full(target, EVAS_CALLBACK_MOUSE_MOVE, _delta_set_eo_cb, system);
}

void
entity_start(Entity_State *entity)
{
}

void
entity_update(Entity_State *entity)
{
   printf("Entity Update Checkpoint. EntityID: %ld\n", entity->id);

   if(system->w & KEY_DOWN) entity_notify(this, "move_forward",  NULL);
   if(system->a & KEY_DOWN) entity_notify(this, "move_left",     NULL);
   if(system->s & KEY_DOWN) entity_notify(this, "move_rearward", NULL);
   if(system->d & KEY_DOWN) entity_notify(this, "move_right",    NULL);
   if(system->q & KEY_DOWN) entity_notify(this, "roll_left",     NULL);
   if(system->e & KEY_DOWN) entity_notify(this, "roll_right",    NULL);

   if(system->delta.x > 0) entity_notify(this, "yaw_left",       &system->delta);
   if(system->delta.x < 0) entity_notify(this, "yaw_right",      &system->delta);
   if(system->delta.y > 0) entity_notify(this, "pitch_forward",  &system->delta);
   if(system->delta.y < 0) entity_notify(this, "pitch_rearward", &system->delta);
}

void
entity_stop(Entity_State *entity)
{
}



