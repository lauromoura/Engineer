#ifndef _CORE_COMPONENT_TRANSFORM_H_
#define _CORE_COMPONENT_TRANSFORM_H_

#include <eng_component.h>

STATE(
   FIELD(Vec3, position)
   FIELD(Sclr, poslock)
   FIELD(Quat, orientation)
   FIELD(Sclr, orilock)
   FIELD(Vec3, bounds)
)

#endif


