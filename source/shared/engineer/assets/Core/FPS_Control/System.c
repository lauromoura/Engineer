#include "System.h"

void system_start(System_State *system)
{
   printf("System Start Checkpoint. System: FPS_Control.\n");
}

void system_update(System_State *system)
{
   printf("System Update Checkpoint. System: FPS_Control.\n");
}

void system_stop(System_State *system)
{
}

void entity_start(Entity_State *entity)
{
}

void entity_update(Entity_State *entity)
{
   Quat orientation     = entity->Core.Transform.orientation;
   Quat orientation_inv = eng_math_quat_invert(orientation);
   Quat rotation;

   if(entity->event.torque.yaw   ||
      entity->event.torque.pitch ||
      entity->event.torque.roll   )
   {
      rotation    = eng_math_eulr_to_quat(entity->event.torque);
      orientation = eng_math_quat_multiply(rotation, orientation);
      orientation = eng_math_quat_normalize(orientation);

      entity->Core.Transform.orientation = orientation;
   }

   if(entity->event.translation.x ||
      entity->event.translation.y ||
      entity->event.translation.z  )
   {
      Quat vector = { 0,
                      entity->event.translation.x,
                      entity->event.translation.y,
                      entity->event.translation.z };

      vector = eng_math_quat_multiply(orientation_inv, vector);
      vector = eng_math_quat_multiply(vector,          orientation);

      entity->Core.Transform.position.x += vector.x;
      entity->Core.Transform.position.y += vector.y;
      entity->Core.Transform.position.z += vector.z;
   }
}

void entity_stop(Entity_State *entity)
{
}

static void
entity_event(move_forward, Entity_State *entity, Data *data)
{
   entity->event.translation.z += (Sclr)(-0.25 * BASIS); // W
}

static void
entity_event(move_rearward, Entity_State *entity, Data *data)
{
   entity->event.translation.z += (Sclr)( 0.25 * BASIS); // S
}

static void
entity_event(move_left, Entity_State *entity, Data *data)
{
   entity->event.translation.x += (Sclr)(-0.25 * BASIS); // A
}

static void
entity_event(move_right, Entity_State *entity, Data *data)
{
   entity->event.translation.x += (Sclr)( 0.25 * BASIS); // D
}

static void
entity_event(roll_left, Entity_State *entity, Data *data)
{
   entity->event.torque.roll = (1 << 10); // Q
}

static void
entity_event(roll_right, Entity_State *entity, Data *data)
{
   entity->event.torque.roll = -(1 << 10); // E
}

static void
entity_event(yaw_left, Entity_State *entity, Data *data)
{
   Vec2 delta = *(Vec2*)data;

   entity->event.torque.yaw = delta.x / 128; // Mouse movement to the left.
}

static void
entity_event(yaw_right, Entity_State *entity, Data *data)
{
   Vec2 delta = *(Vec2*)data;

   entity->event.torque.yaw = delta.x / 128; // Mouse movement to the right;
}

static void
entity_event(pitch_forward, Entity_State *entity, Data *data)
{
   Vec2 delta = *(Vec2*)data;

   entity->event.torque.pitch = delta.y / 128; // Mouse movement up.
}

static void
entity_event(pitch_rearward, Entity_State *entity, Data *data)
{
   Vec2 delta = *(Vec2*)data;

   entity->event.torque.pitch = delta.y / 128; // Mouse movement down.
}

