#ifndef _VOXEL_RENDER_SYSTEM_H_
#define _VOXEL_RENDER_SYSTEM_H_

#include <eng_system.h>
#include "Voxel/Broadphase/System.h"

SYMBOL(Voxel, Render)

PREREQS(
   ASSET(Voxel,
     SYSTEM(Broadphase)
   )
)

GLOBAL(
)

ENTITY(
   INPUTS(
      ASSET(Core,
         COMPONENT(Transform,
            FIELD(Vec3, position)
            FIELD(Quat, orientation)
         )
         COMPONENT(Camera,
            FIELD(Pntr, glviews)
         )
      )
   )
   OUTPUTS(
   )
   EVENTS(
      EVENT(viewport_attach, 8)
   )
)

typedef struct
{
   // GPU Program, Shader & Buffer Objects
   GLuint program;

   GLuint canvas_shader;
   GLuint object_shader;

   GLuint canvas_buffer; // vbo
   GLuint object_buffer; // ssbo

   // Render Metadata
   GLfloat resolution[2];
   GLuint  resolution_gpuid;

   // Environment Data
   GLfloat light[3];
   GLuint  light_gpuid;
   GLfloat time;

   // Target Data
   GLuint        count;
   GLuint        count_gpuid;
   Eina_Inarray *objects;       // Stores the visible collider/shader_data sent in by the camera.
   GLuint        objects_gpuid;
}
Voxel_Render_Buffer;

static void
_voxel_render_init_gl(Evas_Object *obj);

static void
_voxel_render_del_gl(Evas_Object *obj);

static void
_voxel_render_resize_gl(Evas_Object *obj);

static void
_voxel_render_draw_gl(Evas_Object *obj);

static void
_voxel_render_frustum_sweep(Voxel_Render_System_State *system, Voxel_Render_Entity_State *camera, Elm_Glview *glview);
/*
static void
_voxel_render_object_add(Voxel_Render_Buffer *buffer, EntityID entity);

static void
_voxel_render_object_flush(Voxel_Render_Buffer *buffer);

static void
_voxel_render_frustum_sweep(Eo *obj, Engineer_Scene_Data *pd, Voxel_Render_Buffer *buffer);
*/
#endif
