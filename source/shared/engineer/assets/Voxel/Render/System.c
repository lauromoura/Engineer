#include "System.h"

void system_start(System_State *system)
{
   printf("System Start Checkpoint. System: Render.\n");
}

void system_update(System_State *system)
{
   printf("System Update Checkpoint. System: Render.\n");
}

void system_stop(System_State *system)
{
}

void
entity_start(Entity_State *entity)
{
   // Get the address of our Transform.
   printf("Render/Camera Start Checkpoint.\n");

   entity->Core.Camera.glviews = (Pntr)eina_hash_pointer_new(NULL);
}

struct _gl_data
{
   System_State *system;
   Entity_State *entity;
};

static Eina_Bool
_entity_update_glview(const Eina_Hash *hash EINA_UNUSED, const void *key EINA_UNUSED,
        void *data, void *fdata)
{
   struct _gl_data *gldata = fdata;

   Elm_Glview   *glview = data;

   _voxel_render_frustum_sweep(gldata->system, gldata->entity, glview);
   elm_glview_changed_set(glview);

   return EINA_TRUE;
}

// Our render system in this case is our frustum broadphase.
void
entity_update(Entity_State *entity)
{
   struct _gl_data data;

   data.system = (System_State*)system;
   data.entity = entity;

   eina_hash_foreach((Eina_Hash*)entity->Core.Camera.glviews, _entity_update_glview, &data);
}

void entity_stop(Entity_State *entity EINA_UNUSED)
{
}

static bool
entity_event(viewport_attach, Entity_State *entity, void *payload)
{
   Elm_Box   *viewport = *(Eo**)payload;
   Voxel_Render_Buffer *buffer   = calloc(1, sizeof(Voxel_Render_Buffer));
   //efl_domain_current_push(EFL_ID_DOMAIN_SHARED);
   Elm_Glview          *glview   = elm_glview_version_add(viewport, EVAS_GL_GLES_3_X);
   //efl_domain_current_pop();

   if(buffer && glview)
   {
      buffer->time    = 0;
      buffer->objects = eina_inarray_new(sizeof(Collider_Data), 0);

      elm_glview_mode_set(glview, 0
         | ELM_GLVIEW_ALPHA
         | ELM_GLVIEW_DEPTH
         | ELM_GLVIEW_DIRECT
      );

      elm_glview_render_policy_set(glview, ELM_GLVIEW_RENDER_POLICY_ON_DEMAND);
      elm_glview_resize_policy_set(glview, ELM_GLVIEW_RESIZE_POLICY_RECREATE);

      evas_object_size_hint_align_set(glview, EVAS_HINT_FILL, EVAS_HINT_FILL);
      evas_object_size_hint_weight_set(glview, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);

      elm_glview_init_func_set(glview,   _voxel_render_init_gl);
      elm_glview_del_func_set(glview,    _voxel_render_del_gl);
      elm_glview_resize_func_set(glview, _voxel_render_resize_gl);
      elm_glview_render_func_set(glview, _voxel_render_draw_gl);

      efl_key_data_set(glview, "Voxel_Render_Buffer", buffer);
      elm_box_pack_end(viewport, glview);
      evas_object_show(glview);

      eina_hash_set((Eina_Hash*)entity->Core.Camera.glviews, &viewport, glview);

    return EINA_TRUE;
   }
   else
   {
      Evas_Object *label;

      //efl_domain_current_push(EFL_ID_DOMAIN_SHARED);
      label = elm_label_add(viewport);
      elm_box_pack_end(viewport, label);
      //efl_domain_current_pop();

      elm_object_text_set(label,
         "<align=left> GL backend engine is not supported.<br/>"
         " 1. Check your back-end engine or<br/>"
         " 2. Run elementary_test with engine option or<br/>"
         "    ex) $ <b>ELM_ACCEL=gl</b> elementary_test<br/>"
         " 3. Change your back-end engine from elementary_config.<br/></align>");
      evas_object_size_hint_weight_set(label, 0.0, 0.0);
      evas_object_size_hint_align_set(label, EVAS_HINT_FILL, EVAS_HINT_FILL);

      printf("viewport_attach Checkpoint. 2b-2.\n");

      evas_object_show(label);

      return EINA_FALSE;
   }
}

static void
_voxel_render_frustum_sweep(System_State *system, Entity_State *camera, Elm_Glview *glview)
{
   Voxel_Render_Buffer           *buffer;
   Voxel_Broadphase_Entity_State *target;

   Collider_Data collider;
   Quat          viewdirection, viewlocation, orientation, orientationinv;
   GLfloat       invbasis;

   buffer   = efl_key_data_get(glview, "Voxel_Render_Buffer");
   eina_inarray_flush(buffer->objects);

   invbasis = (GLfloat)1 / BASIS;
   orientation    = camera->Core.Transform.orientation;
   orientationinv = eng_math_quat_invert(orientation);

   // Set Global lighting vector for this Camera.
   Quat lightvector = { 0.0, BASIS * 0.57703, BASIS * 0.57703, BASIS * 0.57703 };
   lightvector = eng_math_quat_multiply(orientation, lightvector);
   lightvector = eng_math_quat_multiply(lightvector, orientationinv);

   buffer->light[0] = (float)lightvector.x / BASIS;
   buffer->light[1] = (float)lightvector.y / BASIS;
   buffer->light[2] = (float)lightvector.z / BASIS;

   EINA_INARRAY_FOREACH((Eina_Inarray*)system->prereq.Voxel.Broadphase->graph, target)
   {
      // The Scenespace to Viewspace conversion happens here.
      viewlocation.w = 0;
      viewlocation.x = (target->Core.Transform.position.x - camera->Core.Transform.position.x);
      viewlocation.y = (target->Core.Transform.position.y - camera->Core.Transform.position.y);
      viewlocation.z = (target->Core.Transform.position.z - camera->Core.Transform.position.z);

      viewlocation  = eng_math_quat_multiply(orientation,  viewlocation);
      viewlocation  = eng_math_quat_multiply(viewlocation, orientationinv);
      viewdirection = eng_math_quat_multiply(orientation,  target->Core.Transform.orientation);

      // The Viewport needs data in floats.
      collider.type = target->Voxel.Collider.shape;

      collider.size[0] = target->Voxel.Collider.bounds.x * invbasis;
      collider.size[1] = target->Voxel.Collider.bounds.y * invbasis;
      collider.size[2] = target->Voxel.Collider.bounds.z * invbasis;

      collider.color[0] = (GLfloat)(unsigned char)target->Voxel.Collider.color.red   / 255.00;
      collider.color[1] = (GLfloat)(unsigned char)target->Voxel.Collider.color.green / 255.00;
      collider.color[2] = (GLfloat)(unsigned char)target->Voxel.Collider.color.blue  / 255.00;

      collider.location[0] = viewlocation.x * invbasis;
      collider.location[1] = viewlocation.y * invbasis;
      collider.location[2] = viewlocation.z * invbasis;

      collider.orientation[0] = viewdirection.x * invbasis;
      collider.orientation[1] = viewdirection.y * invbasis;
      collider.orientation[2] = viewdirection.z * invbasis;
      collider.orientation[3] = viewdirection.w * invbasis;

      eina_inarray_push(buffer->objects, &collider);
   }
}

static GLuint
load_shader(Eo *obj, GLuint program, GLenum type, const char *path)
{
   GLuint       shader;
   GLint        compiled;
   GLint        info_len;
   FILE        *file;
   uint64_t     size;
   Evas_GL_API *api = elm_glview_gl_api_get(obj);

   // Create a shader object
   shader = api->glCreateShader(type);
   if(!shader) { printf("Cannot create Shader object.\n"); return 0; }

   // Load source code from the specified shader file.
   file = fopen(path, "r");
   if(!file) { printf("Cannot find Shader file.\n"); return 0; }
   fseek(file, 0, SEEK_END);
   size = ftell(file);
   fseek(file, 0, SEEK_SET);
   GLchar buffer[size + 1];
   buffer[size] = '\0';
   fread(buffer, 1, size, file);
   fclose(file);
   const GLchar *source = buffer;
   api->glShaderSource(shader, 1, &source, NULL);

   // Compile the loaded shader source.
   api->glCompileShader(shader);

   // Check to see if compilation is successful.
   api->glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
   if(compiled) { api->glAttachShader(program, shader); return shader; }
   else
   {
      api->glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
      if(info_len > 1)
      {
         char* info_log = malloc(sizeof(char) * info_len);

         api->glGetShaderInfoLog(shader, info_len, NULL, info_log);
         printf("Error compiling shader:\n%s\n======\n%s\n======\n", info_log, buffer);
         free(info_log);
      }
      api->glDeleteShader(shader);
      return 0;
   }
}

// Callbacks
static void
_voxel_render_init_gl(Evas_Object *obj)
{
   Evas_GL_API         *api = elm_glview_gl_api_get(obj);
   Voxel_Render_Buffer *vrb = efl_key_data_get(obj, "Voxel_Render_Buffer");

   if (!vrb) { printf("Unable to get the Voxel Render Buffer.\n"); return; }

   // Create the program object
   vrb->program = api->glCreateProgram();
   if (!vrb->program) { printf("Unable to create the Voxel Program Pipeline"); return; }

   // Load the vertex/fragment shaders
   vrb->canvas_shader = load_shader(obj, vrb->program, GL_VERTEX_SHADER,   "../../../../source/library/shaders/voxel.vert");
   vrb->object_shader = load_shader(obj, vrb->program, GL_FRAGMENT_SHADER, "../../../../source/library/shaders/voxel.frag");

   api->glBindAttribLocation(vrb->program, 0, "position"); // ??

   api->glLinkProgram(vrb->program);
   api->glUseProgram(vrb->program);

   //gld->objects_gpuid    = api->glGetUniformLocation(gld->program, "shader_data");
   vrb->resolution_gpuid = api->glGetUniformLocation(vrb->program, "resolution");
   vrb->count_gpuid      = api->glGetUniformLocation(vrb->program, "count");
   vrb->light_gpuid      = api->glGetUniformLocation(vrb->program, "light_direction");

   GLint linked;
   api->glGetProgramiv(vrb->program, GL_LINK_STATUS, &linked);
   if (!linked)
   {
      GLint info_len = 0;
      api->glGetProgramiv(vrb->program, GL_INFO_LOG_LENGTH, &info_len);
      if (info_len > 1)
      {
         char* info_log = malloc(sizeof(char) * info_len);

         api->glGetProgramInfoLog(vrb->program, info_len, NULL, info_log);
         printf("Error linking Voxel Program Pipeline:\n%s\n", info_log);
         free(info_log);
      }
      api->glDeleteProgram(vrb->program);

      return;
   }

   // Define our "canvas" vertices/quad for our fragment shader.
   GLfloat canvas[] = { -1.0f,  1.0f, 0.0f,
                        -1.0f, -1.0f, 0.0f,
                         1.0f,  1.0f, 0.0f,
                         1.0f, -1.0f, 0.0f };

   // Create our Vertex Buffer Object and stuff our quad into it.
   api->glGenBuffers(1, &vrb->canvas_buffer);
   api->glBindBuffer(GL_ARRAY_BUFFER, vrb->canvas_buffer);
   api->glBufferData(GL_ARRAY_BUFFER, sizeof(canvas), canvas, GL_STATIC_DRAW);
   api->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
   api->glEnableVertexAttribArray(0);

   // Create our Shader Storage Buffer Object for holding our voxel object data.
   api->glGenBuffers(1, &vrb->object_buffer);
   api->glBindBuffer(GL_SHADER_STORAGE_BUFFER, vrb->object_buffer);
   api->glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, vrb->object_buffer);
   api->glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(Collider_Data) * 64, NULL, GL_DYNAMIC_COPY);
   api->glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

static void
_voxel_render_del_gl(Evas_Object *obj)
{
   Evas_GL_API         *api = elm_glview_gl_api_get(obj);
   Voxel_Render_Buffer *vrb = efl_key_data_get(obj, "Voxel_Render_Buffer");

   printf("_voxel_render_del_gl Checkpoint.\n");
   if (!vrb) { printf("Unable to get the Voxel Render Buffer.\n"); return; }

   api->glDeleteShader(vrb->canvas_shader);
   api->glDeleteShader(vrb->object_shader);
   api->glDeleteProgram(vrb->program);
   api->glDeleteBuffers(1, &vrb->object_buffer);

   //efl_key_data_del(obj, "Voxel_Render_Buffer");
}

static void
_voxel_render_resize_gl(Evas_Object *obj)
{
   int w, h;
   Evas_GL_API         *api = elm_glview_gl_api_get(obj);
   Voxel_Render_Buffer *vrb = efl_key_data_get(obj, "Voxel_Render_Buffer");

   printf("_voxel_render_resize_gl Checkpoint.\n");
   if (!vrb) { printf("Unable to get the Voxel Render Buffer.\n"); return; }

   elm_glview_size_get(obj, &w, &h);

   // We can avoid doing this if viewport is all the same as last frame if you want
   api->glViewport(0, 0, w, h);
}

static void
_voxel_render_draw_gl(Evas_Object *obj)
{
   Evas_GL_API         *api = elm_glview_gl_api_get(obj);
   Voxel_Render_Buffer *vrb = efl_key_data_get(obj, "Voxel_Render_Buffer");

   printf("_voxel_render_draw_gl Checkpoint.\n");
   if (!vrb) { printf("Unable to get the Voxel Render Buffer.\n"); return; }

   int width, height;

   elm_glview_size_get(obj, &width, &height);

   api->glViewport(0, 0, width, height);
   api->glClear(GL_COLOR_BUFFER_BIT);

   vrb->resolution[0] = width;
   vrb->resolution[1] = height;

   // Lets set up some test objects to render...
   vrb->count = eina_inarray_count(vrb->objects);

   // Draw a Triangle
   api->glEnable(GL_BLEND);

   api->glUseProgram(vrb->program);

   api->glUniform2fv(vrb->resolution_gpuid, 1, vrb->resolution);
   api->glUniform1ui(vrb->count_gpuid,         vrb->count);
   api->glUniform3fv(vrb->light_gpuid,      1, vrb->light);

   if(vrb->count > 0)
   {
      void *objects = eina_inarray_nth(vrb->objects, 0);
      api->glBindBuffer(GL_SHADER_STORAGE_BUFFER, vrb->object_buffer);
      api->glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(Collider_Data) * vrb->count, objects);
      api->glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
   }

   api->glBindBuffer(GL_ARRAY_BUFFER, vrb->object_buffer);

   api->glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

   // Optional - Flush the GL pipeline
   api->glFinish();
}

/*
static Eina_Bool
_anim(void *data)
{
   // If the game loop has updated the appropriate camera when this animator goes off...
   elm_glview_changed_set(data);
   return ECORE_CALLBACK_RENEW;
}

static void
_close_cb(void *data, Evas_Object *obj EINA_UNUSED,
          void *event_info EINA_UNUSED)
{
   evas_object_del(data);
}

static void
_gl_del_cb(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   ecore_animator_del(data);
}
*/

