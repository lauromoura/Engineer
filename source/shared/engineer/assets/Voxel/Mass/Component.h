#ifndef _VOXEL_COMPONENT_MASS_H_
#define _VOXEL_COMPONENT_MASS_H_

#include <eng_component.h>

STATE(
   FIELD(AABB, bounding_box)

   FIELD(Sclr, mass)
   FIELD(Mtrx, angular_mass)
   FIELD(Vec3, momentum)
   FIELD(Vec3, angular_momentum)
)

#endif

