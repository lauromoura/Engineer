#ifndef _VOXEL_COMPONENT_COLLIDER_H_
#define _VOXEL_COMPONENT_COLLIDER_H_

#include <eng_component.h>

STATE(
   FIELD(Vec3, bounds)
   FIELD(Sclr, shape)
   FIELD(Color, color)
)

#endif

