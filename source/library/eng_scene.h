#ifndef _ENG_SCENE_H_
#define _ENG_SCENE_H_

#include "engineer.h"

typedef enum
{
   SCENE_EVENT_NULL,
   SCENE_EVENT_NOTIFY_FROM_GUI,
   SCENE_EVENT_ENTITY_CREATE,
   SCENE_EVENT_ENTITY_DESTROY,
   SCENE_EVENT_COMPONENT_ATTACH,
   SCENE_EVENT_COMPONENT_DETACH
}
Eng_Scene_Event;

typedef enum
{
   SCENE_UPKEEP,
   SCENE_GLOBAL_UPDATE,
   SCENE_ENTITY_UPDATE,

   SYSTEM_ENTITY_IMPORT,
   SYSTEM_ENTITY_UPDATE,
   SYSTEM_ENTITY_EXPORT,
   SYSTEM_GLOBAL_UPDATE,
   SYSTEM_CLEANUP,

   SCENE_CLEANUP,

   ASYNC_PHASE,
   RENDER_PHASE,
}
Eng_Scene_Phase;

typedef struct
{
   // Scene Metadata.
   EntityID  entity; // A Scene is a System, which is represented inside of itself with an Entity.
   String   *name;   // The name of this scene.
   uint64_t  size;   // Diameter, in the quantum unit.
   uint32_t  shape;  // 0 for a cube, 1 for a sphere.

   Indx          clockrate; // This value controls the FPS the scene is set to run at.
   Ecore_Timer  *iterator;  // Triggers our frame update, the frequency can be changed.
   Eina_Inarray *timecard;  // Stores the completion time for the present and past frames.

   _Atomic Eng_Scene_Phase phase; // Stores what the scene's active phase and subphase is.
   _Atomic Indx            fence; // Stores the outstanding amount of phase child tasks.

   // Entity Messaging Metadata and shortcuts.
   Eina_Hash *scene_keyholes; // Stores event keys for Scene Notice types.

   // Active Entity Data Cache.
   Eina_Hash    *entity_lookup; // Entity index lookup by EntityID.
   Eina_Inarray *entity_id;     // EntityID reverse lookup table.
   Eina_Inarray *entity_status; // Entity Status/refcount lookup.
   Eina_Inarray *entity_inbox;  // Inbox for System scope notices, for each current Entity.
   Eina_Inarray *entity_outbox; // Outbox for System scope notices, for each current Entity.

   Eina_Inarray *system_order;   // Stores the order that the Scene's child Systems are updated in.

   // System and Component Module Efl_Object Tables.
   Eina_Hash *system_modules;    // Stores the System module Eo pointer using murmur3 keys.
   Eina_Hash *component_modules; // Stores the Component module Eo pointer using murmur3 keys.
}
Eng_Scene_Data;

Eo *
eng_scene_new(const char *name);

EAPI void eng_scene_phase_wait(Eo *obj);

Eina_Bool _eng_scene_iterate_cb(void *obj);

//#define NOTIFY(a, b, c)
//   eng_scene_entity_notify(Eo *obj, Eng_Scene_Data *pd EINA_UNUSED, a, b, c)

#include "eng_scene.eo.h"

#endif
