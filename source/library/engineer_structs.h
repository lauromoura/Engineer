#ifndef _ENGINEER_LIBRARY_GLOBAL_STRUCTS_H_
#define _ENGINEER_LIBRARY_GLOBAL_STRUCTS_H_

// These includes are for EDI's text editor parser, otherwise it throws missing type errors.
#include <Ecore_Getopt.h>
#include <Elementary.h>

#include "type/pntr.h"
#include "type/byte.h"
#include "type/indx.h"
#include "type/sclr.h"
#include "tool/queue.h"

/*** Global Data Structure Definitions ***/

typedef Eo               Eng_Node;
typedef Eo               Eng_Scene;
typedef Eina_Bool        bool;
typedef Eina_Stringshare String;
typedef uint64_t         SymbolID;

typedef enum
{
   IMPORT_MODE,
   EXPORT_MODE
}
System_Transfer_Mode;

typedef struct
{
   Eina_Hash *users;
}
Node_Thread;

typedef struct
{
   GLfloat location[3];
   GLuint type;
   GLfloat orientation[4];
   GLfloat size[3];
   GLfloat padding1;
   GLfloat color[3];
   GLfloat padding2;
}
Collider_Data;

typedef struct
{
   GLfloat location[3];
   GLuint type;
   GLfloat orientation[4];
   GLfloat size[3];
   GLfloat padding1;
   GLfloat color[3];
   GLfloat padding2;
}
Sprite_Data;

typedef struct
{
   Eina_Inarray vertices;
   Eina_Inarray indices;
   Eina_Inarray textures;

   GLfloat location[3];
   GLuint type;
   GLfloat orientation[4];
   GLfloat size[3];
   GLfloat padding1;
   GLfloat color[3];
   GLfloat padding2;
}
Polygon_Data;

typedef struct
{
   GLfloat location[3];
   GLuint type;
   GLfloat orientation[4];
   GLfloat size[3];
   GLfloat padding1;
   GLfloat color[3];
   GLfloat padding2;
}
Voxel_Data;



typedef struct
{
   // This is used in loading and unloading our Class from a block device.
   Eina_Module *eina;

   // Stores which scenes currently have an instance of this Component_Module loaded.
   Eina_Hash *dependent_scenes; // Stores a table of pointers to every dependent Eng_Scene.

   // Component_Module Metadata.
   SymbolID assetid;    // In the form of engineer_hash_murmur3("asset");
   SymbolID id;         // In the form of engineer_hash_murmur3("asset.Component.label")

   // Enumeration Tables.
   Eina_Hash *field_enumerator;

   // Component_Class Methods.
   Eo *
   (*module_new)(Eo *scene, void *class);
   String *
   (*symbol_echo)();
   String *
   (*asset_echo)();
   String *
   (*label_echo)();
   uint32_t
   (*state_sizeof)();

   String *
   (*field_label_echo)(Indx enumerator);
   String *
   (*field_symbol_echo)(Indx enumerator);

   // Component to System interface Services.
   Indx
   (*dependent_register)(Eo *component, uint64_t system_key);
   Indx
   (*dependent_deregister)(Eo *component, uint64_t system_key);

   void *
   (*state)(Eo *component);
   void
   (*state_export)(void *component, Indx field, EntityID entity, Data *state);
   void
   (*state_import)(void *component, Indx field, EntityID entity, Data *state);
   Data *
   (*state_query)(void *component, EntityID target, String *field);

   // Component Management Services.
   Eina_Bool
   (*attach_event)(Eo *component, EntityID parent, void *template);
   Indx
   (*index_get)(void *component, EntityID parentid);
}
Eng_Component_Class;

typedef struct
{
   String *symbol;

   uint64_t             key;
   Eng_Component_Class *class;
}
Eng_Component_Class_Prereq;

typedef struct
{
   uint64_t             class_key; // The key that grabs the internal enumeration index.
   Eng_Component_Class *class;
   uint64_t             field_key;
   Indx                 field;     // Our (Component) field internal enumeration index.
}
Eng_Component_Class_Target;

typedef
   Pntr //Eng_Component_Module
Eng_Component_Module_Target;

typedef struct
{
   // This is used in loading and unloading our Class from a block device.
   Eina_Module *eina;

   // Stores which scenes currently have an instance of this System_Module loaded.
   Eina_Hash *dependent_scenes;

   // System_Module Metadata
   SymbolID assetid;  // In the form of engineer_hash_murmur3("asset");
   SymbolID id;       // In the form of engineer_hash_murmur3("asset.System.label");

   // Enumeration Constant Tables.
   Eina_Inarray *system_prereqs;
   Eina_Hash    *system_events;

   Eina_Inarray *component_prereqs;
   Eina_Inarray *component_inputs;
   Eina_Inarray *component_outputs;

   Eina_Hash    *entity_events;

   // System_Class Internal API Methods.
   Eo *
   (*module_new)(Eo *scene);
   Data *
   (*system_state)(Eo *system);

   String *
   (*symbol_echo)();
   String *
   (*asset_echo)();
   String *
   (*label_echo)();

   Eina_Bool
   (*system_event_register)();
   Eina_Bool
   (*entity_event_register)();

   void
   (*dependent_register)(Eo *system, uint64_t class_id);
   uint64_t *
   (*dependent_query)(Eo *system, uint64_t class_id);
   void
   (*dependent_deregister)(Eo *system, uint64_t class_id);

   Eina_Bool
   (*iterate)(Eo *system);

   EntityID
   (*entity_query)(Eo *system, EntityID target);
   Eina_Bool
   (*entity_add)(Eo *system, EntityID target);
   Eina_Bool
   (*entity_remove)(Eo *system, EntityID target);
}
Eng_System_Class;

typedef struct
{
   String *symbol;

   uint64_t          key;
   Eng_System_Class *class;
}
Eng_System_Class_Prereq;

typedef struct
{
   Eng_System_Class *class;
   Eo               *module;
}
Eng_System_Module_Entry;

typedef struct
{
           Indx  id;
           Indx  total;
   _Atomic Indx *progress;

   void         *global; // Our System Module's system-global data.

   Eina_Inarray *entity_targets;
   Eng_Queue    *state_buffer;
   Eng_Queue    *handoff_buffer;

   Eina_Inarray *component_module_targets;
}
Eng_System_Worker;

typedef struct
{
   uint64_t key; // Our Event Murmur3 Key;
   EntityID sender;
   EntityID reciever;
   uint32_t size;
   uint32_t extra;
   uint8_t  data[];
}
Eng_Event;

#endif

