#include "eng_scene.h"

typedef struct
{
   EntityID             target;
   Eng_Component_Class *class;
   uint8_t              initial[];
} Component_Attach_Payload;

/*** Constructors ***/

Eo *
eng_scene_new(const char *name EINA_UNUSED)
{
   Eo *scene;

   efl_domain_current_push(EFL_ID_DOMAIN_SHARED);
   scene = efl_add(ENG_SCENE_CLASS, eng_node());
   efl_domain_current_pop();

   eng_scene_timer_init(scene);
   //eng_scene_name_set(scene, name));

   return scene;
}

EOLIAN static Efl_Object *
_eng_scene_efl_object_constructor(Eo *obj, Eng_Scene_Data *pd EINA_UNUSED)
{
   return efl_constructor(efl_super(obj, ENG_SCENE_CLASS));
}

EOLIAN static Efl_Object *
_eng_scene_efl_object_finalize(Eo *obj, Eng_Scene_Data *pd)
{
   obj = efl_finalize(efl_super(obj, ENG_SCENE_CLASS));

   uint64_t        key;
   Eng_Scene_Event keyhole;

   printf("\nScene Creation Checkpoint.");
   printf("\n==========================\n");

   // The Scene size is in units of (2^x)/(2^16) meters where x is the setting. Max: bitwidth--.
   pd->size = 63;

   // Set up the Scene iterator default clockrate.
   pd->clockrate = 32;
   pd->timecard  = eina_inarray_new(sizeof(uint64_t), 3);

   pd->scene_keyholes = eina_hash_int64_new(NULL);

   key     = eng_hash_murmur3("Notify_from_Efl");
   keyhole = SCENE_EVENT_NOTIFY_FROM_GUI;
   eina_hash_set(pd->scene_keyholes, &key, (void*)(uintptr_t)keyhole);
   eng_node_notice_register(eng_node(), key, 32);

   key     = eng_hash_murmur3("Entity_Create");
   keyhole = SCENE_EVENT_ENTITY_CREATE;
   eina_hash_set(pd->scene_keyholes, &key, (void*)(uintptr_t)keyhole);
   eng_node_notice_register(eng_node(), key, sizeof(EntityID));

   key     = eng_hash_murmur3("Entity_Destroy");
   keyhole = SCENE_EVENT_ENTITY_DESTROY;
   eina_hash_set(pd->scene_keyholes, &key, (void*)(uintptr_t)keyhole);
   eng_node_notice_register(eng_node(), key, sizeof(EntityID));

   key     = eng_hash_murmur3("Component_Attach");
   keyhole = SCENE_EVENT_COMPONENT_ATTACH;
   eina_hash_set(pd->scene_keyholes, &key, (void*)(uintptr_t)keyhole);
   eng_node_notice_register(eng_node(), key, sizeof(Component_Attach_Payload));

   key     = eng_hash_murmur3("Component_Detach");
   keyhole = SCENE_EVENT_COMPONENT_DETACH;
   eina_hash_set(pd->scene_keyholes, &key, (void*)(uintptr_t)keyhole);
   eng_node_notice_register(eng_node(), key, sizeof(EntityID));

   pd->entity_lookup = eina_hash_int64_new(NULL);
   pd->entity_id     = eina_inarray_new(sizeof(EntityID),   0);
   pd->entity_status = eina_inarray_new(sizeof(uint64_t),   0);
   pd->entity_inbox  = eina_inarray_new(sizeof(Eng_Queue*), 0);
   pd->entity_outbox = eina_inarray_new(sizeof(Eng_Queue*), 0);

   // We set up our "System as Entity" interface for presenting the Scene itself as an Entity.
   pd->entity = eng_node_entity_id_use(eng_node());
   eng_node_entity_scene_set(eng_node(), pd->entity, obj);
   eng_scene_entity_create_event(obj, (Data*)&pd->entity);

   pd->component_modules = eina_hash_int64_new(eng_free_cb);
   pd->system_modules    = eina_hash_int64_new(eng_free_cb);
   pd->system_order      = eina_inarray_new(sizeof(uint64_t), 0);

   return obj;
}

EOLIAN static void
_eng_scene_efl_object_destructor(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd EINA_UNUSED)
{
   //eng_scene_entity_destroy_event(obj, pd->entity);

   eina_hash_free(pd->system_modules);
   eina_hash_free(pd->component_modules);

   eina_hash_free(pd->scene_keyholes);

   eina_inarray_free(pd->system_order);

   ecore_timer_del(pd->iterator);
   eina_inarray_free(pd->timecard);

   efl_destructor(efl_super(obj, ENG_SCENE_CLASS));
}

/*** Scene Efl Object property getter/setters. ***/

EOLIAN static void
_eng_scene_name_set(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        const char *name)
{
   pd->name = eina_stringshare_add(name);
}

EOLIAN static const char *
_eng_scene_name_get(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd)
{
  return pd->name;
}

EOLIAN static void
_eng_scene_phase_set(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        uint32_t phase)
{
   STORE(&pd->phase, phase);
}

EOLIAN static uint32_t
_eng_scene_phase_get(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd)
{
   return LOAD(&pd->phase);
}

EAPI void
eng_scene_phase_wait(Eo *obj)
{
   Eng_Scene_Data *pd = efl_data_scope_get(obj, ENG_SCENE_CLASS);

   while(LOAD(&pd->fence)) continue;
}

EOLIAN static EntityID
_eng_scene_id_get(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd)
{
   return pd->entity;
}

EOLIAN static void
_eng_scene_fence_increment(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd)
{
   AFA(&pd->fence, 1);
}

EOLIAN static void
_eng_scene_fence_decrement(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd)
{
   AFS(&pd->fence, 1);
}

EOLIAN static void
_eng_scene_timer_init(Eo *obj, Eng_Scene_Data *pd)
{
   pd->iterator = ecore_timer_add((double)1/pd->clockrate, _eng_scene_iterate_cb, obj);
}

/*** Component_Module Loading, Unloading, and Utilitites ***/

EOLIAN static Eo *
_eng_scene_component_module_load(Eo *obj, Eng_Scene_Data *pd,
        String *component_symbol)
{
   Eng_Component_Class *class;
   Eo                  *module;

   // First, make sure we have a valid Component_Class for this.
   class = eng_node_component_class_load(eng_node(), component_symbol, obj);
   if(!class) return NULL;

   // Second, check to see if the target Component_Module is already loaded into the scene.
   module = eng_scene_component_module_query_loaded(obj, class);
   if(module) return module;
   module = class->module_new(obj, class);

   // Register the submitted dependent System_Module if not already present.
   eina_hash_set(pd->component_modules, &class->id, module);

   return module;
}

EOLIAN static void
_eng_scene_component_module_unload(Eo *obj, Eng_Scene_Data *pd,
        Eng_Component_Class *class, uint64_t dependent_system_key)
{
   Eo *module;

   // First, check to see if the target Component_Module is already loaded into the scene.
   module = eng_scene_component_module_query_loaded(obj, class);
   if(module)
   {
      // If so, deregister the submitted dependent System_Module.
      if(eina_hash_find(pd->component_modules, &class->id))
         eina_hash_del(pd->component_modules, &class->id, NULL);

      if(class->dependent_deregister(module, dependent_system_key) == 0)
      {
         eng_node_component_class_unload(eng_node(), class, obj);
         efl_del(module);
      }
   }
}

EOLIAN static Eo *
_eng_scene_component_module_query_loaded_by_id(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        uint64_t component_class_id)
{
   return eina_hash_find(pd->component_modules, &component_class_id);
}

EOLIAN static Eo *
_eng_scene_component_module_query_loaded(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        Eng_Component_Class *class)
{
   return eina_hash_find(pd->component_modules, &class->id);
}

/*** System_Module Loader Internal Helpers ***/

static Eina_Bool
_eng_scene_system_module_sorter(Eo *obj, Eina_Hash *system_set, uint64_t *key, void *order)
{
   Eng_System_Class        *class;
   Eina_Hash               *visited;
   Eng_System_Class_Prereq *prereq;

   visited = eina_hash_find(system_set, key);
   if(visited != system_set) return EINA_TRUE;
   eina_hash_modify(system_set, key, obj);

   class = eng_node_system_class_query_loaded_by_id(eng_node(), *(uint64_t*)key);
   EINA_INARRAY_FOREACH(class->system_prereqs, prereq)
      _eng_scene_system_module_sorter(obj, system_set, &prereq->key, order);

   eina_inarray_push(order, &class->id);

   return EINA_TRUE;
}

static Eina_Bool
_eng_scene_system_module_counter(const Eina_Hash *hash EINA_UNUSED,
        const void *key, void *data EINA_UNUSED, void *fdata)
{
   Eina_Inarray *inverse_order = fdata;

   eina_inarray_push(inverse_order, key);

   return EINA_TRUE;
}

static Eina_Inarray *
_eng_scene_system_module_sort(Eo *obj, Eina_Hash *system_set)
{
   Eina_Inarray *order, *order_inverse;
   uint64_t     *prereq_key;

   order         = eina_inarray_new(sizeof(uint64_t), 0);
   order_inverse = eina_inarray_new(sizeof(uint64_t), 0);

   eina_hash_foreach(system_set, _eng_scene_system_module_counter, order);

   EINA_INARRAY_FOREACH(order, prereq_key)
      _eng_scene_system_module_sorter(obj, system_set, prereq_key, order_inverse);

   eina_inarray_flush(order);

   EINA_INARRAY_FOREACH(order_inverse, prereq_key)
      eina_inarray_push(order, prereq_key);

   eina_inarray_free(order_inverse);
   if(!eina_inarray_count(order)) { eina_inarray_free(order); return NULL; }
   else return order;
}

static Eina_Bool
_eng_scene_system_module_load_prereqs_abort(const Eina_Hash *hash EINA_UNUSED,
        const void *key EINA_UNUSED, void *data, void *fdata)
{
   Eng_Scene        *scene = fdata;
   Eng_System_Class *class = data;

   eng_scene_system_module_unload(scene, class);
   eng_node_system_class_unload(eng_node(), class, fdata);

   return EINA_TRUE;
}

static Eina_Hash *
_eng_scene_system_module_load_prereqs(Eo *obj, Eng_Scene_Data *pd,
        String *system_symbol)
{
   #define ABORT \
      eina_hash_foreach(system_set, _eng_scene_system_module_load_prereqs_abort, obj); \
         eina_hash_free(system_set); system_set = NULL; return system_set;

   Eng_System_Class           *class;
   Eng_System_Class_Prereq    *s_prereq;
   Eng_Component_Class_Prereq *c_prereq;
   Eo                         *module;
   uint64_t                    class_id;

   static uint64_t  *stack,       depth;
   static Eina_Hash *system_set, *return_set;

   if(!system_set) system_set = eina_hash_int64_new(NULL);
   depth++;

   // If our hash data for this class_id is equal to the location of the stack pointer, we have a
   //    cycle. If it's equal to anything else, we have already explored that part of the tree.
   class_id = eng_hash_murmur3(system_symbol);
   stack    = eina_hash_find(system_set, &class_id);
   if(stack == (uint64_t*)&stack) { ABORT; }
   if(stack) return system_set;
   eina_hash_set(system_set, &class_id, &stack);

   class = eng_node_system_class_load(eng_node(), system_symbol, obj);
   if(!class) { ABORT; }
   eina_hash_modify(system_set, &class->id, system_set);

   // If any of our recursive children returns false, pass it up the stack.
   // Otherwise, we need to add the System_Module to the dependent set of each of the systems it requires.
   EINA_INARRAY_FOREACH(class->system_prereqs, s_prereq)
      if(!_eng_scene_system_module_load_prereqs(obj, pd, s_prereq->symbol))
         return system_set;

   // Load (or increment the load reference counter for) all prerequisite Component_Modules.
   //    This is a simple affair, since Component's never have a prerequisite.
   EINA_INARRAY_FOREACH(class->component_prereqs, c_prereq)
      eng_scene_component_module_load(obj, c_prereq->symbol);

   module = eng_scene_system_module_query_loaded(obj, class);
   if(!module) module = class->module_new(obj);
   eina_hash_set(pd->system_modules, &class->id, module);

   EINA_INARRAY_FOREACH(class->component_prereqs, c_prereq)
   {
      c_prereq->class = eng_node_component_class_query_loaded_by_id(eng_node(), c_prereq->key);
      module          = eng_scene_component_module_query_loaded_by_id(obj,      c_prereq->key);
      c_prereq->class->dependent_register(module, class->id);
   }

   EINA_INARRAY_FOREACH(class->system_prereqs, s_prereq)
   {
      s_prereq->class = eng_node_system_class_query_loaded_by_id(eng_node(), s_prereq->key);
      module          = eng_scene_system_module_query_loaded_by_id(obj,      s_prereq->key);
      s_prereq->class->dependent_register(module, class->id);
   };

   return_set = system_set;
   depth--;
   if(!depth) system_set = NULL;
   return return_set;

   #undef ABORT
}

EOLIAN static Eo *
_eng_scene_system_module_load(Eo *obj, Eng_Scene_Data *pd,
        String *system_symbol)
{
   Eng_System_Class *class;

   Eo           *module;
   uint64_t     *target, class_id;
   Eina_Hash    *system_set; // This tracks which parts of the graph have already been visited.
   Eina_Inarray *system_order;

   class_id = eng_hash_murmur3(system_symbol);

   module = eng_scene_system_module_query_loaded_by_id(obj, class_id);
   if(module) return module;

   // In order to find the new build order, we have to find the union of the set of the old
   //    system_order and the new module's dependencies, plus the new module.
   system_set = _eng_scene_system_module_load_prereqs(obj, pd, system_symbol);
   if(!system_set) return NULL;

   class  = eng_node_system_class_query_loaded_by_id(eng_node(), class_id);
   module = eina_hash_find(system_set, &class->id);

   EINA_INARRAY_FOREACH(pd->system_order, target)
      eina_hash_set(system_set, target, system_set);

   system_order = _eng_scene_system_module_sort(obj, system_set);
   if(system_order) { eina_inarray_free(pd->system_order); pd->system_order = system_order; }
   else             { eina_inarray_free(system_order); eina_hash_free(system_set); return NULL; }

   return module;
}

EOLIAN static void
_eng_scene_system_module_unload(Eo *obj, Eng_Scene_Data *pd EINA_UNUSED,
        Eng_System_Class *class)
{
   Eo                         *module;
   Eng_Component_Class_Target *target;

   printf("System_Module unload Checkpoint 1.\n");

   if(!(module = eng_scene_system_module_query_loaded(obj, class)) ) return;

   printf("System_Module unload Checkpoint 2.\n");

   EINA_INARRAY_FOREACH(class->component_prereqs, target)
      eng_scene_component_module_unload(obj, target->class, class->id);

   eina_hash_del(pd->system_modules, &class->id, NULL);
   efl_del(module);
   eng_node_system_class_unload(eng_node(), class, obj);
}

EOLIAN static Eo *
_eng_scene_system_module_query_loaded_by_id(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        uint64_t system_class_id)
{
   return eina_hash_find(pd->system_modules, &system_class_id);
}

EOLIAN static Eo *
_eng_scene_system_module_query_loaded(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        Eng_System_Class *class)
{
   return eina_hash_find(pd->system_modules, &class->id);
}

// Fixme: Not done yet.
EOLIAN static void
_eng_scene_asset_load(Eo *obj, Eng_Scene_Data *pd,
        String *asset_symbol)
{
   String    *path, *syspath, *system_file;
   Eina_List *list;
   String    *system_suffix = "System.so";
   uint32_t   extension;
   char      *system_symbol;

   path = eng_node_path_get(eng_node());
   if(!path) return;

   syspath = eina_stringshare_printf("%s/libraries", path);

   // Get a list of System files from ../libraries
   list = ecore_file_ls(syspath);
   EINA_LIST_FREE(list, system_file)
   {
      if(strstr(system_file, asset_symbol) == system_file && // This compares to see if the module file has the system prefix.
         strstr(system_file, system_suffix))
      {
         extension     = strlen(system_file) - strlen(".so");
         system_symbol = malloc(extension + 1);

         strncpy(system_symbol, system_file, extension);
         system_symbol[extension] = '\0';

         _eng_scene_system_module_load(obj, pd, system_symbol);

         free(system_symbol);
      }

      free((char*)system_file);
   }
}

// Fixme: Not done yet.
EOLIAN static void
_eng_scene_asset_unload(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd EINA_UNUSED,
        String *asset_symbol EINA_UNUSED)
{
}

/*** Iteration Methods. These are what make everything happen. ***/

static void
_eng_scene_cycle_mailboxes(Eo *obj, Eng_Scene_Data *pd EINA_UNUSED)
{
   EntityID   *entity;
   Eng_Queue  *inbox, *outbox;

   EINA_INARRAY_FOREACH(pd->entity_id, entity)
   {
      inbox  = eng_scene_entity_inbox_get(obj, *entity);
      outbox = eng_scene_entity_outbox_get(obj, *entity);
      if(outbox)
      {
         eng_queue_free(outbox);
         eng_scene_entity_outbox_set(obj, *entity, NULL);
      }
      if(inbox)
      {
         eng_scene_entity_outbox_set(obj, *entity, inbox);
         eng_scene_entity_inbox_set(obj, *entity, NULL);
      }
   }
}

static void
_eng_scene_event_dispatch(Eo *obj, Eng_Scene_Data *pd)
{
   Indx               progress, offset;
   Eng_Queue         *outbox;
   Eng_Queue_Payload *payload;
   Eng_Event         *event;

   if( (outbox = eng_scene_entity_outbox_get(obj, pd->entity)) )
   {
      // Alter the Scenegraph according to the Scene outbox.
      progress = 0;
      while(progress < outbox->occupancy)
      {
         offset  = (outbox->tail_in + progress) % outbox->capacity;
         payload = (Eng_Queue_Payload*)(outbox->payload + offset);
         event   = (Eng_Event*)payload->data;
         switch((Eng_Scene_Event)eina_hash_find(pd->scene_keyholes, &event->key))
         {
            case SCENE_EVENT_NULL :
            /* Nothing Happens! */
            break;

            case SCENE_EVENT_NOTIFY_FROM_GUI :
            eng_scene_entity_notify_from_efl_event(obj, (Data*)event->data);
            break;

            case SCENE_EVENT_ENTITY_CREATE :
            eng_scene_entity_create_event(obj, (Data*)event->data);
            break;

            case SCENE_EVENT_ENTITY_DESTROY :
            //eng_scene_entity_destroy_event(obj, (Data*)event->data);
            break;

            case SCENE_EVENT_COMPONENT_ATTACH :
            eng_scene_component_attach_event(obj, (Data*)event->data);
            break;

            case SCENE_EVENT_COMPONENT_DETACH :
            //eng_scene_component_detach_event(obj, (Data*)event->data);
            break;

         }
         progress += PAGE_ALIGN(payload->size + sizeof(Eng_Queue_Payload));
      }
   }
}

EAPI Eina_Bool
eng_scene_iterate(Eo *obj)
{
   Eng_Scene_Data   *pd;
   Eng_System_Class *system;
   Eo               *module;
   double            timestamp;
   uint64_t         *current, count;

   pd = efl_data_scope_get(obj, ENG_SCENE_CLASS);

   printf("\nScene Iterator Checkpoint.\n");
   printf("==========================\n");

   STORE(&pd->phase, SCENE_UPKEEP);

   // Store the timestamp for when this Sector's clock tick begins.
   timestamp = ecore_time_get();

   // Cycle the Scene's Entity Mailboxen.
   _eng_scene_cycle_mailboxes(obj, pd);

   STORE(&pd->phase, SCENE_GLOBAL_UPDATE);

   // Alter the Scenegraph according to the Scene's Entity outbox.
   _eng_scene_event_dispatch(obj, pd);

   EINA_INARRAY_FOREACH(pd->system_order, current)
   {
      system = eng_node_system_class_query_loaded_by_id(eng_node(), *current);
      module = eng_scene_system_module_query_loaded(obj, system);
      system->iterate(module);
   }

   STORE(&pd->phase, SCENE_CLEANUP);

   // Check to see if we are taking more than 85% of our tick time, if so, decrease the clock rate.
   timestamp -= ecore_time_get();
   if (pd->clockrate != 1 && timestamp > 1/pd->clockrate * 0.85)
   {
      pd->clockrate--;
      ecore_timer_interval_set(pd->iterator, 1/pd->clockrate);
   }
   // If TiDi is engaged, but the tick time load is less than 70%, if so, increase the clock rate.
   else if (pd->clockrate != 32 && timestamp < 1/pd->clockrate * 0.70)
   {
      pd->clockrate++;
      ecore_timer_interval_set(pd->iterator, 1/pd->clockrate);
   }
   // Set our completion timestamp for this frame.
   timestamp = ecore_time_get();
   count = eina_inarray_push(pd->timecard, &timestamp);

   printf("Scene Iterator End Checkpoint. Clockrate: %d Htz, Timestamp: %1.5f\n",
      pd->clockrate, *(double*)eina_inarray_nth(pd->timecard, count));

   return ECORE_CALLBACK_RENEW;
}

Eina_Bool
_eng_scene_iterate_cb(void *obj)
{
    eng_scene_iterate(obj);
    return ECORE_CALLBACK_RENEW;
}

/*** Entity Internal API ***/

EOLIAN static void
_eng_scene_entity_index_set(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        EntityID entity_id, Indx index)
{
    index++;
    eina_hash_set(pd->entity_lookup, &entity_id, (void*)(uintptr_t)index);
}

EOLIAN static Indx
_eng_scene_entity_index_get(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        EntityID entity_id)
{
   Indx index;

   index = (Indx)(uintptr_t)eina_hash_find(pd->entity_lookup, &entity_id);
   if(index) return index - 1;
   else      return -1;
}

EOLIAN static void
_eng_scene_entity_status_set(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        EntityID target, char status)
{
   Indx index;

   if (status < 4) // Make sure that our mode is either zero, one, two, or three.
   {
      index = (Indx)(uintptr_t)eina_hash_find(pd->entity_lookup, &target);
      index--;
      eina_inarray_replace_at(pd->entity_status, index, &status);
   }
}

EOLIAN static char
_eng_scene_entity_status_get(Eo *obj, Eng_Scene_Data *pd,
        EntityID entity)
{
   Indx index = eng_scene_entity_index_get(obj, entity);
   if (index == (Indx)-1) return 0;
   char *status = eina_inarray_nth(pd->entity_status, index);

   return *status;
}

EOLIAN static void
_eng_scene_entity_inbox_set(Eo *obj, Eng_Scene_Data *pd,
        EntityID entity, Eng_Queue *inbox)
{
   Index index = eng_scene_entity_index_get(obj, entity);
   eina_inarray_replace_at(pd->entity_inbox, index, &inbox);
}

EOLIAN static Eng_Queue *
_eng_scene_entity_inbox_get(Eo *obj, Eng_Scene_Data *pd,
        EntityID entity)
{
   Eng_Queue **inbox;
   Index      index;

   index = eng_scene_entity_index_get(obj, entity);
   inbox = (Eng_Queue**)eina_inarray_nth(pd->entity_inbox, index);
   if(inbox && !*inbox) *inbox = eng_queue_new(256);

   return *inbox;
}

EOLIAN static void
_eng_scene_entity_outbox_set(Eo *obj, Eng_Scene_Data *pd,
        EntityID entity, Eng_Queue *outbox)
{
   Index index = eng_scene_entity_index_get(obj, entity);
   eina_inarray_replace_at(pd->entity_outbox, index, &outbox);
}

EOLIAN static Eng_Queue *
_eng_scene_entity_outbox_get(Eo *obj, Eng_Scene_Data *pd,
        EntityID entity)
{
   Index index = eng_scene_entity_index_get(obj, entity);
   return *(Eng_Queue**)eina_inarray_nth(pd->entity_outbox, index);
}

EOLIAN static void
_eng_scene_entity_notify_from_efl_event(Eo *obj, Eng_Scene_Data *pd EINA_UNUSED,
        Data *notice)
{
   struct
   {
      EntityID  sender;
      EntityID  reciever;
      String   *symbol;
      uint64_t  padding;
      uint8_t   payload[];
   }
   *order;

   printf("Entity Notify from Efl Event.\n");

   order = (void*)notice; //drain->data;
   eng_scene_entity_notify(obj, order->sender, order->reciever, order->symbol, (Data*)order->payload);
}

EOLIAN static Eina_Bool
_eng_scene_entity_create_event(Eo *obj, Eng_Scene_Data *pd,
        Data *notice)
{
   EntityID  entity = *(EntityID*)notice;
   Indx      index;
   uint64_t  status = 1;
   Eng_Queue *empty = NULL;

   if(eng_scene_entity_index_get(obj, entity) == (Indx)UINT_NULL)
   {
      index =
      eina_inarray_push(pd->entity_id,     &entity);
      eina_inarray_push(pd->entity_status, &status);
      eina_inarray_push(pd->entity_inbox,  &empty);
      eina_inarray_push(pd->entity_outbox, &empty);

      eng_scene_entity_index_set(obj, entity, index); // this sets pd->lookup

      printf("Entity Created.     | EntityID: %ld, Index: %d,\n", entity, index);
   }
   return EINA_TRUE;
}

/*** Entity EOLIAN API ***/

EOLIAN static EntityID
_eng_scene_entity_create(Eo *obj, Eng_Scene_Data *pd EINA_UNUSED,
        EntityID sender)
{
   EntityID new_id;

   new_id = eng_node_entity_id_use(eng_node());
   eng_node_entity_scene_set(eng_node(), new_id, obj);
   eng_scene_entity_notify(obj, sender, pd->entity, "Entity_Create", (Data*)&new_id);

   return new_id;
}

EOLIAN static void
_eng_scene_entity_notify(Eo *obj, Eng_Scene_Data *pd EINA_UNUSED,
        EntityID sender, EntityID reciever, String *symbol, Data *payload)
{
   eng_scene_entity_notify_with_extra(obj, sender, reciever, symbol, payload, 0);
}

EOLIAN static void
_eng_scene_entity_notify_with_extra(Eo *obj, Eng_Scene_Data *pd,
        EntityID sender, EntityID reciever, String *symbol, Data *payload, Indx extra)
{
   Eng_Queue *inbox;
   Eng_Event *event;
   uint64_t   key, size;

   if(eng_scene_entity_index_get(obj, reciever) == (Indx)-1) return;

   key   = eng_hash_murmur3(symbol);
   size  = eng_node_notice_sizeof(eng_node(), symbol);
   inbox = _eng_scene_entity_inbox_get(obj, pd, reciever);

   if(!inbox) inbox = eng_queue_new(size);
   event = eng_queue_fill(inbox, sizeof(Eng_Event) + size + extra);
   event->key      = key;
   event->sender   = sender;
   event->reciever = reciever;
   event->size     = size;
   event->extra    = extra;
   memcpy((void*)event->data, (void*)payload, size + extra);
   eng_queue_fill_done(inbox, event);

   printf("Entity Notified.    | EntityID: %ld, Symbol: %s\n", reciever, symbol);
}

EOLIAN static void
_eng_scene_entity_notify_from_efl(Eo *obj, Eng_Scene_Data *pd,
        EntityID reciever, String *symbol, Data *payload)
{
   uint64_t size;

   size = eng_node_notice_sizeof(eng_node(), symbol);

   struct
   {
      EntityID  sender;
      EntityID  reciever;
      String   *symbol;
      uint64_t  padding;
      uint8_t   payload[];
   }
   *data;

   data = malloc(sizeof(data) + size);

   data->sender   = pd->entity;
   data->reciever = reciever;
   data->symbol   = symbol;
   memcpy((void*)data->payload, (void*)payload, size);

   eng_scene_entity_notify_with_extra(
      obj,
      reciever,
      pd->entity,
      "Notify_from_Efl",
      (Data*)data,
      size);

   free(data);
}

EOLIAN static Data *
_eng_scene_entity_state_get(Eo *obj, Eng_Scene_Data *pd EINA_UNUSED,
        EntityID target, Eng_Component_Class *class, String *label)
{
   Eo *module;

   module = eng_scene_component_module_query_loaded(obj, class);

   return class->state_query(module, target, label);
}

EOLIAN static uint64_t
_eng_scene_component_attach_event(Eo *obj, Eng_Scene_Data *pd EINA_UNUSED,
        Data *notice)
{
   Eo *module;

   struct Payload { EntityID target; Eng_Component_Class *class;  uint8_t initial[];}
      *payload;

   payload = (struct Payload*)notice;
   module  = eng_scene_component_module_query_loaded(obj, payload->class);

   payload->class->attach_event(module, payload->target, payload->initial);

   return sizeof(Eng_Event) + payload->class->state_sizeof();
}

/*** Component API ***/

// Instead of calling the standard Entity_Notify, we have a custom version for dealing with the fact
//    that the initialization data size varies with the target Component_Key.
EOLIAN static void
_eng_scene_component_attach(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        EntityID sender, EntityID target, String *component_symbol, Data *initial)
{
   Eng_Component_Class *class;
   Eng_Queue           *inbox;
   Eng_Event           *event;
   uint64_t   key, hsize, psize, isize;

   struct { EntityID target; Eng_Component_Class *class; uint8_t initial[]; } *payload;

   class = eng_node_component_class_query_loaded(eng_node(), component_symbol);
   if(class) isize = class->state_sizeof(); else return;

   key   = eng_hash_murmur3("Component_Attach");
   hsize = sizeof(Eng_Event);
   psize = eng_node_notice_sizeof(eng_node(), "Component_Attach");
   inbox = eng_scene_entity_inbox_get(obj, pd->entity);

   if(!inbox) inbox = eng_queue_new(psize);
   event = eng_queue_fill(inbox, hsize + psize + isize);
   event->key      = key;
   event->sender   = sender;
   payload         = (void*)event->data;
   payload->target = target;
   payload->class  = class;

   if(initial) memcpy(payload->initial, initial, isize);
   else        memset(payload->initial, 0,       isize);
   eng_queue_fill_done(inbox, event);

   printf("Entity Notified.    | EntityID: %ld, Symbol: Component_Attach\n", target);
}

EOLIAN static Data *
_eng_scene_system_state_get(Eo *obj, Eng_Scene_Data *pd EINA_UNUSED,
        String *system_symbol)
{
   Eng_System_Class *class;
   Eo               *module;
   uint64_t          key;

   key     = eng_hash_murmur3(system_symbol);
   class   = eng_node_system_class_query_loaded_by_id(eng_node(), key);
   module  = eng_scene_system_module_query_loaded_by_id(obj, key);

   return class->system_state(module);
}

#include "eng_scene.eo.c"
/*
EOLIAN static void
_eng_scene_entity_destroy(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        uint target)
{
  eng_node_entity_status_set(pd->node, target, 1);
}

EOLIAN static void
_eng_scene_entity_dispose(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd EINA_UNUSED,
        uint target EINA_UNUSED)
{
}

EOLIAN static void
_eng_scene_entity_archive(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd EINA_UNUSED,
        uint target EINA_UNUSED)
{
}

EOLIAN static void
_eng_scene_entity_recall(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd EINA_UNUSED,
        uint target EINA_UNUSED)
{
}

EOLIAN static void
_eng_scene_entity_data_swap(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        uint targeta, uint targetb)
{
   Eng_Scene_Entity *payloada, *payloadb, payloadbuffer;
   uint *targetalookup = eina_hash_find(pd->future->entitylookup, &targeta);
   uint *targetblookup = eina_hash_find(pd->future->entitylookup, &targetb);
   if (targetalookup == NULL || targetblookup == NULL) return;
   uint  lookupbuffer = *targetalookup;

   payloada = eina_inarray_nth(pd->future->entitycache, *targetalookup);
   payloadb = eina_inarray_nth(pd->future->entitycache, *targetblookup);
   payloadbuffer = *payloada;
   *payloada = *payloadb;
   *payloadb = payloadbuffer;

   eina_hash_modify(pd->future->entitylookup, &targeta, targetblookup);
   eina_hash_modify(pd->future->entitylookup, &targetb, &lookupbuffer);
}

EOLIAN static ComponentID
_eng_scene_entity_list_components(Eo *obj, Eng_Scene_Data *pd EINA_UNUSED,
        EntityID entityid, ClassLabel target)
{
   Eng_Component_Class *class;
   Eo *node = efl_parent_get(obj);
   uint64_t first, component, classid;

   // We should now go thru the list of active scene components and ask if they have the specified
   //    entity. If it does, add it to our return list.

   // Should be 1, but that produces a segfault.
   first = engineer_scene_entity_firstcomponent_get(obj, entityid);
   if (first == ULONG_NULL) return ULONG_NULL;

   classid = engineer_hash_murmur3(target);

   //class = engineer_node_component_class_get_by_symbolid(node, first);
   class = efl_key_data_get(module, "Component_Class");
   if (classid == (uint64_t)class->labelid) return first;

   component = engineer_scene_component_siblingnext_get(obj, first);
   while(component != first)
   {
      class = engineer_node_component_class_get(node, component);
      if (classid == (uint64_t)class->labelid) return component;

      component = engineer_scene_component_siblingnext_get(obj, component);
   }
   return ULONG_NULL;
}
*/

