#ifndef _ENG_COMPONENT_H_
#define _ENG_COMPONENT_H_
#define _ENG_SYSTEM_H_

#include "engineer.h"

/*** M4 Macro Stubs ***/
#define STATE(definition)
#define FIELD(definition)

typedef struct
{
   Eo   *module;
   Indx  counter;
}
Eng_Component_Target;

typedef struct
{
   Eng_Component_Class *class;

   Eina_Inarray *dependents; // This contains a table that stores the set of Systems that depend on this Component,

   Eina_Hash    *lookup; // Takes in an EntityID as a key and returns the parent inarray index.
   Eina_Inarray *parent; // The parent's EntityID reverse lookup table.
   void         *vault;  // This is a substruct that stores all of our Component fields as SOA.
}
Eng_Component_Data;

EAPI String *
eng_component_symbol_echo();

EAPI String *
eng_component_asset_echo();

EAPI String *
eng_component_label_echo();

EAPI uint32_t
eng_component_state_sizeof();

EAPI Eina_Hash *
eng_component_field_enumerator_new();

EAPI String *
eng_component_field_label_echo(Indx enumerator);

EAPI String *
eng_component_field_symbol_echo(Indx enumerator);
/*
EAPI Eina_Inarray *
engineer_component_field_targets_get(Eina_Inarray *targets);
*/
EAPI Efl_Object *
eng_component_new(Eo *parent, Eng_Component_Class *class);

EAPI void *
_eng_component_vault_init();

EAPI void
_eng_component_index_set(Eng_Component_Data *pd, EntityID entityid, Indx index);

EAPI Indx
eng_component_index_get(Eng_Component_Data *pd, EntityID target);

EAPI void
_eng_component_push(Eng_Component_Data *pd, EntityID parent, Data *initial);

EAPI Eina_Bool
eng_component_attach_event(Eo *module, EntityID parent, Data *template);

#include <eng_component.eo.h>

#endif
