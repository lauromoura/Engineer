#ifndef _ENG_SYSTEM_H_
#define _ENG_SYSTEM_H_
#define _ENG_COMPONENT_H_

#include <engineer.h>
//#include "eng_component.h"

/*** M4 Macro Stubs ***/
#define SYMBOL(definition)
#define PREREQS(definition)
#define GLOBAL(definition)
#define ENTITY(definition)

typedef struct
{
   Eina_Bool started; //    purposes.  This includes the Eng_Scene itself, even though it is not a
   Eina_Bool running; //    "proper" System.

   /*** System Specific Data ***/
   Eina_Inarray *worker_state;
   void         *global_state;

   Eina_Hash *dependent_systems; // Used to determine when to unload modules.

   Eng_Queue *system_inbox;  // Recieves System Notices from Entities and other sources.
   Eng_Queue *system_outbox;

   Eina_Inarray *entity_targets;      // Stores the set of Entities the System is operating on.
   Eng_Queue    *entity_state_buffer; // This is where our copy of the Entity state is stored.

   /*** Prerequisite tables ***/
   Eina_Inarray *component_prereqs;
   Eina_Inarray *component_inputs;
   Eina_Inarray *component_outputs;
}
Eng_System_Data;

/*** EFL Object Factory Method ***/
EAPI Efl_Object * eng_system_new(Eo *parent);

/*** Constant Table Factory Methods ***/
EAPI String * eng_system_symbol_echo();
EAPI String * eng_system_asset_echo();
EAPI String * eng_system_label_echo();

EAPI Eina_Inarray * eng_system_prereq_enumerator_new();
EAPI Eina_Hash    * eng_system_event_enumerator_new();

EAPI Eina_Inarray * eng_system_component_prereq_class_enumerator_new(Eo *node);
EAPI Eina_Inarray * eng_system_component_input_class_enumerator_new(Eo *node);
EAPI Eina_Inarray * eng_system_component_output_class_enumerator_new(Eo *node);

EAPI Eina_Hash * eng_system_entity_event_enumerator_new();

/*** System Internal Invocation API Stubs ***/

#define system_start(state) \
   _eng_system_start(const EntityID this  EINA_UNUSED, \
                                         state EINA_UNUSED)
#define system_update(state) \
   _eng_system_update(const EntityID this  EINA_UNUSED, \
                                          state EINA_UNUSED)
#define system_stop(state) \
   _eng_system_stop(const EntityID this  EINA_UNUSED, \
                                        state EINA_UNUSED)
#define system_event(symbol, state, payload) \
   _eng_system_event_##symbol(const EntityID this    EINA_UNUSED, \
                                             state   EINA_UNUSED, \
                                             payload EINA_UNUSED)

#define entity_start(state) \
   _eng_system_entity_start(const EntityID      this   EINA_UNUSED, \
                            const System_State *system EINA_UNUSED, \
                                                state  EINA_UNUSED)
#define entity_update(state) \
   _eng_system_entity_update(const EntityID      this   EINA_UNUSED, \
                             const System_State *system EINA_UNUSED, \
                                                 state  EINA_UNUSED)
#define entity_stop(state) \
   _eng_system_entity_stop(const EntityID      this   EINA_UNUSED, \
                           const System_State *system EINA_UNUSED, \
                                               state  EINA_UNUSED)
#define entity_event(symbol, state, payload) \
   _eng_system_entity_event_##symbol(const EntityID      this    EINA_UNUSED, \
                                     const System_State *system  EINA_UNUSED, \
                                                         state   EINA_UNUSED, \
                                                         payload EINA_UNUSED)

/** System Programming API ***/

// Sends a Notice of $TYPE Event with an attached payload to the specified System.
// Effect Speed: Next Frame *** NOT COMPLETE ***
#define system_notify(target, event, payload) \
   eng_scene_entity_notify(system->scene, this, target, event, (Data*)payload)

// Retreive the current value of a System Field.
// Effect Speed: Immediate. *** NOT FUNCTIONAL ***
#define system_query(target, this, field) \
   eng_scene_system_query(system->scene, target, this, field)

// Creates a new Entity, attaches it to a specified parent Entity and returns it's EntityID.
// Effect Speed: Next Frame
#define entity_create() \
   eng_scene_entity_create(system->scene, this)

// Sends a Notice of $TYPE Event with an attached payload to the specified Entity.
// Effect Speed: Next Frame
#define entity_notify(target, event, payload) \
   eng_scene_entity_notify(system->scene, this, target, event, (Data*)payload)

// Destroy a currently existing entity, and any Components attached to it.
// Effect Speed: Next Frame
#define entity_destroy(entity) \
   eng_scene_entity_destroy(system->scene, this, entity)

// Attaches a new Component to the specified Entity, and returns it's ComponentID.
// Effect Speed: Next Frame
#define component_attach(class, entity, initial) \
   eng_scene_component_attach(system->scene, this, class, entity, initial)

// Retrieve the current value of a target Component field.
// Effect Speed: Immediate. *** NOT FUNCTIONAL ***
#define component_query(class, entity, field) \
   eng_scene_component_query(system->scene, this, class, entity, field)

// Remove a currently existing Component from the specified Entity.
// Effect Speed: Next Frame
#define component_detach(class, entity) \
   eng_scene_component_detach(system->scene, this, class, entity)

/*** Array Access & Manipulation Macros ***/
/*
#define array_count(array) \
   eng_state_array_count(array)

#define array_push(array, input) \
   eng_state_array_push(array, input)

#define array_pop(array) \
   eng_state_array_pop(array)

#define array_insert(array, index, input) \
   eng_state_array_insert(array, index, input)

#define array_remove(array, index) \
   eng_state_array_remove(array, index)

#define array_read(array, index) \
   eng_state_array_read(array, index)

#define array_write(array, index, input) \
   eng_state_array_write(array, index, input)

#define array_flush(array) \
   eng_state_array_flush(array)
*/

#include <eng_system.eo.h>

#endif

