#include "quat.h"

Quat
eng_math_quat_multiply(Quat multiplier, Quat multiplicand)
{
   #define q1 multiplier
   #define q2 multiplicand
   Quat output;

   output.x = MULT(q1.w, q2.x) + MULT(q1.x, q2.w) + MULT(q1.y, q2.z) - MULT(q1.z, q2.y);
   output.y = MULT(q1.w, q2.y) - MULT(q1.x, q2.z) + MULT(q1.y, q2.w) + MULT(q1.z, q2.x);
   output.z = MULT(q1.w, q2.z) + MULT(q1.x, q2.y) - MULT(q1.y, q2.x) + MULT(q1.z, q2.w);
   output.w = MULT(q1.w, q2.w) - MULT(q1.x, q2.x) - MULT(q1.y, q2.y) - MULT(q1.z, q2.z);

   return output;
   #undef q1
   #undef q2
}

Quat
eng_math_quat_invert(Quat input)
{
   Quat output;

   output.w =  input.w;
   output.x = -input.x;
   output.y = -input.y;
   output.z = -input.z;

   return output;
}

Quat
eng_math_quat_normalize(Quat input)
{
   Quat output;
   Sclr basis, square, sqrt, inverse;

   basis    = BASIS;
   square   = MULT(input.x, input.x) + MULT(input.y, input.y)
            + MULT(input.z, input.z) + MULT(input.w, input.w);
   sqrt     = SQRT(square);
   inverse  = DIVD(basis, sqrt);
   output.x = MULT(input.x, inverse);
   output.y = MULT(input.y, inverse);
   output.z = MULT(input.z, inverse);
   output.w = MULT(input.w, inverse);

   return output;
}

