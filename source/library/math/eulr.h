#ifndef _ENG_MATH_EULR_H_
#define _ENG_MATH_EULR_H_

#include "../type/eulr.h"
#include "quat.h"

Quat eng_math_eulr_to_quat(Eulr input);

#endif

