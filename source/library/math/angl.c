#include "angl.h"

extern Sclr cordic_gain_c;
extern Sclr cordic_gain_h;

/*** Elementary Arithmatic Functions ***/

Angl
eng_math_angl_mult(Angl multiplicand, Angl multiplier)
{
   Angl output;

   output   = multiplicand * multiplier;
   output  += (output & ((Angl)1 << (ANGLRADIX - 1)) ) << 1;
   output >>= ANGLRADIX;

   return output;
}

Angl
eng_math_angl_divd(Angl dividend, Angl divisor)
{
   Angl output;

   output = (dividend << ANGLRADIX) / divisor;

   return output;
}

/*** Elementary Trigonomic Functions ***/

Vec2
eng_math_sincos(Angl input)
{
   // Domain: |a| < 1.74 or 1 > a > -1;
   Vec3 buffer;
   Vec2 output;
   // input % (int64_t)(ANGLBASIS * 3.14159)

   buffer.x = cordic_gain_c; // Cos()
   buffer.y = 0;             // Sin()
   buffer.z = input % (int64_t)(ANGLBASIS * 1.74);

   buffer   = cordic_circular_zmode(buffer);
   output.x = ANGL2SCLR(buffer.x);
   output.y = ANGL2SCLR(buffer.y);

   return output;
}

Sclr
eng_math_sin(Angl input)
{
   // A sine approximation via a fifth-order polynomial.
   int64_t x, x2, o;
   static const int64_t
      A  = PI/2,                        // PI/2
      B  = PI - (5 * (BASIS + 8))/2,    // PI - 5/2
      C  = (PI -(3 * (BASIS + 8)))/2,   // (PI - 3)/2
      S  = (BASIS << (RADIX + 1)) / PI; // 2/PI

    x = MULT(input, S);

    x = x << (62 - RADIX);         // Shift to full s64 range (Q16->Q62)
    if( (x ^ (x << 1)) < 0)        // Test for quadrant 1 or 2
       x = ((int64_t)1 << 63) - x;
    x = x >> (62 - RADIX);

    //x2 =     MULT(x, x);   // Ax - Bx^3 + Cx^5
    //o  = B - MULT(C, x2);  // Ax - x(Bx^2 - Cx^4)
    //o  = A - MULT(o, x2);  // x(A - (Bx^2 - Cx^4)
    //o  =     MULT(o, x);   // x(A - x^2(B - Cx^2))

    x2 =     ((x * x)  >> 8);
    o  = B - ((C * x2) >> 24);
    o  = A - ((o * x2) >> 24);
    o  =     ((o * x)  >> 16);

    return o;
}

Sclr
eng_math_cos(Angl input)
{
   // A cosine approximation via a fifth-order polynomial.
   int64_t x, x2, o;
   static const int64_t
      A  = PI/2,                      // PI/2
      B  = PI - (5 * (BASIS + 8))/2,  // PI - 5/2
      C  = (PI -(3 * (BASIS + 8)))/2, // (PI - 3)/2
      S  = (BASIS << (RADIX + 1))/PI; // 2/PI

    x = MULT(input, S);
    x += BASIS;         // cosine -> sine calculation.

    x = x << (62 - RADIX);         // Shift to full s64 range (Q16->Q62)
    if( (x ^ (x << 1)) < 0)        // Test for quadrant 1 or 2
       x = ((int64_t)1 << 63) - x;
    x = x >> (62 - RADIX);

    //x2 =     MULT(x, x);   // Ax - Bx^3 + Cx^5
    //o  = B - MULT(C, x2);  // Ax - x(Bx^2 - Cx^4)
    //o  = A - MULT(o, x2);  // x(A - (Bx^2 - Cx^4)
    //o  =     MULT(o, x);   // x(A - x^2(B - Cx^2))

    x2 =     ((x * x)  >> 8);
    o  = B - ((C * x2) >> 24);
    o  = A - ((o * x2) >> 24);
    o  =     ((o * x)  >> 16);

    // Ax - Bx^3 + Cx^5 - Dx^7
    // Ax - x(Bx^2 - Cx^4 + Dx^6)
    // x(A - (Bx^2 - Cx^4 + Dx^6))
    // x(A - x^2(B - Cx^2 + Dx^4))
    // x(A - x^2(B - x^2(C - Dx^2)))

    return o;
}

Sclr
eng_math_tan(Angl input)
{
   // Domain: |a| < 1.74
   Vec3 buffer;
   Sclr output;

   buffer.x = cordic_gain_c; // Cos()
   buffer.y = 0;             // Sin()
   buffer.z = input;

   buffer = cordic_circular_zmode(buffer);
   output = ANGLDIVD(buffer.y, buffer.x);
   output = ANGL2SCLR(output);

   return output;
}

Sclr
eng_math_atan(Angl input)
{
   // Domain: all a
   Vec3 buffer;
   Sclr output;

   buffer.x = ANGLBASIS;
   buffer.y = input;
   buffer.z = 0;

   buffer = cordic_circular_ymode(buffer);
   output = ANGL2SCLR(buffer.z);

   return output;
}

Sclr
eng_math_asin(Angl input) // Fixme //
{
   // Domain: |a| < 0.98

   Vec3 buffer;
   Sclr output;

   buffer.x = cordic_gain_c;
   buffer.y = input;
   buffer.z = 0;

   buffer = cordic_circular_aymode(buffer);
   output = ANGL2SCLR(buffer.z);

   //We will use the trig identity for this if there isn't a good way to vectorize it directly.
   //output = ANGL2SCLR(ATAN(DIVD(a, SQRT(1 - MULT(a, a)))));

   return output;
}


/*** Hyperbolic Functions ***/

Vec2
eng_math_sincosh(Angl input)
{
   // Domain: |a| < 1.13 OR |a| <= 1.125, after scaling,
   Vec3 buffer;
   Vec2 output;

   buffer.x = cordic_gain_h; // Cosh()
   buffer.y = 0;             // Sinh()
   buffer.z = input;

   buffer   = cordic_hyperbolic_zmode(buffer);
   output.x = buffer.x;
   output.y = buffer.y;

   return output;
}

Sclr
eng_math_tanh(Angl input)
{
   // Domain: |a| < 1.13 units.
   Vec3 buffer;
   Sclr output;

   buffer.x = cordic_gain_h; // Cosh()
   buffer.y = 0;             // Sinh()
   buffer.z = input;

   buffer = cordic_hyperbolic_zmode(buffer);
   output = ANGLDIVD(buffer.y, buffer.x);

   return output;
}

Sclr
eng_math_atanh(Angl input)
{
   // Domain: |a| < 1.13 units.
   Vec3 buffer;
   Sclr output;

   buffer.x = ANGLBASIS;
   buffer.y = input;
   buffer.z = 0;         // Atanh()

   buffer = cordic_hyperbolic_ymode(buffer);
   output = buffer.z;

   return output;
}

