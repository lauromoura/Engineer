#ifndef _ENG_MATH_QUAT_H_
#define _ENG_MATH_QUAT_H_

#include "../type/quat.h"
#include "sclr.h"

Quat eng_math_quat_multiply(Quat multiplier, Quat multiplicand);
Quat eng_math_quat_invert(Quat input);
Quat eng_math_quat_normalize(Quat input);

#endif

