#include "aabbtree.h"

typedef struct
{
   uint32_t parent;
   uint32_t mask;

   AABB boundary;

   union
   {
      Sclr data;
      uint32_t child[2];
   }
   payload;
}
Node;

State_Array *
eng_math_aabbtree_new(unsigned int step)
{
   State_Array *aabbtree;

   aabbtree = (State_Array*)eng_state_array_new(sizeof(Node), step);

   return aabbtree;
}

void
eng_math_aabbtree_free(State_Array *aabbtree)
{
   eng_state_array_free(aabbtree);
}

void
eng_math_aabbtree_insert(const State_Array *tree,
        Sclr payload, AABB boundary)
{
   AABB ubuffer;
   Node *node, nbuffer, *lchild, *rchild;
   node = &nbuffer;

   if(eng_state_array_count(tree) > 0)
   {
      // Walk down the tree, taking the most appropriate branch for the input until we hit a leaf.
      uint32_t current = 0;
      while(current != UINT_NULL)
      {
         node = eng_state_array_read(tree, current);

         // Check to see if the current node is a branch.
         if(node->mask == EINA_TRUE)
         {
            // Determine which side of the branch to take, and set the current node to it.
            Sclr leftp, rightp;

            lchild  = eng_state_array_read(tree, node->payload.child[0]);
            ubuffer = eng_math_aabb_union(lchild->boundary, boundary);
            leftp   = eng_math_aabb_perimeter(ubuffer)
                    - eng_math_aabb_perimeter(lchild->boundary);

            rchild  = eng_state_array_read(tree, node->payload.child[1]);
            ubuffer = eng_math_aabb_union(rchild->boundary, boundary);
            rightp  = eng_math_aabb_perimeter(ubuffer)
                    - eng_math_aabb_perimeter(rchild->boundary);

            current = (leftp < rightp) ? node->payload.child[0] : node->payload.child[1];
         }
         else
         {
            // Split the current leaf into a new branch, and insert the relevant entities into it.
            Node lbuffer, rbuffer;

            lchild                = &lbuffer;
            lchild->parent        = current;
            lchild->mask          = EINA_FALSE;
            lchild->boundary      = node->boundary;
            lchild->payload.data  = node->payload.data;

            rchild                = &rbuffer;
            rchild->parent        = current;
            rchild->mask          = EINA_FALSE;
            rchild->boundary      = boundary;
            rchild->payload.data  = payload;

            node->mask             = EINA_TRUE;
            node->boundary         = eng_math_aabb_union(node->boundary, boundary);
            node->payload.child[0] = eng_state_array_push((State_Array*)tree, lchild);
            node->payload.child[1] = eng_state_array_push((State_Array*)tree, rchild);

            current = UINT_NULL;
         }
      }

      // Walk back up the tree making sure all ancestral bounds contain thier children.
      if(node->parent != UINT_NULL)
      {
         ubuffer = node->boundary;
         node = eng_state_array_read(tree, node->parent);
         while(!eng_math_aabb_subsect(ubuffer, node->boundary))
         {
            lchild = eng_state_array_read(tree, node->payload.child[0]);
            rchild = eng_state_array_read(tree, node->payload.child[1]);
            node->boundary = eng_math_aabb_union(lchild->boundary, rchild->boundary);

            if(node->parent == UINT_NULL) break;
            ubuffer = node->boundary;
            node = eng_state_array_read(tree, node->parent);
         }
      }
   }
   else
   {
      node->parent        = UINT_NULL;
      node->mask          = EINA_FALSE;
      node->boundary      = boundary;
      node->payload.data  = payload;

      eng_state_array_push((State_Array*)tree, node);
   }
}

void
eng_math_aabbtree_swap(const State_Array *tree EINA_UNUSED,
        uint32_t indexa EINA_UNUSED, uint32_t indexb EINA_UNUSED)
{
}

void
eng_math_aabbtree_remove(const State_Array *tree EINA_UNUSED,
        uint32_t index EINA_UNUSED)
{
}

Eina_Inarray *
eng_math_aabbtree_intersect_elements(const State_Array *tree EINA_UNUSED)
{
   return NULL;
}

Eina_Inarray *
eng_math_aabbtree_intersect_ray(const State_Array *tree EINA_UNUSED, Vec3 input EINA_UNUSED)
{
   return NULL;
}

Eina_Inarray *
eng_math_aabbtree_intersect_aabb(const State_Array *tree,
        AABB input)
{
   Eina_Inarray *buffer, *result;
   uint32_t *current;
   Node *node;

   if(eng_state_array_count((State_Array*)tree) > 0)
   {
      buffer = eina_inarray_new(sizeof(uint32_t), 0);
      result = eina_inarray_new(sizeof(EntityID), 0);

      eina_inarray_push(buffer, 0);
      while(eina_inarray_count(buffer) > 0)
      {
         current = eina_inarray_pop(buffer);
         node = eng_state_array_read(tree, *current);

         if(eng_math_aabb_intersect(input, node->boundary))
         {
            if(node->mask == EINA_TRUE)
            {
               eina_inarray_push(result, &node->payload.data);
            }
            else
            {
               // Add both children to the todo buffer.
               eina_inarray_push(buffer, &node->payload.child[0]);
               eina_inarray_push(buffer, &node->payload.child[1]);
            }
         }
      }

      eina_inarray_free(buffer);
      return result;
   }
   return NULL;
}

Eina_Inarray *
eng_math_aabbtree_intersect_sphere(const State_Array *tree,
        Sphere input)
{
   Eina_Inarray *buffer, *result;
   uint32_t     *current;
   Node         *node;

   if(eng_state_array_count(tree) > 0)
   {
      buffer = eina_inarray_new(sizeof(uint32_t), 0);
      result = eina_inarray_new(sizeof(EntityID), 0);

      eina_inarray_push(buffer, 0);
      while(eina_inarray_count(buffer) > 0)
      {
         current = eina_inarray_pop(buffer);
         node = eng_state_array_read(tree, *current);

         if(eng_math_aabb_intersect_sphere(node->boundary, input))
         {
            if(node->mask == EINA_TRUE)
            {
               eina_inarray_push(result, &node->payload.data);
            }
            else
            {
               // Add both children to the todo buffer.
               eina_inarray_push(buffer, &node->payload.child[0]);
               eina_inarray_push(buffer, &node->payload.child[1]);
            }
         }
      }

      eina_inarray_free(buffer);
      return result;
   }
   return NULL;
}

#undef Node
