#ifndef _ENG_MATH_AABBTREE_H_
#define _ENG_MATH_AABBTREE_H_

#include "Eina.h"
#include "../tool/array.h"
#include "../type/aabbnode.h"
#include "aabb.h"
#include "sphere.h"

State_Array *
eng_math_aabbtree_new(unsigned int step);

void
eng_math_aabbtree_free(State_Array *aabbtree);

void
eng_math_aabbtree_insert(const State_Array *tree, Sclr payload, AABB boundary);

void
eng_math_aabbtree_swap(const State_Array *tree, uint32_t indexa, uint32_t indexb);

void
eng_math_aabbtree_remove(const State_Array *tree, uint32_t index);

Eina_Inarray *
eng_math_aabbtree_intersect_elements(const State_Array *tree);

Eina_Inarray *
eng_math_aabbtree_intersect_ray(const State_Array *tree, Vec3 input);

Eina_Inarray *
eng_math_aabbtree_intersect_aabb(const State_Array *tree, AABB input);

Eina_Inarray *
eng_math_aabbtree_intersect_sphere(const State_Array *tree, Sphere input);

Eina_Inarray *
eng_math_aabbtree_intersect_frustum(const State_Array *tree, Sphere input);

#endif

