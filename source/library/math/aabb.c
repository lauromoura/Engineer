#include "aabb.h"

Sclr
eng_math_aabb_perimeter(AABB input)
{
   return 2 * (MULT(input.bounds.x, input.bounds.x)
             + MULT(input.bounds.y, input.bounds.y)
             + MULT(input.bounds.z, input.bounds.z));
}

AABB
eng_math_aabb_union(AABB a, AABB b)
{
   AABB output;

   Vec3
      amax = {a.position.x + a.bounds.x, a.position.y + a.bounds.y, a.position.z + a.bounds.z},
      amin = {a.position.x - a.bounds.x, a.position.y - a.bounds.y, a.position.z - a.bounds.z},
      bmax = {b.position.x + b.bounds.x, b.position.y + b.bounds.y, b.position.z + b.bounds.z},
      bmin = {b.position.x - b.bounds.x, b.position.y - b.bounds.y, b.position.z - b.bounds.z};

   amax.x = (amax.x > bmax.x) ? amax.x : bmax.x;
   amax.y = (amax.y > bmax.y) ? amax.y : bmax.y;
   amax.z = (amax.z > bmax.z) ? amax.z : bmax.z;
   amin.x = (amin.x < bmin.x) ? amin.x : bmin.x;
   amin.y = (amin.y < bmin.y) ? amin.y : bmin.y;
   amin.z = (amin.z < bmin.z) ? amin.z : bmin.z;

   output.bounds.x = (amax.x - amin.x) / 2;
   output.bounds.y = (amax.y - amin.y) / 2;
   output.bounds.z = (amax.z - amin.z) / 2;

   output.position.x = amin.x + output.bounds.x;
   output.position.y = amin.y + output.bounds.y;
   output.position.z = amin.z + output.bounds.z;

   return output;
}

Eina_Bool
eng_math_aabb_intersect(AABB a, AABB b)
{
   Vec3
      amax = {a.position.x + a.bounds.x, a.position.y + a.bounds.y, a.position.z + a.bounds.z},
      amin = {a.position.x - a.bounds.x, a.position.y - a.bounds.y, a.position.z - a.bounds.z},
      bmax = {b.position.x + b.bounds.x, b.position.y + b.bounds.y, b.position.z + b.bounds.z},
      bmin = {b.position.x - b.bounds.x, b.position.y - b.bounds.y, b.position.z - b.bounds.z};

   return
      (amin.x <= bmax.x) && (amax.x >= bmin.x) &&
      (amin.y <= bmax.y) && (amax.y >= bmin.y) &&
      (amin.z <= bmax.z) && (amax.z >= bmin.z);
}

Eina_Bool
eng_math_aabb_intersect_sphere(AABB a, Sphere b)
{
   Vec3
      amax = {a.position.x + a.bounds.x, a.position.y + a.bounds.y, a.position.z + a.bounds.z},
      amin = {a.position.x - a.bounds.x, a.position.y - a.bounds.y, a.position.z - a.bounds.z};

  Sclr distance;
  Vec3 separation;

  separation.x = CLAMP(b.position.x, amin.x, amax.x) - b.position.x;
  separation.y = CLAMP(b.position.y, amin.y, amax.y) - b.position.y;
  separation.z = CLAMP(b.position.z, amin.z, amax.z) - b.position.z;
  distance = eng_math_vec3_length(separation);

  // The AABB and the sphere overlap if the closest point within the rectangle is
  // within the sphere's radius.
  return distance < b.radius;

}

Eina_Bool
eng_math_aabb_subsect(AABB a, AABB b)
{
   Vec3
      amax = {a.position.x + a.bounds.x, a.position.y + a.bounds.y, a.position.z + a.bounds.z},
      amin = {a.position.x - a.bounds.x, a.position.y - a.bounds.y, a.position.z - a.bounds.z},
      bmax = {b.position.x + b.bounds.x, b.position.y + b.bounds.y, b.position.z + b.bounds.z},
      bmin = {b.position.x - b.bounds.x, b.position.y - b.bounds.y, b.position.z - b.bounds.z};

   return
      (amax.x <= bmax.x) && (amin.x >= bmin.x) &&
      (amax.y <= bmax.y) && (amin.y >= bmin.y) &&
      (amax.x <= bmax.z) && (amin.z >= bmin.z);
}

