#ifndef _ENG_MATH_AABB_H_
#define _ENG_MATH_AABB_H_

#include "../type/aabb.h"
#include "sphere.h"

Sclr
eng_math_aabb_perimeter(AABB input);

AABB
eng_math_aabb_union(AABB a, AABB b);

Eina_Bool
eng_math_aabb_intersect(AABB a, AABB b);

Eina_Bool
eng_math_aabb_intersect_sphere(AABB a, Sphere b);

Eina_Bool
eng_math_aabb_subsect(AABB a, AABB b);

#endif

