#ifndef _ENG_MATH_SCLR_H_
#define _ENG_MATH_SCLR_H_

#include "../type/sclr.h"
#include "vec3.h"
#include "cordic.h"

#define MULT(a, b)     eng_math_mult(a, b)
#define DIVD(a, b)     eng_math_divd(a, b)

#define ABS(a)         eng_math_abs(a)
#define CLAMP(a, b, c) eng_math_clamp(a, b, c)
#define EXP(a)         eng_math_exp(a)
#define LOG(a)         eng_math_ln(a)
#define SQRT(a)        eng_math_sqrt(a)

Sclr eng_math_mult(Sclr multiplicand, Sclr multiplier);
Sclr eng_math_divd(Sclr dividend,     Sclr divisor);

Sclr eng_math_abs(Sclr input);
Sclr eng_math_clamp(Sclr input, Sclr min, Sclr max);

Sclr eng_math_exp(Sclr input);
Sclr eng_math_ln(Sclr input);
Sclr eng_math_sqrt(Sclr input);

#endif

