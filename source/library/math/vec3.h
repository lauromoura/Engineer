#ifndef _ENG_MATH_VEC3_H_
#define _ENG_MATH_VEC3_H_

#include "../type/vec3.h"
#include "sclr.h"

#define VEC3SCLE(a, b)   eng_math_vec_scale(a, b)
#define VEC3DOT(a, b)    eng_math_vec3_dot(a, b)
#define VEC3CROSS(a, b)  eng_math_vec3_cross(a, b)
#define VEC3NORM(a, b)   eng_math_vec3_normalize(a)

Sclr eng_math_vec3_length(Vec3 input);
Vec3 eng_math_vec3_scale(Vec3 input, Sclr factor);
Sclr eng_math_vec3_dot(Vec3 inputa, Vec3 inputb);
Vec3 eng_math_vec3_cross(Vec3 inputa, Vec3 inputb);
Vec3 eng_math_vec3_normalize(Vec3 input);

#endif

