#ifndef _ENG_MATH_VEC2_H_
#define _ENG_MATH_VEC2_H_

#include "../type/vec2.h"
#include "sclr.h"

//#define VEC2MULT(a, b)   eng_math_vec2_mult(a, b)
//#define VEC2DIVD(a, b)   eng_math_vec2_divd(a, b)
#define VEC2SCLE(a, b)   eng_math_vec2_scale(a, b)
#define VEC2DOT(a, b)    eng_math_vec2_dot(a, b)
#define VEC2NORM(a)      eng_math_vec2_normalize(a)

//Vec2 eng_math_vec2_mult(Vec2 va, Vec2 vb);
//Vec2 eng_math_vec2_divd(Vec2 va, Vec2 vb);
Vec2 eng_math_vec2_scale(Vec2 input, Sclr factor);
Sclr eng_math_vec2_dot(Vec2 inputa, Vec2 inputb);
Vec2 eng_math_vec2_normalize(Vec2 input);

#endif

