#include "eulr.h"

Quat
eng_math_eulr_to_quat(Eulr input)
{
   Sclr cosr, cosp, cosy, sinr, sinp, siny, cycr, sysr;
   Quat output;

   cosy = MULT(COS(input.yaw), BASIS/2);
   siny = MULT(SIN(input.yaw), BASIS/2);

   cosp = MULT(COS(input.pitch), BASIS/2);
   sinp = MULT(SIN(input.pitch), BASIS/2);

   cosr = MULT(COS(input.roll), BASIS/2);
   sinr = MULT(SIN(input.roll), BASIS/2);

   cycr = MULT(cosy, cosr);
   sysr = MULT(siny, sinr);

   output.w = MULT(cosp, cycr) + MULT(sinp, sysr);
   output.x = MULT(sinp, cycr) - MULT(cosp, sysr);
   output.y = MULT(MULT(sinp, sinr), cosy) + MULT(MULT(cosp, cosr), siny);
   output.z = MULT(MULT(sinp, cosr), siny) - MULT(MULT(cosp, sinr), cosy);

   return eng_math_quat_normalize(output);
}
