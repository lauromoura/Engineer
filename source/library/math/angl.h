#ifndef _ENG_MATH_ANGL_H_
#define _ENG_MATH_ANGL_H_

#include "../type/angl.h"
#include "vec2.h"
#include "vec3.h"
#include "cordic.h"

#define ANGLMULT(a, b) eng_math_angl_mult(a, b)
#define ANGLDIVD(a, b) eng_math_angl_divd(a, b)

#define SINCOS(a) eng_math_sincos(a)
#define SIN(a)    eng_math_sin(a)
#define COS(a)    eng_math_cos(a)
#define TAN(a)    eng_math_tan(a)
#define ATAN(a)   eng_math_atan(a)
#define ASIN(a)   eng_math_asin(a)

#define SINCOSH(a) eng_math_sincosh(a)
#define TANH(a)    eng_math_tanh(a)
#define ATANH(a)   eng_math_atanh(a)

Angl eng_math_angl_mult(Angl multiplicand, Angl multiplier);
Angl eng_math_angl_divd(Angl dividend,     Angl divisor);

Vec2 eng_math_sincos(Angl input);
Sclr eng_math_sin(Angl input);
Sclr eng_math_cos(Angl input);
Sclr eng_math_tan(Angl input);
Sclr eng_math_atan(Angl input);
Sclr eng_math_asin(Angl input);

Vec2 eng_math_sincosh(Angl input);
Sclr eng_math_tanh(Angl input);
Sclr eng_math_atanh(Angl input);

#endif

