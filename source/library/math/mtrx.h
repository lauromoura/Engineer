#ifndef _ENG_MATH_MTRX_H_
#define _ENG_MATH_MTRX_H_

#include "../type/mtrx.h"
#include "quat.h"

Quat eng_math_mtrx_mult_by_quat(Mtrx multiplicand, Quat multiplier);
Mtrx eng_math_mtrx_rotation_setup(Quat input);

#endif

