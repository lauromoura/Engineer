#include "sets.h"

Set *
eng_math_set_union(Set *a, Set *b)
{
   Eina_Inarray *result = eina_inarray_new(sizeof(uint64_t), 0);
   int64_t targeta, targetb;
   Indx i = 0;
   Indx j = 0;
   Indx m = eina_inarray_count((Eina_Inarray*)a);
   Indx n = eina_inarray_count((Eina_Inarray*)b);

   while (i < m && j < n)
   {
      targeta = *(Indx*)eina_inarray_nth(a, i);
      targetb = *(Indx*)eina_inarray_nth(b, j);
      if      (targeta <  targetb)  { eina_inarray_push(result, &targeta); i += 1; }
      else if (targeta >  targetb)  { eina_inarray_push(result, &targetb); j += 1; }
      else  /*(targeta == targetb)*/{ eina_inarray_push(result, &targeta); i += 1; j += 1; }
   }

   while(i < m) { eina_inarray_push(result, &targeta); i += 1; }
   while(j < n) { eina_inarray_push(result, &targetb); j += 1; }

   return result;
}

uSet *
eng_math_uset_union(uSet *a, uSet *b)
{
   Eina_Inarray *result = eina_inarray_new(sizeof(uint64_t), 0);
   uint64_t targeta, targetb;
   Indx i = 0;
   Indx j = 0;
   Indx m = eina_inarray_count((Eina_Inarray*)a);
   Indx n = eina_inarray_count((Eina_Inarray*)b);

   while (i < m && j < n)
   {
      targeta = *(Indx*)eina_inarray_nth(a, i);
      targetb = *(Indx*)eina_inarray_nth(b, j);
      if      (targeta <  targetb)  { eina_inarray_push(result, &targeta); i += 1; }
      else if (targeta >  targetb)  { eina_inarray_push(result, &targetb); j += 1; }
      else  /*(targeta == targetb)*/{ eina_inarray_push(result, &targeta); i += 1; j += 1; }
   }

   while(i < m) { eina_inarray_push(result, &targeta); i += 1; }
   while(j < n) { eina_inarray_push(result, &targetb); j += 1; }

   return result;
}
