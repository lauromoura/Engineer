#ifndef _ENG_MATH_SETS_H_
#define _ENG_MATH_SETS_H_

#include "../type/indx.h"

typedef Eina_Inarray Set;
typedef Eina_Inarray uSet;

Set *
eng_math_set_union(Set *a, Set *b);

uSet *
eng_math_uset_union(uSet *a, uSet *b);

#endif
