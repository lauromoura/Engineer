#include "eng_viewport.h"

// eolian_gen `pkg-config --variable=eolian_flags ecore eo evas efl` -I .
//    -I "/home/brokenshakles/.efler/efl/src/lib/" -g h engineer_viewport.eo

/*** Efl_Object Section ***/

EOLIAN Eo *
eng_viewport_add(Eo *window)
{
   Eo *viewport;

   //efl_domain_current_push(EFL_ID_DOMAIN_SHARED);
   viewport = efl_add(ENG_VIEWPORT_CLASS, window);
   //efl_domain_current_pop();

   return viewport;
}

EOLIAN static Efl_Object *
_eng_viewport_efl_object_constructor(Eo *obj, Eng_Viewport_Data *pd EINA_UNUSED)
{
   return efl_constructor(efl_super(obj, ENG_VIEWPORT_CLASS));
}

Efl_Object *
_eng_viewport_efl_object_finalize(Eo *obj, Eng_Viewport_Data *pd)
{
   obj = efl_finalize(efl_super(obj, ENG_VIEWPORT_CLASS));

   pd->camera = (EntityID)-1;
   pd->frame  = elm_box_add(efl_parent_get(obj));

   elm_win_resize_object_add(efl_parent_get(obj), pd->frame);
   evas_object_size_hint_weight_set(pd->frame, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   evas_object_size_hint_align_set(pd->frame, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_show(pd->frame);

   return obj;
}

EOLIAN static void
_eng_viewport_efl_object_destructor(Eo *obj EINA_UNUSED, Eng_Viewport_Data *pd EINA_UNUSED)
{
   efl_destructor(efl_super(obj, ENG_VIEWPORT_CLASS));
}

EOLIAN static Efl_Object *
_eng_viewport_frame_get(Eo *obj EINA_UNUSED, Eng_Viewport_Data *pd)
{
   return pd->frame;
}

EOLIAN static void
_eng_viewport_alpha_set(Eo *obj EINA_UNUSED, Eng_Viewport_Data *pd,
        char alpha)
{
   pd->alpha = alpha;
}

EOLIAN static char
_eng_viewport_alpha_get(Eo *obj EINA_UNUSED, Eng_Viewport_Data *pd)
{
   return pd->alpha;
}

EOLIAN static EntityID
_eng_viewport_camera_get(Eo *obj EINA_UNUSED, Eng_Viewport_Data *pd)
{
   return pd->camera;
}

EOLIAN static void
_eng_viewport_look(Eo *obj, Eng_Viewport_Data *pd,
        EntityID camera)
{
   Eng_Scene *scene;

   if(pd->camera != (EntityID)-1) return;
   pd->camera = camera;

   printf("Viewport Look Checkpoint. Viewport Pointer: %p\n", obj);

   scene = eng_node_entity_scene_get(eng_node(), camera);
   eng_scene_entity_notify_from_efl(
      scene,
      camera,
      "viewport_attach",
      (Data*)&pd->frame
   );
}

EOLIAN static EntityID
_eng_viewport_ignore(Eo *obj EINA_UNUSED, Eng_Viewport_Data *pd)
{
   if(pd->camera == (EntityID)-1) return pd->camera;
   pd->camera = (EntityID)-1;

   return pd->camera;
}

#include "eng_viewport.eo.c"
