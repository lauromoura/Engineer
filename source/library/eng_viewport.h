#ifndef _ENG_VIEWPORT_H_
#define _ENG_VIEWPORT_H_

#include "engineer.h"
#include <math.h>

typedef struct
{
   uint     alpha;
   Evas_Object *frame;
   EntityID camera;  // The camera that the Viewport is looking through.
}
Eng_Viewport_Data;

EOLIAN Eo *
eng_viewport_add(Eo *obj);

#include "eng_viewport.eo.h"

#endif

