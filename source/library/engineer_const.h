#ifndef _ENGINEER_LIBRARY_GLOBAL_CONSTANTS_H_
#define _ENGINEER_LIBRARY_GLOBAL_CONSTANTS_H_

// We need a standard stringification macro.
#define STRINGIFY(x)  STRINGIFY2(x)
#define STRINGIFY2(x) #x

// It's useful to set up some macro's to store the target hardware cache page size.
// PAGE_SIZE must be a power of two,
#define PAGE_SIZE          64
#define PAGE_ALIGN(offset) ((offset + (PAGE_SIZE - 1)) & ~(PAGE_SIZE - 1))

// This sets the base word size/data types for all number storage.
//    The base should be set equal in size to the native bitwidth of the target CPU arch.
//    Minimum 64 bits.
#define BASE int64_t
#define HALF int32_t

// This sets the (signed) Scalar type/bitwidth used by the Engineer.so library.
#define SCALE (sizeof(BASE) * 8)

// This sets, from the right, where our radix point will be placed in our Word.
#define RADIX 16
#define BASIS ((BASE)1 << RADIX)

#define ANGLRADIX 31
#define ANGLBASIS ((BASE)1 << ANGLRADIX)

#define ANGL2SCLR(a) (a >> (ANGLRADIX - RADIX));
#define SCLR2ANGL(a) (a << (ANGLRADIX - RADIX));

#define UINT_NULL  ~((uint)0)
#define ULONG_NULL ~((ulong)0)

#define PI (BASE)(3.1415926536897932384626 * BASIS)

// Some shorthand for atomic operations.
#define LOAD(a)       atomic_load(a)
#define STORE(a, b)   atomic_store(a, b)
#define AFA(a, b)     atomic_fetch_add(a, b)
#define AAF(a, b)     atomic_add_fetch(a, b)
#define AFS(a, b)     atomic_fetch_sub(a, b)
#define CAS(a, b, c)  atomic_compare_exchange_weak(a, b, c)
#define CASS(a, b, c) atomic_compare_exchange_strong(a, b, c)

#endif
