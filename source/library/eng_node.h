#ifndef _ENG_NODE_H_
#define _ENG_NODE_H_

#include "engineer.h"

// EntityID's and ComponentID's are each 64 bits long, with the largest 24 bits of the number
//    specifying the cluster Ip address of the originating Node. This allows compatibility with
//    subsets of both private network ranges of IpV4 and IpV6 (10.xxx.xxx.xxx for the former).
typedef struct
{
   String *game; // The title/label of the game the node is (partially) hosting.
   String *path; // Where the Game's Asset folder is located relative to the root drive.

   int        mode;      // Stores the mode of this Node, standalone(0), client(1), server(2).
   int        ipaddress; // The 24 bit cluster Ip subaddress of the host of this node.
   Eina_Hash *peers;     // A list of connected server peer nodes.

   // This is where we keep references to our console GUI widgets.
   Eina_Hash *consoles;     // Stores the set of all consoles on this node.
   Eina_Hash *notice_sizes; // Stores size data for notice/event passing

   // Scenes and Scene Modules
   Eina_Hash *scenes;            // Our active scene Efl_Objects, referenced by thier proper names.
   Eina_Hash *component_classes; // Finds the Component class reference, keyed by it's murmur3 hash.
   Eina_Hash *system_classes;    // System Class Reference

   // EntityID Take-a-Ticket System.
   uint64_t      entity_count;  // Stores the total amount of EntityID's originated by the Node.
   Eina_Inarray *entity_queue;  // Stores this node's (presently) available EntityID's.

   // Active Entity Data Cache.
   Eina_Hash *entity_scene;  // Stores the current scene of each Entity loaded into this Node.

   //Eina_Hash *templates;
}
Eng_Node_Data;

EAPI Eo *
eng_node();

EAPI EntityID
eng_node_entity();

#include "eng_node.eo.h"

#endif
