#ifndef _ENG_QUEUE_H_
#define _ENG_QUEUE_H_

#include <stdatomic.h>
#include <stdlib.h>
#include <Ecore_Getopt.h>
#include <Ecore.h>
#include "../engineer_const.h"

typedef enum
{
   VACANT   = 0,
   FILLING  = 1,
   WAITING  = 2,
   DRAINING = 4
}
Eng_Queue_Payload_Flag;

typedef struct
{
   _Atomic uint32_t flag;
   _Atomic uint32_t size;
           char     data[];
}
Eng_Queue_Payload;

typedef struct
{
   _Atomic uint32_t capacity;
   _Atomic uint32_t occupancy;

   _Atomic  int16_t producers;
   _Atomic  int16_t consumers;

   _Atomic uint32_t head_in;
   _Atomic uint32_t head_out;

   _Atomic uint32_t tail_in;
   _Atomic uint32_t tail_out;

   _Atomic uint16_t fill_task_max;
   _Atomic uint16_t fill_task_count;

   Ecore_Thread_Cb fill_task;
   Ecore_Thread_Cb fill_end;
   Ecore_Thread_Cb fill_cancel;

   _Atomic uint8_t *payload;
}
Eng_Queue;

Eng_Queue* eng_queue_new(uint32_t size);
Eina_Bool  eng_queue_fill_task_set(Eng_Queue *queue,
                                   Ecore_Thread_Cb  func_blocking,
                                   Ecore_Thread_Cb  func_end,
                                   Ecore_Thread_Cb  func_cancel);

void      eng_queue_fill_task_max_set(Eng_Queue *queue, uint32_t new_maximum);
void*     eng_queue_fill(Eng_Queue *queue, uint32_t size);
Eina_Bool eng_queue_fill_done(Eng_Queue *queue, void *payload);
void*     eng_queue_drain(Eng_Queue *queue);
Eina_Bool eng_queue_drain_done(Eng_Queue *queue, void *payload);
Eina_Bool eng_queue_resize(Eng_Queue *queue, uint32_t size);
void      eng_queue_flush(Eng_Queue *queue);
void      eng_queue_free(Eng_Queue *queue);

#endif

