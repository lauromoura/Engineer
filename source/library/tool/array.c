#include "array.h"

/*** Module Array Access Methods ***/

State_Array *
eng_state_array_new(unsigned int member_size, unsigned int step)
{
   State_Array *array;
   if(!step) step = ARRAY_STEP_SIZE;
   array = malloc(sizeof(State_Array));

   EINA_MAGIC_SET(&array->payload, EINA_MAGIC_INARRAY);
   array->payload.version = EINA_ARRAY_VERSION;
   array->payload.member_size = member_size;
   array->payload.len = 0;
   array->payload.max = 0;
   array->payload.step = step;
   array->payload.members = NULL;

   array->touched = EINA_FALSE;

   return array;
}

void
eng_state_array_free(State_Array *target EINA_UNUSED)
{
}

uint32_t
eng_state_array_count(const State_Array *array)
{
   return eina_inarray_count(&array->payload);
}

int
eng_state_array_push(State_Array *array, void *input)
{
   array->touched = EINA_TRUE;
   return eina_inarray_push(&array->payload, input);
}

void *
eng_state_array_pop(State_Array *array)
{
   array->touched = EINA_TRUE;
   return eina_inarray_pop(&array->payload);
}

Eina_Bool
eng_state_array_insert(State_Array *array, uint32_t index, void *input)
{
   array->touched = EINA_TRUE;
   return eina_inarray_insert_at(&array->payload, index, input);
}

Eina_Bool
eng_state_array_remove(State_Array *array, uint32_t index)
{
   array->touched = EINA_TRUE;
   return eina_inarray_remove_at(&array->payload, index);
}

void *
eng_state_array_read(const State_Array *array, uint32_t index)
{
   return eina_inarray_nth(&array->payload, index);
}

Eina_Bool
eng_state_array_write(State_Array *array, uint32_t index, const void *input)
{
   array->touched = EINA_TRUE;
   return eina_inarray_replace_at(&array->payload, index, input);
}

void
eng_state_array_flush(State_Array *array)
{
   array->touched = EINA_TRUE;
   eina_inarray_flush(&array->payload);
}


