#ifndef _ENG_STATE_ARRAY_H_
#define _ENG_STATE_ARRAY_H_

#define EINA_MAGIC_INARRAY 0x98761270
#include "Eina.h"

#ifndef ARRAY_STEP_SIZE
   #define ARRAY_STEP_SIZE 32
#endif

typedef struct
{
   Eina_Inarray payload;
   Eina_Bool    touched;
}
State_Array;

/*** Module Array Manipulation Functions ***/
State_Array * eng_state_array_new(unsigned int member_size, unsigned int step);
void          eng_state_array_free(State_Array *target);
uint32_t      eng_state_array_count(const State_Array *array);
int           eng_state_array_push(State_Array *array, void *input);
void *        eng_state_array_pop(State_Array *array);
Eina_Bool     eng_state_array_insert(State_Array *array, uint32_t index, void *input);
Eina_Bool     eng_state_array_remove(State_Array *array, uint32_t index);
void *        eng_state_array_read(const State_Array *array, uint32_t index);
Eina_Bool     eng_state_array_write(State_Array *array, uint32_t index, const void *input);
void          eng_state_array_flush(State_Array *array);

#endif

