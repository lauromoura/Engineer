#include "queue.h"

#define MAX_BACKOFF  8
#define DEFAULT_CAPACITY PAGE_SIZE * 64

_Atomic uint64_t message_count = 0;
_Atomic uint64_t worker_block_aquisitions = 0;

struct _Workers
{
   int16_t producers;
   int16_t consumers;
};

static void
_fill_task_end_wrapper(void *data, Ecore_Thread *thread)
{
   Eng_Queue *queue = data;

   if(queue->fill_end) queue->fill_end(data, thread);
   AFS(&queue->fill_task_count, 1);
}

static void
_fill_task_cancel_wrapper(void *data, Ecore_Thread *thread)
{
   Eng_Queue *queue = data;

   if(queue->fill_cancel) queue->fill_cancel(data, thread);
   AFS(&queue->fill_task_count, 1);
}

static void
_fill_task_event_cb(void *data)
{
   Eng_Queue *queue = data;

   ecore_thread_run(
      queue->fill_task,
      _fill_task_end_wrapper,
      _fill_task_cancel_wrapper,
      queue
   );
}

static void
_fill_task_run(Eng_Queue *queue)
{
   uint16_t count;

   count = LOAD(&queue->fill_task_count);
   if(queue->fill_task                    &&
      LOAD(&queue->fill_task_max) > count &&
      CAS(&queue->fill_task_count, &count, count + 1))
   { ecore_main_loop_thread_safe_call_async(_fill_task_event_cb, queue); }
}

static Eina_Bool
_worker_register(_Atomic int16_t *target)
{
   int16_t cpu_pop_old, cpu_pop_new;

   do
   {
      cpu_pop_old = LOAD(target);

      if(cpu_pop_old == SHRT_MAX      // Queue is disabled.
      || cpu_pop_old == SHRT_MAX - 1) // Queue cannot accept any more producers.
         return EINA_FALSE;
      cpu_pop_new = cpu_pop_old + 1;
   }
   while(cpu_pop_old < 0 || !CAS(target, &cpu_pop_old, cpu_pop_new));

   return EINA_TRUE;
}

static void
_worker_deregister(_Atomic int16_t *target)
{
   int16_t cpu_pop_old, cpu_pop_new;

   do
   {
      cpu_pop_old = LOAD(target);

      if(cpu_pop_old < 0) cpu_pop_new = cpu_pop_old + 1;
      else                cpu_pop_new = cpu_pop_old - 1;
   }
   while(!CAS(target, &cpu_pop_old, cpu_pop_new));
}

static Eina_Bool
_worker_block(Eng_Queue *queue)
{
   struct  _Workers *cpu_new;
   _Atomic int32_t  *population;
           int32_t   data_old, data_new;

   // Order all new producers/consumers to spin/sleep in front of the queue until unblock is called.
   do
   {
      population = (_Atomic int32_t*)&queue->producers;
      data_old   = LOAD(population);
      data_new   = data_old;
      cpu_new    = (struct _Workers*)&data_new;

      if(cpu_new->producers > -1) data_new = ~data_new;
      else return EINA_FALSE; // If the queue is already being altered, fail.
   }
   while(!CAS(population, &data_old, data_new));

   // Wait for all current producers/consumers to finish.
   while(LOAD(population) != -1) continue;

   return EINA_TRUE;
}

static Eina_Bool
_worker_unblock(Eng_Queue *queue)
{
   struct  _Workers *cpu_new;
   _Atomic int32_t  *population;
           int32_t   data_old, data_new;

   do
   {
      population = (_Atomic int32_t*)&queue->producers;
      data_old   = LOAD(population);
      data_new   = data_old;
      cpu_new    = (struct _Workers*)&data_new;

      if(cpu_new->producers < 0) data_new = ~data_new;
      else return EINA_FALSE; // If the queue is not currently being altered, fail.
   }
   while(!CAS(population, &data_old, data_new));

   return EINA_TRUE;
}

Eng_Queue*
eng_queue_new(uint32_t capacity)
{
   Eng_Queue *queue;

   capacity = PAGE_ALIGN(capacity);
   if(!capacity) capacity = DEFAULT_CAPACITY;

   queue            = calloc(1, sizeof(Eng_Queue));
   queue->payload   = calloc(1, PAGE_ALIGN(capacity));
   queue->occupancy = 0;
   queue->capacity  = capacity;

   return queue;
}

Eina_Bool
eng_queue_fill_task_set(Eng_Queue *queue,
                        Ecore_Thread_Cb  func_blocking,
                        Ecore_Thread_Cb  func_end,
                        Ecore_Thread_Cb  func_cancel)
{
   if(!_worker_block(queue)) return EINA_FALSE;

   queue->fill_task   = func_blocking;
   queue->fill_end    = func_end;
   queue->fill_cancel = func_cancel;

   _worker_unblock(queue);

   return EINA_TRUE;
}

void
eng_queue_fill_task_max_set(Eng_Queue *queue, uint32_t new_maximum)
{
   STORE(&queue->fill_task_max, new_maximum);
}

void *
eng_queue_fill(Eng_Queue *queue, uint32_t size)
{
   Eng_Queue_Payload *head_in;
   uint32_t           head_prev, head_next, notice_size;
   uint32_t           old_occupancy, new_occupancy, old_capacity, new_capacity;

   if(!queue || !size) return NULL;
   notice_size = PAGE_ALIGN(sizeof(Eng_Queue_Payload) + size);

   if(!_worker_register(&queue->producers)) return NULL;

   do
   {
      old_occupancy = LOAD(&queue->occupancy);
      new_occupancy = old_occupancy + notice_size;
      old_capacity = LOAD(&queue->capacity);

      // If the queue is full...
      if(old_capacity < new_occupancy)
      {
         _worker_deregister(&queue->producers);

         new_capacity = old_capacity;
         do new_capacity = new_capacity << 1;
         while(new_capacity < new_occupancy);
         eng_queue_resize(queue, new_capacity);
         
         if(!_worker_register(&queue->producers)) return NULL;
      }
   }
   while(!CAS(&queue->occupancy, &old_occupancy, new_occupancy));

   do
   {
      head_prev = LOAD(&queue->head_in);
      head_next = (head_prev + notice_size) % queue->capacity;
      head_in   = (Eng_Queue_Payload*)(queue->payload + head_prev);
   }
   while(!CAS(&queue->head_in, &head_prev, head_next));

   STORE(&head_in->size, size);
   STORE(&head_in->flag, FILLING);

   return head_in->data;
}

Eina_Bool
eng_queue_fill_done(Eng_Queue *queue, void *payload)
{
   Eng_Queue_Payload *head_out;
   uint32_t head_prev, head_next, notice_size;

   head_out    = (Eng_Queue_Payload*)((uint8_t*)payload - sizeof(Eng_Queue_Payload));
   notice_size = PAGE_ALIGN(sizeof(Eng_Queue_Payload) + LOAD(&head_out->size));
   head_prev   = (_Atomic uint8_t*)head_out - queue->payload;
   head_next   = (head_prev + notice_size) % queue->capacity;

   while(LOAD(&head_out->flag) == FILLING &&
         CASS(&queue->head_out, &head_prev, head_next))
   {
      STORE(&head_out->flag, WAITING);

      head_out    = (Eng_Queue_Payload*)(queue->payload + head_next);
      notice_size = PAGE_ALIGN(sizeof(Eng_Queue_Payload) + LOAD(&head_out->size));
      head_prev   = head_next;
      head_next   = (head_next + notice_size) % queue->capacity;
   }

   if(queue->fill_task) _fill_task_run(queue);
   _worker_deregister(&queue->producers);

   return EINA_TRUE;
}

void *
eng_queue_drain(Eng_Queue *queue)
{
   Eng_Queue_Payload *tail_in;
   uint32_t           tail_prev, tail_next, notice_size;

   if(!_worker_register(&queue->consumers)) return NULL;

   do
   {
      tail_prev   = LOAD(&queue->tail_in);
      tail_in     = (Eng_Queue_Payload*)(queue->payload + tail_prev);
      notice_size = PAGE_ALIGN(sizeof(Eng_Queue_Payload) + LOAD(&tail_in->size));
      tail_next   = (tail_prev + notice_size) % queue->capacity;

      // If there is nothing left to drain, return NULL;
      if(LOAD(&tail_in->flag) != WAITING)
         { _worker_deregister(&queue->consumers); return NULL; }
   }
   while(!CAS(&queue->tail_in, &tail_prev, tail_next));

   STORE(&tail_in->flag, DRAINING);

   return tail_in->data;
}

Eina_Bool
eng_queue_drain_done(Eng_Queue *queue, void *payload)
{
   Eng_Queue_Payload *tail_out;
   uint32_t tail_prev, tail_next, notice_size;

   tail_out    = (Eng_Queue_Payload*)((uint8_t*)payload - sizeof(Eng_Queue_Payload));
   notice_size = PAGE_ALIGN(sizeof(Eng_Queue_Payload) + LOAD(&tail_out->size));
   tail_prev   = (_Atomic uint8_t*)tail_out - queue->payload;
   tail_next   = (tail_prev + notice_size) % queue->capacity;
   memset(payload, 0, notice_size - sizeof(Eng_Queue_Payload));

   while(LOAD(&tail_out->flag) == DRAINING &&
         CASS(&queue->tail_out, &tail_prev, tail_next))
   {
      STORE(&tail_out->size, 0);
      STORE(&tail_out->flag, VACANT);
      AFS(&queue->occupancy, notice_size);

      tail_out    = (Eng_Queue_Payload*)(queue->payload + tail_next);
      notice_size = PAGE_ALIGN(sizeof(Eng_Queue_Payload) + LOAD(&tail_out->size));
      tail_prev   = tail_next;
      tail_next   = (tail_next + notice_size) % queue->capacity;
   }

   _worker_deregister(&queue->consumers);

   return EINA_TRUE;
}

Eina_Bool
eng_queue_resize(Eng_Queue *queue, uint32_t new_capacity)
{
   //printf("m# %ld | eng_queue_resize Attempt Checkpoint.\n", AFA(&message_count, 1)); fflush(stdout);

   #define PAYLOAD_MOVE                                   \
      memmove(queue->payload  + queue->tail_out + offset, \
              queue->payload  + queue->tail_out,          \
              queue->capacity - queue->tail_out)

   int32_t   offset;

   new_capacity = PAGE_ALIGN(new_capacity);

   if(new_capacity == LOAD(&queue->capacity)) return EINA_TRUE;
   if(!_worker_block(queue))                  return EINA_FALSE;

   if(new_capacity < queue->occupancy) { _worker_unblock(queue); return EINA_FALSE; }

   offset  = new_capacity - queue->capacity;

   if(queue->occupancy == queue->capacity)
   {
      // In this case, the queue is currently full.
      queue->payload = realloc(queue->payload, new_capacity);
      PAYLOAD_MOVE;

      if(queue->head_in == queue->capacity)
      {
         queue->head_in  = 0;
         queue->head_out = 0;
      }

      queue->tail_in  += offset;
      queue->tail_out += offset;
      memset(queue->payload + queue->head_in, 0, offset);
   }
   else if(queue->tail_out < queue->head_in)
   {
      // The length of the vacant zone crosses the wrap boundary, which simplifies things, but
      //    requires the movement of the entire data set if the tail does not equal zero.
      if(queue->tail_out)
         memmove(queue->payload, queue->payload + queue->tail_out, queue->occupancy);

      queue->payload    = realloc(queue->payload, new_capacity);
      queue->head_in   -= queue->tail_out;
      queue->head_out  -= queue->tail_out;
      queue->tail_in   -= queue->tail_out;
      queue->tail_out  -= queue->tail_out;
      memset(queue->payload + queue->head_in, 0, new_capacity - queue->occupancy);
   }
   else if(queue->head_in < queue->tail_out)
   {
      // The vacant zone is entirely contiguous in this case, so we have to move the data after it.
      // Depending on which direction the data must move, we need to do it either before or after
      //    the realloc.
      if(new_capacity < queue->capacity) { PAYLOAD_MOVE; }
      queue->payload = realloc(queue->payload, new_capacity);
      if(new_capacity > queue->capacity) { PAYLOAD_MOVE; }
      memset(queue->payload + queue->tail_out, 0, offset);

      queue->tail_in  += offset;
      queue->tail_out += offset;
   }
   else // In the case that the queue is currently empty.
   {
      queue->payload = realloc(queue->payload, new_capacity);
      if(new_capacity > queue->capacity)
         memset(queue->payload + queue->capacity, 0, offset);

      queue->head_in  = 0;
      queue->head_out = 0;
      queue->tail_in  = 0;
      queue->tail_out = 0;
   }

   STORE(&queue->capacity, new_capacity);
   _worker_unblock(queue);

   //printf("m# %ld | eng_queue_resize Success Checkpoint!\n", AFA(&message_count, 1)); fflush(stdout);

   return EINA_TRUE;

   #undef PAYLOAD_MOVE
}

void
eng_queue_flush(Eng_Queue *queue)
{
   while(!_worker_block(queue)) continue;

   memset(queue->payload, 0, queue->capacity);
   queue->occupancy = 0;
   queue->head_in   = 0;
   queue->head_out  = 0;
   queue->tail_in   = 0;
   queue->tail_out  = 0;

   _worker_unblock(queue);
}

void
eng_queue_free(Eng_Queue *queue)
{
   while(!_worker_block(queue)) continue;

   free(queue->payload);
   free(queue);
}
