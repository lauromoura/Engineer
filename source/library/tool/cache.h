#ifndef CACHE_H_INCLUDED
#define CACHE_H_INCLUDED

#include <stddef.h>
size_t CacheLineSize();

#endif

// Based on work by Nick Strupat
// Date: October 29, 2010
// Returns the cache line size (in bytes) of the processor, or 0 on failure

