#ifndef _ENG_HASH_MURMUR3_H_
#define _ENG_HASH_MURMUR3_H_

#include <Ecore_Getopt.h>
#include <stdint.h>

uint64_t
eng_hash_murmur3(const char *key);

#endif
