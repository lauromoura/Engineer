#include "eng_node.h"

/*** Node EFL API ***/

EAPI Eo *
eng_node()
{
   static Eo *node;

   if(!node)
   {
      efl_domain_current_push(EFL_ID_DOMAIN_SHARED);
      node = efl_add(ENG_NODE_CLASS, NULL);
      efl_domain_current_pop();
   }
   return node;
}

/*** Node Eo Constructors/Destructors ***/

EOLIAN static Efl_Object *
_eng_node_efl_object_constructor(Eo *obj, Eng_Node_Data *pd EINA_UNUSED)
{
   return efl_constructor(efl_super(obj, ENG_NODE_CLASS));
}

EOLIAN static Efl_Object *
_eng_node_efl_object_finalize(Eo *obj, Eng_Node_Data *pd)
{
   obj = efl_finalize(efl_super(obj, ENG_NODE_CLASS));

   // Set up our path elements.
   //if (pd->path == NULL) getcwd(pd->path, sizeof(PATH_MAX));
   if(pd->game == NULL) pd->game = "Default Node Title";
   if(pd->path == NULL) pd->path = "";

   pd->ipaddress = 1; // 2130706433; // For now, (127).0.0.1
   pd->mode      = 0; // Standalone = 0; Client = 1; Server = 2;.
   pd->peers     = eina_hash_string_superfast_new(eng_free_cb);

   pd->notice_sizes = eina_hash_int64_new(NULL);

   pd->scenes            = eina_hash_string_superfast_new(eng_free_cb);
   pd->component_classes = eina_hash_int64_new(NULL);
   pd->system_classes    = eina_hash_int64_new(NULL);

   pd->entity_count  = 0;
   pd->entity_queue  = eina_inarray_new(sizeof(uint64_t), 0);

   pd->entity_scene  = eina_hash_int64_new(NULL);

   //pd->templates = eina_hash_pointer_new(NULL);

   printf("Finalize Path: %s, Title: %s\n", pd->path, pd->game);
   printf("Active Threads Test: %d\n", ecore_thread_available_get());

   // Set up our path var.  Usually /opt/$GAMENAME.
   //Eina_Stringshare *path = eina_stringshare_printf("%s/%s/", pd->path, pd->title);

   return obj;
}

EOLIAN static void
_eng_node_efl_object_destructor(Eo *obj, Eng_Node_Data *pd EINA_UNUSED)
{
   efl_destructor(efl_super(obj, ENG_NODE_CLASS));
}

/*** Node Eo @property getter/setters. ***/

EOLIAN static String *
_eng_node_path_get(Eo *obj EINA_UNUSED, Eng_Node_Data *pd)
{
   return pd->path;
}

EOLIAN static void
_eng_node_path_set(Eo *obj EINA_UNUSED, Eng_Node_Data *pd, const char *path)
{
   pd->path = eina_stringshare_add(path);
}

EOLIAN static String *
_eng_node_game_get(Eo *obj EINA_UNUSED, Eng_Node_Data *pd)
{
   return pd->game;
}

EOLIAN static void
_eng_node_game_set(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        const char *game)
{
   pd->game = eina_stringshare_add(game);
}

/*** Asset and Module Loading/Unloading and Management Methods ***/

EOLIAN static Eng_Component_Class *
_eng_node_component_class_load(Eo *obj, Eng_Node_Data *pd,
        String *component_symbol, Eng_Scene *dependent)
{
   Eina_Hash *(*field_table_new)();

   Eng_Component_Class *class;
   Eina_Stringshare    *file;

   class = eng_node_component_class_query_loaded(obj, component_symbol);
   if(class)
   {
      // After making sure that the dependent scene is not already registered, register it.
      if(!eina_hash_find(class->dependent_scenes, &dependent))
         eina_hash_set(class->dependent_scenes, &dependent, dependent);
      return class;
   }

   // Make sure our module file exists before loading it.
   file =
      eina_stringshare_printf("%s/libraries/%s.so", pd->path, component_symbol);
   if(!ecore_file_exists(file))
   {
      printf("ERROR: Component_Class can't be loaded, its' module file can't be found.\n");
      eina_stringshare_del(file); return NULL;
   }

   // Create our eina_module reference handle.
   class       = malloc(sizeof(Eng_Component_Class));
   class->eina = eina_module_new(file);
   if(!class->eina)
   {
      printf("ERROR: Component_Class can't be loaded, its' module file is not properly formed.\n");
      free(class); eina_stringshare_del(file); return NULL;
   }
   eina_module_load(class->eina);

   #define ACTION(a, b) eina_module_symbol_get(a, b)
   #define TARGET "eng_component_"
   #define RETURN \
      { eina_module_free(class->eina); free(class); eina_stringshare_del(file); return NULL; }

   field_table_new = ACTION(class->eina, TARGET"field_enumerator_new");
   if(!(class->field_enumerator = field_table_new())) RETURN

   class->module_new   = ACTION(class->eina, TARGET"new");
   if(!class->module_new)   RETURN
   class->symbol_echo  = ACTION(class->eina, TARGET"symbol_echo");
   if(!class->symbol_echo)  RETURN
   class->asset_echo   = ACTION(class->eina, TARGET"asset_echo");
   if(!class->asset_echo)   RETURN
   class->label_echo   = ACTION(class->eina, TARGET"label_echo");
   if(!class->label_echo)   RETURN
   class->state_sizeof = ACTION(class->eina, TARGET"state_sizeof");
   if(!class->state_sizeof) RETURN

   class->field_label_echo  = ACTION(class->eina, TARGET"field_label_echo");
   if(!class->field_label_echo)  RETURN
   class->field_symbol_echo = ACTION(class->eina, TARGET"field_symbol_echo");
   if(!class->field_symbol_echo) RETURN

   class->dependent_register   = ACTION(class->eina, TARGET"dependent_register");
   if(!class->dependent_register)   RETURN
   class->dependent_deregister = ACTION(class->eina, TARGET"dependent_deregister");
   if(!class->dependent_deregister) RETURN

   class->state =        ACTION(class->eina, TARGET"state");
   if(!class->state)        RETURN
   class->state_export = ACTION(class->eina, TARGET"state_export");
   if(!class->state_export) RETURN
   class->state_import = ACTION(class->eina, TARGET"state_import");
   if(!class->state_import) RETURN
   class->state_query  = ACTION(class->eina, TARGET"state_query");
   if(!class->state_query)  RETURN

   class->attach_event = ACTION(class->eina, TARGET"attach_event");
   if(!class->attach_event) RETURN
   class->index_get    = ACTION(class->eina, TARGET"index_get");
   if(!class->index_get)    RETURN

   #undef RETURN
   #undef TARGET
   #undef ACTION

   // Set up our module class lookup data.
   class->assetid          = eng_hash_murmur3(class->asset_echo());
   class->id               = eng_hash_murmur3(class->symbol_echo());
   class->dependent_scenes = eina_hash_pointer_new(NULL);

   eina_hash_set(class->dependent_scenes, &dependent, dependent);
   eina_hash_set(pd->component_classes,   &class->id, class);

   printf("Class File loaded.  | Pointer: %p | Symbol_Key: %"PRId64" | File: %s\n",
      class, class->id, file);

   return class;
}

EOLIAN static void
_eng_node_component_class_unload(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        Eng_Component_Class *class, Eng_Scene *dependent)
{
   if(class)
   {
      if(!(dependent = eina_hash_find(class->dependent_scenes, &dependent)) ) return;

      eina_hash_del_by_key(class->dependent_scenes, dependent);
      if(!eina_hash_population(class->dependent_scenes))
      {
         eina_hash_del(pd->component_classes, &class->id, NULL);
         eina_hash_free(class->dependent_scenes);
         eina_module_free(class->eina);
         free(class);
      }
   }
}

EOLIAN static Eng_Component_Class *
_eng_node_component_class_query_loaded_by_id(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        SymbolID class_id)
{
   return eina_hash_find(pd->component_classes, &class_id);
}

// Check to see if the Component_Class is already loaded into the Node.
EOLIAN static Eng_Component_Class *
_eng_node_component_class_query_loaded(Eo *obj, Eng_Node_Data *pd EINA_UNUSED,
        String *component_symbol)
{
   SymbolID  classid = eng_hash_murmur3(component_symbol);

   return eng_node_component_class_query_loaded_by_id(obj, classid);
}

EOLIAN static Eng_System_Class *
_eng_node_system_class_load(Eo *obj, Eng_Node_Data *pd,
        String *system_symbol, Eng_Scene *dependent)
{
   Eng_System_Class           *class;
   Eng_Component_Class_Prereq *prereq;
   Eng_Component_Class_Target *target;
   String                     *file;

   // System Prerequestite (Component fields) Class constant Factories.
   Eina_Inarray * (*system_prereq_table_new)();
   Eina_Hash    * (*system_event_table_new)();

   Eina_Inarray * (*component_prereq_table_new)(Eo *node);
   Eina_Inarray * (*component_input_table_new)(Eo *node);
   Eina_Inarray * (*component_output_table_new)(Eo *node);

   Eina_Hash    * (*entity_event_table_new)();

   // Check to see if the System Class is already loaded into the Node.
   //   If it is, check to see if the dependent Eng_Scene is registered with the Class.
   //   If not, register it.
   class = eng_node_system_class_query_loaded(obj, system_symbol);
   if(class)
   {
      if(!eina_hash_find(class->dependent_scenes, &dependent))
         eina_hash_set(class->dependent_scenes, &dependent, dependent);
      return class;
   }

   // Make sure our Class file exists before loading it.
   file = eina_stringshare_printf("%s/libraries/%s.so", pd->path, system_symbol);
   if(!ecore_file_exists(file))
   {
      printf("ERROR: System_Class can't be loaded, its' module file can't be found.\n");
      eina_stringshare_del(file); return NULL;
   }

   // Create our eina_module reference handle.
   class       = malloc(sizeof(Eng_System_Class));
   class->eina = eina_module_new(file);
   if(!class->eina)
   {
      printf("ERROR: System_Class can't be loaded, its' module file is not properly formed.\n");
      free(class); eina_stringshare_del(file); return NULL;
   }
   eina_module_load(class->eina);

   // Link our System_Class function pointers to our loaded Eina_Module instruction data.
   #define ACTION(a, b) eina_module_symbol_get(a, b)
   #define TARGET "eng_system_"
   #define RETURN { eina_module_free(class->eina); eina_stringshare_del(file); return NULL; }

   // First, we check for the presence of all necesarry prerequisites, and load essential constants.
   system_prereq_table_new = ACTION(class->eina, TARGET"prereq_enumerator_new");
   if(!(class->system_prereqs = system_prereq_table_new()))      RETURN
   system_event_table_new  = ACTION(class->eina, TARGET"event_enumerator_new");
   if(!(class->system_events  = system_event_table_new()))  RETURN

   component_prereq_table_new = ACTION(class->eina, TARGET"component_prereq_class_enumerator_new");
   if(!(class->component_prereqs = component_prereq_table_new(obj))) RETURN
   component_input_table_new  = ACTION(class->eina, TARGET"component_input_class_enumerator_new");
   if(!(class->component_inputs  = component_input_table_new(obj)))  RETURN
   component_output_table_new = ACTION(class->eina, TARGET"component_output_class_enumerator_new");
   if(!(class->component_outputs = component_output_table_new(obj))) RETURN

   entity_event_table_new = ACTION(class->eina, TARGET"entity_event_enumerator_new");
   if(!(class->entity_events = entity_event_table_new())) RETURN

   // Next, we load our API function pointers.
   class->module_new   = ACTION(class->eina, TARGET"new");
   if(!class->module_new)  RETURN
   class->system_state = ACTION(class->eina, TARGET"state");
   if(!class->system_state) RETURN
   class->symbol_echo  = ACTION(class->eina, TARGET"symbol_echo");
   if(!class->symbol_echo) RETURN
   class->asset_echo   = ACTION(class->eina, TARGET"asset_echo");
   if(!class->asset_echo)  RETURN
   class->label_echo   = ACTION(class->eina, TARGET"label_echo");
   if(!class->label_echo)  RETURN

   class->system_event_register = ACTION(class->eina, TARGET"event_register");
   if(!class->system_event_register) RETURN
   class->entity_event_register = ACTION(class->eina, TARGET"entity_event_register");
   if(!class->entity_event_register) RETURN

   class->dependent_register   = ACTION(class->eina, TARGET"dependent_register");
   if(!class->dependent_register)   RETURN
   class->dependent_query      = ACTION(class->eina, TARGET"dependent_query");
   if(!class->dependent_query)      RETURN
   class->dependent_deregister = ACTION(class->eina, TARGET"dependent_deregister");
   if(!class->dependent_deregister) RETURN

   class->iterate = ACTION(class->eina, TARGET"iterate");
   if(!class->iterate) RETURN

   class->entity_query  = ACTION(class->eina, TARGET"entity_query");
   if(!class->entity_query)  RETURN
   class->entity_add    = ACTION(class->eina, TARGET"entity_add");
   if(!class->entity_add)    RETURN
   class->entity_remove = ACTION(class->eina, TARGET"entity_remove");
   if(!class->entity_remove) RETURN

   #undef RETURN
   #undef TARGET
   #undef ACTION

   class->dependent_scenes = eina_hash_pointer_new(NULL);
   eina_hash_set(class->dependent_scenes, &dependent, dependent);

   EINA_INARRAY_FOREACH(class->component_prereqs, prereq)
      prereq->class = eng_node_component_class_load(obj, prereq->symbol, dependent);

   EINA_INARRAY_FOREACH(class->component_inputs, target)
   {
      target->class = eng_node_component_class_query_loaded_by_id(obj, target->class_key);
      target->field = (Indx)(uint64_t)(eina_hash_find(target->class->field_enumerator, &target->field_key)) - 1;
   }

   EINA_INARRAY_FOREACH(class->component_outputs, target)
   {
      target->class = eng_node_component_class_query_loaded_by_id(obj, target->class_key);
      target->field = (Indx)(uint64_t)(eina_hash_find(target->class->field_enumerator, &target->field_key)) - 1;
   }

   class->system_event_register();
   class->entity_event_register();

   // Set up our system class enumerator and lookup data.
   class->assetid  = eng_hash_murmur3(class->asset_echo());
   class->id       = eng_hash_murmur3(class->symbol_echo());

   // Now we register our fully loaded System_Class in this Node.
   eina_hash_add(pd->system_classes, &class->id, class);

   printf("Class File loaded.  | Pointer: %p | Symbol_Key: %"PRId64" | File: %s\n",
      class, class->id, file);

   return class;
}

EOLIAN static void
_eng_node_system_class_unload(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        Eng_System_Class *class, Eng_Scene *dependent)
{
   // Make sure that the submitted class pointer is actually loaded.
   if(!eina_hash_find(pd->system_classes, &class->id)) return;

   // Check to make sure that the specified dependenct scene is registered.
   if(!(dependent = eina_hash_find(class->dependent_scenes, &dependent)) ) return;

   eina_hash_del_by_key(class->dependent_scenes, dependent);
   if(!eina_hash_population(class->dependent_scenes))
   {
      eina_hash_del(pd->system_classes, &class->id, NULL);
      eina_hash_free(class->dependent_scenes);
      eina_hash_free(class->system_events);
      eina_inarray_free(class->system_prereqs);
      eina_module_free(class->eina);
      free(class);
   }
}

EOLIAN static Eng_System_Class *
_eng_node_system_class_query_loaded_by_id(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        SymbolID classid)
{
   return eina_hash_find(pd->system_classes, &classid);
}

// Check to see if the System_Class is already loaded into the Node.
EOLIAN static Eng_System_Class *
_eng_node_system_class_query_loaded(Eo *obj, Eng_Node_Data *pd EINA_UNUSED,
        String *system_symbol)
{
   SymbolID  classid = eng_hash_murmur3(system_symbol);

   return eng_node_system_class_query_loaded_by_id(obj, classid);
}


/*** Entity Notification and Event API ***/

EOLIAN static void
_eng_node_notice_register(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        uint64_t event_key, uint64_t size)
{
   uint64_t current;

   size++;
   current = (uint64_t)eina_hash_find(pd->notice_sizes, &event_key);
   if(!current)
   {
      eina_hash_set(pd->notice_sizes, &event_key, (void*)size);
      return;
   }
   if(current != size)
      printf("ERROR: Notice size mismatch! event_key: %ld, size: %ld\n", event_key, --size);
}

EOLIAN static uint64_t
_eng_node_notice_sizeof(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        String *symbol)
{
   uint64_t key  = eng_hash_murmur3(symbol);
   uint64_t size = (uint64_t)eina_hash_find(pd->notice_sizes, &key);
   return --size;
}


/*** EntityID Take-a-Ticket System. ***/

EOLIAN static EntityID
_eng_node_entity_id_use(Eo *obj EINA_UNUSED, Eng_Node_Data *pd)
{
   uint64_t newid;

   if (eina_inarray_count(pd->entity_queue) == 0)
   {
      newid = pd->entity_count;
      pd->entity_count += 1;
   }
   else newid = *(uint64_t*)eina_inarray_pop(pd->entity_queue);

   return newid;
}

EOLIAN static void
_eng_node_entity_id_free(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        EntityID target)
{
   eina_inarray_insert_at(pd->entity_queue, 0, &target);
}

/*** Entity Internal API ***/

EOLIAN static void
_eng_node_entity_scene_set(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        EntityID entity, Eng_Scene *scene)
{
   eina_hash_set(pd->entity_scene, &entity, scene);
}

EOLIAN static Eng_Scene *
_eng_node_entity_scene_get(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        EntityID entity)
{
   return eina_hash_find(pd->entity_scene, &entity);
}

#include "eng_node.eo.c"

