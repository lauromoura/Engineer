#include "byte.h"

extern inline Byte
byte(double input);

extern inline void
Byte_NEW(Eina_Inarray **key);

extern inline void
Byte_FREE(Eina_Inarray **key);

extern inline void
Byte_PUSH(Eina_Inarray **key, Byte *input);

extern inline void
Byte_POP(Eina_Inarray **key);

extern inline void
Byte_READ(Eina_Inarray **key, Byte *bufferkey, uint64_t index);

extern inline void
Byte_WRITE(Eina_Inarray **key, Byte *bufferkey, uint64_t index);

extern inline void
Byte_COPY(Eina_Inarray **source, Eina_Inarray **destination, uint64_t index);

#undef SUBTYPES
#undef TYPE

