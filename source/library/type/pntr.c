#include "pntr.h"
/*
extern inline Pntr
pntr(double input);
*/
extern inline void
Pntr_NEW(Eina_Inarray **key);

extern inline void
Pntr_FREE(Eina_Inarray **key);

extern inline void
Pntr_PUSH(Eina_Inarray **key, Pntr *input);

extern inline void
Pntr_POP(Eina_Inarray **key);

extern inline void
Pntr_READ(Eina_Inarray **key, Pntr *bufferkey, uint64_t index);

extern inline void
Pntr_WRITE(Eina_Inarray **key, Pntr *bufferkey, uint64_t index);

#undef SUBTYPES
#undef TYPE
