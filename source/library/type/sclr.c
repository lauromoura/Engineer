#include "sclr.h"

extern inline Sclr
sclr(double input);

extern inline void
Sclr_NEW(Eina_Inarray **key);

extern inline void
Sclr_FREE(Eina_Inarray **key);

extern inline void
Sclr_PUSH(Eina_Inarray **key, Sclr *input);

extern inline void
Sclr_POP(Eina_Inarray **key);

extern inline void
Sclr_READ(Eina_Inarray **key, Sclr *bufferkey, uint64_t index);

extern inline void
Sclr_WRITE(Eina_Inarray **key, Sclr *bufferkey, uint64_t index);

#undef SUBTYPES
#undef TYPE
