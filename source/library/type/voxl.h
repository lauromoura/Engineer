#ifndef _ENG_TYPE_VOXL_H_
#define _ENG_TYPE_VOXL_H_

#include "color.h"

#undef TYPE
#define TYPE Voxl

#undef SUBTYPES
#define SUBTYPES \
   FIELD(material, Color) \
   FIELD(light,    Color) \

#include "type.h"

#endif


