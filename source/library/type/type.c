#ifdef TYPE
#ifdef SUBTYPES

#define SYMBOL(type) extern inline void type##_NEW
#define DEFINE(type) SYMBOL(type)(SUBSTRUCT(type) *key);
DEFINE(TYPE)
#undef DEFINE
#undef SYMBOL

#define SYMBOL(type) extern inline void type##_FREE
#define DEFINE(type) SYMBOL(type)(SUBSTRUCT(type) *key);
DEFINE(TYPE)
#undef DEFINE
#undef SYMBOL

#define SYMBOL(type) extern inline void type##_PUSH
#define DEFINE(type) SYMBOL(type)(SUBSTRUCT(type) *key, type *input);
DEFINE(TYPE)
#undef DEFINE
#undef SYMBOL

#define SYMBOL(type) extern inline void type##_POP
#define DEFINE(type) SYMBOL(type)(SUBSTRUCT(type) *key);
DEFINE(TYPE)
#undef DEFINE
#undef SYMBOL

#define SYMBOL(type) extern inline void type##_READ
#define DEFINE(type) SYMBOL(type)(SUBSTRUCT(type) *key, type *buffer, uint64_t index);
DEFINE(TYPE)
#undef DEFINE
#undef SYMBOL

#define SYMBOL(type) extern inline void type##_WRITE
#define DEFINE(type) SYMBOL(type)(SUBSTRUCT(type) *key, type *buffer, uint64_t index);
DEFINE(TYPE)
#undef DEFINE
#undef SYMBOL

#undef SUBSTRUCT

#undef SUBTYPES
#undef TYPE

#endif
#endif

