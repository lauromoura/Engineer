#include "angl.h"

extern inline Angl
angl(double input);

extern inline void
Angl_NEW(Eina_Inarray **key);

extern inline void
Angl_FREE(Eina_Inarray **key);

extern inline void
Angl_PUSH(Eina_Inarray **key, Angl *input);

extern inline void
Angl_POP(Eina_Inarray **key);

extern inline void
Angl_READ(Eina_Inarray **key, Angl *bufferkey, uint64_t index);

extern inline void
Angl_WRITE(Eina_Inarray **key, Angl *bufferkey, uint64_t index);

#undef SUBTYPES
#undef TYPE

