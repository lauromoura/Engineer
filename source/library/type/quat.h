#ifndef _ENG_TYPE_QUAT_H_
#define _ENG_TYPE_QUAT_H_

#include "angl.h"
#include "sclr.h"

#define TYPE Quat

#define SUBTYPES  \
   FIELD(w, Angl) \
   FIELD(x, Sclr) \
   FIELD(y, Sclr) \
   FIELD(z, Sclr)

#include "type.h"

inline Quat
quat(double x, double y, double z, double a)
{
   Quat output;
   double sinbuffer, xsqr, ysqr, zsqr, l;

   // Normalize our vector before submitting it.
   xsqr = x * x;
   ysqr = y * y;
   zsqr = z * z;
   l =  1 / sqrt(xsqr + ysqr + zsqr);
   x = x * l;
   y = y * l;
   z = z * l;

   // Convert our Axis/Angle input into Quaternion form.
   sinbuffer = sin(a / 2);
   a  = cos(a / 2);
   x *= sinbuffer;
   y *= sinbuffer;
   z *= sinbuffer;

   output.w = (Sclr)(a * BASIS);
   output.x = (Sclr)(x * BASIS);
   output.y = (Sclr)(y * BASIS);
   output.z = (Sclr)(z * BASIS);

   return output;
}

#endif

