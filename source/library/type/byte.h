#ifndef _ENG_TYPE_BYTE_H_
#define _ENG_TYPE_BYTE_H_

#include <Ecore_Getopt.h>
#include "../engineer_const.h"

typedef uint8_t Byte;

//typedef Bool Byte;

#define Byte_SOA Eina_Inarray*

inline Byte
byte(double input)
{
   return (Byte)(input * BASIS);
}

inline void
Byte_NEW(Eina_Inarray **key)
{
   *key = eina_inarray_new(sizeof(Byte), 0);
}

inline void
Byte_FREE(Eina_Inarray **key)
{
   eina_inarray_free(*key);
}

inline void
Byte_PUSH(Eina_Inarray **key, Byte *input)
{
   eina_inarray_push(*key, input);
}

inline void
Byte_POP(Eina_Inarray **key)
{
   eina_inarray_pop(*key);
}

inline void
Byte_READ(Eina_Inarray **key, Byte *bufferkey, uint64_t index)
{
   *bufferkey = *(Byte*)eina_inarray_nth(*key, index);
}

inline void
Byte_WRITE(Eina_Inarray **key, Byte *bufferkey, uint64_t index)
{
   eina_inarray_replace_at(*key, index, bufferkey);
}

inline void
Byte_COPY(Eina_Inarray **source, Eina_Inarray **destination, uint64_t index)
{
   eina_inarray_replace_at(*destination, index, eina_inarray_nth(*source, index));
}

#endif

