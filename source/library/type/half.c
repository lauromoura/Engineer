#include "half.h"

extern inline Half
half(double input);

extern inline void
Half_NEW(Eina_Inarray **key);

extern inline void
Half_FREE(Eina_Inarray **key);

extern inline void
Half_PUSH(Eina_Inarray **key, Half *input);

extern inline void
Half_POP(Eina_Inarray **key);

extern inline void
Half_READ(Eina_Inarray **key, Half *bufferkey, uint64_t index);

extern inline void
Half_WRITE(Eina_Inarray **key, Half *bufferkey, uint64_t index);

#undef SUBTYPES
#undef TYPE

