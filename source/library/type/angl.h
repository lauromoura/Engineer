#ifndef _ENG_TYPE_ANGL_H_
#define _ENG_TYPE_ANGL_H_

#include <Ecore_Getopt.h>
#include "../engineer_const.h"

typedef BASE Angl;

#define Angl_SOA Eina_Inarray*

inline Angl
angl(double input)
{
   return (Angl)(input * BASIS);
}

inline void
Angl_NEW(Eina_Inarray **key)
{
   *key = eina_inarray_new(sizeof(Angl), 0);
}

inline void
Angl_FREE(Eina_Inarray **key)
{
   eina_inarray_free(*key);
}

inline void
Angl_PUSH(Eina_Inarray **key, Angl *input)
{
   eina_inarray_push(*key, input);
}

inline void
Angl_POP(Eina_Inarray **key)
{
   eina_inarray_pop(*key);
}

inline void
Angl_READ(Eina_Inarray **key, Angl *bufferkey, uint64_t index)
{
   *bufferkey = *(Angl*)eina_inarray_nth(*key, index);
}

inline void
Angl_WRITE(Eina_Inarray **key, Angl *bufferkey, uint64_t index)
{
   eina_inarray_replace_at(*key, index, bufferkey);
}

#endif

