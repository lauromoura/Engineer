#ifndef _ENG_TYPE_PNTR_H_
#define _ENG_TYPE_PNTR_H_

#include <Ecore_Getopt.h>
#include "../engineer_const.h"

typedef struct {} Data;
typedef Data*     Pntr;

#define Pntr_SOA Eina_Inarray*
/*
inline Pntr
pntr(double input)
{
   return (Pntr)(input * BASIS);
}
*/
inline void
Pntr_NEW(Eina_Inarray **key)
{
   *key = eina_inarray_new(sizeof(Pntr), 0);
}

inline void
Pntr_FREE(Eina_Inarray **key)
{
   eina_inarray_free(*key);
}

inline void
Pntr_PUSH(Eina_Inarray **key, Pntr *input)
{
   eina_inarray_push(*key, input);
}

inline void
Pntr_POP(Eina_Inarray **key)
{
   eina_inarray_pop(*key);
}

inline void
Pntr_READ(Eina_Inarray **key, Pntr *bufferkey, uint64_t index)
{
   *bufferkey = *(Pntr*)eina_inarray_nth(*key, index);
}

inline void
Pntr_WRITE(Eina_Inarray **key, Pntr *bufferkey, uint64_t index)
{
   eina_inarray_replace_at(*key, index, bufferkey);
}

#endif

