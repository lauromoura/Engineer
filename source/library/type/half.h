#ifndef _ENG_TYPE_HALF_H_
#define _ENG_TYPE_HALF_H_

#include <Ecore_Getopt.h>
#include "../engineer_const.h"

typedef int32_t Half;

//typedef Half Indx;

#define Half_SOA Eina_Inarray*

inline Half
half(double input)
{
   return (Half)(input * BASIS);
}

inline void
Half_NEW(Eina_Inarray **key)
{
   *key = eina_inarray_new(sizeof(Half), 0);
}

inline void
Half_FREE(Eina_Inarray **key)
{
   eina_inarray_free(*key);
}

inline void
Half_PUSH(Eina_Inarray **key, Half *input)
{
   eina_inarray_push(*key, input);
}

inline void
Half_POP(Eina_Inarray **key)
{
   eina_inarray_pop(*key);
}

inline void
Half_READ(Eina_Inarray **key, Half *bufferkey, uint64_t index)
{
   *bufferkey = *(Half*)eina_inarray_nth(*key, index);
}

inline void
Half_WRITE(Eina_Inarray **key, Half *bufferkey, uint64_t index)
{
   eina_inarray_replace_at(*key, index, bufferkey);
}

#endif

