#ifndef _ENG_TYPE_AABB_H_
#define _ENG_TYPE_AABB_H_

#include "vec3.h"

#undef TYPE
#define TYPE AABB

#undef SUBTYPES
#define SUBTYPES  \
   FIELD(position, Vec3) \
   FIELD(bounds,   Vec3)

#include "type.h"

//Fixme: not implemented, copied from elsewhere.
inline AABB *
aabb(double x EINA_UNUSED, double y EINA_UNUSED, double z EINA_UNUSED)
{
   AABB *output = malloc(sizeof(AABB));

   //output->x = (Sclr)(x * BASIS);
   //output->y = (Sclr)(y * BASIS);
   //output->z = (Sclr)(z * BASIS);

   return output;
}

#endif

