#ifdef TYPE
#ifdef SUBTYPES

#define SUBSTRUCT(type) type##_SOA

#define FIELD(key, type) type key;
#define DEFINE(type) \
   typedef struct \
   { \
      SUBTYPES \
   } \
   type;
   DEFINE(TYPE)
#undef DEFINE
#undef FIELD

#define FIELD(key, type) SUBSTRUCT(type) key;
#define DEFINE(type) \
   typedef struct \
   { \
      SUBTYPES \
   } \
   SUBSTRUCT(type);
   DEFINE(TYPE)
#undef DEFINE
#undef FIELD

#define SYMBOL(type) inline void type##_NEW
#define FIELD(subkey, type) type##_NEW(&key->subkey);
#define DEFINE(type) \
   SYMBOL(type)(SUBSTRUCT(type) *key) \
   { \
      SUBTYPES \
   }
   DEFINE(TYPE)
#undef DEFINE
#undef FIELD
#undef SYMBOL

#define SYMBOL(type) inline void type##_FREE
#define FIELD(subkey, type) type##_FREE(&key->subkey);
#define DEFINE(type) \
   SYMBOL(type)(SUBSTRUCT(type) *key) \
   { \
      SUBTYPES \
   }
   DEFINE(TYPE)
#undef DEFINE
#undef FIELD
#undef SYMBOL

#define SYMBOL(type) inline void type##_PUSH
#define FIELD(subkey, type) type##_PUSH(&key->subkey, &input->subkey);
#define DEFINE(type) \
   SYMBOL(type)(SUBSTRUCT(type) *key, type *input) \
   { \
      SUBTYPES \
   }
   DEFINE(TYPE)
#undef DEFINE
#undef FIELD
#undef SYMBOL

#define SYMBOL(type) inline void type##_POP
#define FIELD(subkey, type) type##_POP(&key->subkey);
#define DEFINE(type) \
   SYMBOL(type)(SUBSTRUCT(type) *key) \
   { \
      SUBTYPES \
   }
   DEFINE(TYPE)
#undef DEFINE
#undef FIELD
#undef SYMBOL

#define SYMBOL(type) inline void type##_READ
#define FIELD(subkey, type) type##_READ(&key->subkey, &buffer->subkey, index);
#define DEFINE(type) \
   SYMBOL(type)(SUBSTRUCT(type) *key, type *buffer, uint64_t index) \
   { \
      SUBTYPES \
   }
   DEFINE(TYPE)
#undef DEFINE
#undef FIELD
#undef SYMBOL

#define SYMBOL(type) inline void type##_WRITE
#define FIELD(subkey, type) type##_WRITE(&key->subkey, &buffer->subkey, index);
#define DEFINE(type) \
   SYMBOL(type)(SUBSTRUCT(type) *key, type *buffer, uint64_t index) \
   { \
      SUBTYPES \
   }
   DEFINE(TYPE)
#undef DEFINE
#undef FIELD
#undef SYMBOL

#ifndef SWITCH
   #undef SUBSTRUCT
   #undef SUBTYPES
   #undef TYPE
#endif

#endif
#endif

