#include "brick.h"
/*
extern inline Sclr
sclr(double input);
*/
extern inline void
Brick_NEW(Eina_Inarray **key);

extern inline void
Brick_FREE(Eina_Inarray **key);

extern inline void
Brick_PUSH(Eina_Inarray **key, Brick *input);

extern inline void
Brick_POP(Eina_Inarray **key);

extern inline void
Brick_READ(Eina_Inarray **key, Brick *bufferkey, uint64_t index);

extern inline void
Brick_WRITE(Eina_Inarray **key, Brick *bufferkey, uint64_t index);

#undef SUBTYPES
#undef TYPE

