#ifndef _ENG_TYPE_BRICK_H_
#define _ENG_TYPE_BRICK_H_

#include <Ecore_Getopt.h>
#include "../engineer_const.h"
#include "voxl.h"

typedef Voxl Brick[16 * 16 * 16];

#define Brick_SOA Eina_Inarray*
/*
inline Brick
brick(double input)
{
   return (Sclr)(input * BASIS);
}
*/
inline void
Brick_NEW(Eina_Inarray **key)
{
   *key = eina_inarray_new(sizeof(Brick), 0);
}

inline void
Brick_FREE(Eina_Inarray **key)
{
   eina_inarray_free(*key);
}

inline void
Brick_PUSH(Eina_Inarray **key, Brick *input)
{
   eina_inarray_push(*key, input);
}

inline void
Brick_POP(Eina_Inarray **key)
{
   eina_inarray_pop(*key);
}

inline void
Brick_READ(Eina_Inarray **key, Brick *bufferkey, uint64_t index)
{
   memcpy(bufferkey, eina_inarray_nth(*key, index), sizeof(Brick));
}

inline void
Brick_WRITE(Eina_Inarray **key, Brick *bufferkey, uint64_t index)
{
   eina_inarray_replace_at(*key, index, bufferkey);
}

#endif

