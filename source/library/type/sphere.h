#ifndef _ENG_TYPE_SPHERE_H_
#define _ENG_TYPE_SPHERE_H_

#include "vec3.h"

#undef TYPE
#define TYPE Sphere

#undef SUBTYPES
#define SUBTYPES  \
   FIELD(position, Vec3) \
   FIELD(radius,   Sclr) \
   FIELD(hollow,   Sclr)

#include "type.h"

//Fixme: not implemented, copied from elsewhere.
inline Sphere *
sphere(double x EINA_UNUSED, double y EINA_UNUSED, double z EINA_UNUSED)
{
   Sphere *output = malloc(sizeof(Sphere));

   //output->x = (Sclr)(x * BASIS);
   //output->y = (Sclr)(y * BASIS);
   //output->z = (Sclr)(z * BASIS);

   return output;
}

#endif

