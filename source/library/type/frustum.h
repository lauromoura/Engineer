#ifndef _ENG_TYPE_FRUSTUM_H_
#define _ENG_TYPE_FRUSTUM_H_

#include "aabb.h"

#undef TYPE
#define TYPE Frustum

#undef SUBTYPES
#define SUBTYPES  \
   FIELD(position, Vec3) \
   FIELD(bounds,   Vec3)

#include "type.h"

//Fixme: not implemented, copied from elsewhere.
inline Frustum *
frustum(double x EINA_UNUSED, double y EINA_UNUSED, double z EINA_UNUSED)
{
   Frustum *output = malloc(sizeof(AABB));

   //output->x = (Sclr)(x * BASIS);
   //output->y = (Sclr)(y * BASIS);
   //output->z = (Sclr)(z * BASIS);

   return output;
}

#endif

