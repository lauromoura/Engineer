#ifndef _ENG_TYPE_EULR_H_
#define _ENG_TYPE_EULR_H_

#include "angl.h"

#define TYPE Eulr

#define SUBTYPES  \
   FIELD(yaw,   Angl) \
   FIELD(pitch, Angl) \
   FIELD(roll,  Angl)

#include "type.h"

#endif

