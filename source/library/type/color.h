#ifndef _ENG_TYPE_COLOR_H_
#define _ENG_TYPE_COLOR_H_

#include "byte.h"

#undef TYPE
#define TYPE Color

#undef SUBTYPES
#define SUBTYPES \
   FIELD(red,   Byte) \
   FIELD(blue,  Byte) \
   FIELD(green, Byte) \
   FIELD(alpha, Byte) \

#include "type.h"

inline Color
color(double red, double green, double blue, double alpha)
{
   Color *color, colordata;
   color = &colordata;

   color->red   = (unsigned char)255 * red;
   color->green = (unsigned char)255 * green;
   color->blue  = (unsigned char)255 * blue;
   color->alpha = (unsigned char)255 * alpha;

   return *color;
}

#endif

