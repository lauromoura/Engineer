#ifndef _ENG_TYPE_OCTNODE_H_
#define _ENG_TYPE_OCTNODE_H_

#include "byte.h"
#include "indx.h"

#undef TYPE
#define TYPE Oct_Node

#undef SUBTYPES
#define SUBTYPES  \
   FIELD(parent,    Indx) \
   FIELD(firstborn, Indx) \
   FIELD(childmask, Byte) \
                          \
   FIELD(depth,     Byte) \
   FIELD(upper,     Indx) \
   FIELD(middle,    Indx) \
   FIELD(lower,     Indx) \

#include "type.h"

#endif

