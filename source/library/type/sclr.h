#ifndef _ENG_TYPE_SCLR_H_
#define _ENG_TYPE_SCLR_H_

#include <Ecore_Getopt.h>
#include "../engineer_const.h"

typedef BASE Sclr;

typedef Sclr Index;
typedef Sclr ClassID;
typedef Sclr EntityID;
typedef Sclr ComponentID;
typedef Sclr StateID;
typedef Sclr EventID;

#define Sclr_SOA Eina_Inarray*

inline Sclr
sclr(double input)
{
   return (Sclr)(input * BASIS);
}

inline void
Sclr_NEW(Eina_Inarray **key)
{
   *key = eina_inarray_new(sizeof(Sclr), 0);
}

inline void
Sclr_FREE(Eina_Inarray **key)
{
   eina_inarray_free(*key);
}

inline void
Sclr_PUSH(Eina_Inarray **key, Sclr *input)
{
   eina_inarray_push(*key, input);
}

inline void
Sclr_POP(Eina_Inarray **key)
{
   eina_inarray_pop(*key);
}

inline void
Sclr_READ(Eina_Inarray **key, Sclr *bufferkey, uint64_t index)
{
   *bufferkey = *(Sclr*)eina_inarray_nth(*key, index);
}

inline void
Sclr_WRITE(Eina_Inarray **key, Sclr *bufferkey, uint64_t index)
{
   eina_inarray_replace_at(*key, index, bufferkey);
}

#endif

