#ifndef _ENG_TYPE_VEC2_H_
#define _ENG_TYPE_VEC2_H_

#include "sclr.h"

#define TYPE Vec2

#define SUBTYPES  \
   FIELD(x, Sclr) \
   FIELD(y, Sclr)

#include "type.h"

inline Vec2
vec2(double x, double y)
{
   Vec2 output;

   output.x = (Sclr)(x * BASIS);
   output.y = (Sclr)(y * BASIS);

   return output;
}

#endif

