#ifndef _ENG_TYPE_VEC3_H_
#define _ENG_TYPE_VEC3_H_

#include "sclr.h"

#define TYPE Vec3

#define SUBTYPES  \
   FIELD(x, Sclr) \
   FIELD(y, Sclr) \
   FIELD(z, Sclr)

#include "type.h"

inline Vec3
vec3(double x, double y, double z)
{
   Vec3 output;

   output.x = (Sclr)(x * BASIS);
   output.y = (Sclr)(y * BASIS);
   output.z = (Sclr)(z * BASIS);

   return output;
}

#endif

