#ifndef _ENG_TYPE_AABBNODE_H_
#define _ENG_TYPE_AABBNODE_H_

#include "aabb.h"

#undef TYPE
#define TYPE AABB_Node

#undef SUBTYPES
#define SUBTYPES  \
   FIELD(parent,   Sclr) \
   FIELD(boundary, AABB) \
   FIELD(payload,  Sclr)

#include "type.h"

#endif

