#include "indx.h"

extern inline Indx
indx(double input);

extern inline void
Indx_NEW(Eina_Inarray **key);

extern inline void
Indx_FREE(Eina_Inarray **key);

extern inline void
Indx_PUSH(Eina_Inarray **key, Indx *input);

extern inline void
Indx_POP(Eina_Inarray **key);

extern inline void
Indx_READ(Eina_Inarray **key, Indx *bufferkey, uint64_t index);

extern inline void
Indx_WRITE(Eina_Inarray **key, Indx *bufferkey, uint64_t index);

#undef SUBTYPES
#undef TYPE

