#ifndef _ENG_TYPE_INDX_H_
#define _ENG_TYPE_INDX_H_

#include <Ecore_Getopt.h>
#include "../engineer_const.h"

typedef uint32_t Indx;

#define Indx_SOA Eina_Inarray*

inline Indx
indx(double input)
{
   return (Indx)(input * BASIS);
}

inline void
Indx_NEW(Eina_Inarray **key)
{
   *key = eina_inarray_new(sizeof(Indx), 0);
}

inline void
Indx_FREE(Eina_Inarray **key)
{
   eina_inarray_free(*key);
}

inline void
Indx_PUSH(Eina_Inarray **key, Indx *input)
{
   eina_inarray_push(*key, input);
}

inline void
Indx_POP(Eina_Inarray **key)
{
   eina_inarray_pop(*key);
}

inline void
Indx_READ(Eina_Inarray **key, Indx *bufferkey, uint64_t index)
{
   *bufferkey = *(Indx*)eina_inarray_nth(*key, index);
}

inline void
Indx_WRITE(Eina_Inarray **key, Indx *bufferkey, uint64_t index)
{
   eina_inarray_replace_at(*key, index, bufferkey);
}

inline void
Indx_COPY(Eina_Inarray **source, Eina_Inarray **destination, uint64_t index)
{
   eina_inarray_replace_at(*destination, index, eina_inarray_nth(*source, index));
}

#endif

