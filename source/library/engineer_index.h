#ifndef _ENGINEER_LIBRARY_GLOBAL_INDEX_H_
#define _ENGINEER_LIBRARY_GLOBAL_INDEX_H_

#define EFL_BETA_API_SUPPORT 1
#define EFL_EO_API_SUPPORT 1

#include <stdlib.h>
#include <stdio.h>
#include <stdatomic.h>
#include <string.h>

#include <Ecore_Getopt.h>
#include <Eo.h>
#include <Elementary.h>

#include "engineer_const.h"
#include "engineer_structs.h" // This used to be on bottom. Why?

#include "tool/array.h"

#include "type/pntr.h"
#include "type/byte.h"
#include "type/indx.h"
#include "type/half.h"
#include "type/sclr.h"
#include "type/angl.h"
#include "type/vec2.h"
#include "type/vec3.h"
#include "type/eulr.h"
#include "type/mtrx.h"
#include "type/quat.h"
#include "type/aabb.h"
#include "type/aabbnode.h"
#include "type/octnode.h"
#include "type/sphere.h"
#include "type/frustum.h"
#include "type/color.h"
#include "type/voxl.h"
#include "type/brick.h"

#include "math/cordic.h"
#include "math/sclr.h"
#include "math/angl.h"
#include "math/vec2.h"
#include "math/vec3.h"
#include "math/eulr.h"
#include "math/mtrx.h"
#include "math/quat.h"
#include "math/aabb.h"
#include "math/aabbtree.h"
#include "math/sphere.h"
#include "math/frustum.h"
#include "math/sets.h"

#include "tool/murmur3.h"
#include "tool/queue.h"
#include "tool/cache.h"

#endif
